package com.cosmetic.cosmetic.mock.service;

import com.cosmetic.cosmetic.dto.account.AccountDto;
import com.cosmetic.cosmetic.dto.user.UserDto;
import com.cosmetic.cosmetic.dto.user.UserMappedDto;
import com.cosmetic.cosmetic.dto.user.UserSaveDto;
import com.cosmetic.cosmetic.fixture.AccountFixture;
import com.cosmetic.cosmetic.fixture.UserFixture;
import com.cosmetic.cosmetic.service.CrudService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@ExtendWith(MockitoExtension.class)
public class CrudServiceTests {
    @Mock
    private UserDto userDto;

    @Mock
    private UserMappedDto userMappedDto;

    @Mock
    private UserSaveDto userSaveDto;

    @Mock
    CrudService<UserMappedDto, UUID, UserDto, UserSaveDto> crudService;

    @BeforeEach
    void init() {
        this.userDto = UserFixture.singleUserDto();
        this.userMappedDto = UserFixture.singleUserMappedDto();
    }

    @Nested
    @DisplayName("should return the correct type & value of: ")
    class interfaceCrud {
        List<UserMappedDto> list = new ArrayList<>();

        @BeforeEach
        void init() {
            list.add(userMappedDto);
        }

        @Test
        @DisplayName("FindAll")
        void testingTypeFindAll() {
            Mockito.when(crudService.findAll()).thenReturn(list);
            Assertions.assertEquals(crudService.findAll(), list);
            Assertions.assertInstanceOf(crudService.findAll().getClass(), new ArrayList<UserMappedDto>());
        };

        @Test
        @DisplayName("FindOne")
        void testingTypeFindOne() {
            Mockito.when(crudService.findOne(Mockito.any())).thenReturn(userMappedDto);
            Assertions.assertEquals(userMappedDto, crudService.findOne(userMappedDto.getId()));
            Assertions.assertInstanceOf(userMappedDto.getClass(),
                    crudService.findOne(userMappedDto.getId())
            );
        };

        @Test
        @DisplayName("Create")
        void testingTypeCreate() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
            Mockito.when(crudService.create(userSaveDto)).thenReturn(userMappedDto);
            Assertions.assertEquals(userMappedDto, crudService.create(userSaveDto));
            Assertions.assertInstanceOf(userMappedDto.getClass(),
                    crudService.create(userSaveDto)
            );
        };

        @Test
        @DisplayName("Update")
        void testingTypeUpdate() {
            Mockito.when(crudService.update(userDto)).thenReturn(userMappedDto);
            Assertions.assertEquals(userMappedDto, crudService.update(userDto));
            Assertions.assertInstanceOf(userMappedDto.getClass(),
                    crudService.update(userDto)
            );
        };

        @Test
        @DisplayName("Delete")
        void testingTypeDelete() {
            Mockito.when(crudService.delete(userMappedDto.getId())).thenReturn(Boolean.TRUE);
            Assertions.assertEquals(Boolean.TRUE, crudService.delete(userMappedDto.getId()));
            Assertions.assertInstanceOf(Boolean.TRUE.getClass(),
                    crudService.delete(userMappedDto.getId())
            );
        };
    }
}
