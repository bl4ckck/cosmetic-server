package com.cosmetic.cosmetic.mock.service;

import com.cosmetic.cosmetic.entity.Brand;
import com.cosmetic.cosmetic.entity.Product;
import com.cosmetic.cosmetic.entity.Variant;
import com.cosmetic.cosmetic.mapper.product.ProductMappedMapper;
import com.cosmetic.cosmetic.repository.ProductRepository;
import com.cosmetic.cosmetic.service.ProductService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;

import java.util.*;
import java.util.stream.Collectors;

@ActiveProfiles("test")
@ExtendWith(MockitoExtension.class)
class ProductServiceTests {
    @InjectMocks
    private ProductService productService;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private ProductMappedMapper mapper;

    private final ProductMappedMapper productMappedMapper = ProductMappedMapper.INSTANCE;

    @Nested
    @DisplayName("Find all product")
    class findAllProduct {
        @Test
        @DisplayName("should return find all product formatted with mapper")
        void validFindAll() {
            Set<Product> productList = new HashSet<>();
            // Variant
            Set<Variant> variantList = new HashSet<>();
            variantList.add(new Variant(
                    UUID.randomUUID(),
                    "VARIANT_NAME",
                    20000D,
                    12,
                    50D,
                    0
            ));
            Product product = new Product(
                    UUID.randomUUID(),
                    "awe",
                    new String[]{"img1", "img2"},
                    Boolean.FALSE
            );
            product.setVariants(variantList);
            product.getVariants().stream()
                    .peek(val -> val.setProduct(product))
                    .collect(Collectors.toSet());
            productList.add(product);
            // Brand
            Brand brand = new Brand(
                    UUID.randomUUID(),
                    productList,
                    "nama brand",
                    "desc",
                    "logo",
                    "banner"
            );
            brand.getProducts()
                    .stream()
                    .peek(val -> val.setBrand(brand))
                    .collect(Collectors.toSet());
            List<Product> newProduct = new ArrayList<>(productList);
            Mockito.when(productRepository.findAll()).thenReturn(newProduct);
            Assertions.assertEquals(mapper.toList(newProduct), productService.findAll());
            Assertions.assertNotNull(mapper);
            System.out.println(mapper.toList(newProduct));
        }
    }
}
