package com.cosmetic.cosmetic.mock.service;

import com.cosmetic.cosmetic.dto.user.UserDto;
import com.cosmetic.cosmetic.dto.user.UserMappedDto;
import com.cosmetic.cosmetic.dto.user.UserSaveDto;
import com.cosmetic.cosmetic.entity.User;
import com.cosmetic.cosmetic.fixture.UserFixture;
import com.cosmetic.cosmetic.mapper.user.UserMappedMapper;
import com.cosmetic.cosmetic.repository.UserRepository;
import com.cosmetic.cosmetic.service.UserService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.ActiveProfiles;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@ActiveProfiles("test")
@ExtendWith(MockitoExtension.class)
public class UserServiceTests {
    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserMappedMapper userMappedMapper;

    @Mock
    private User user;

    @Mock
    private UserDto userDto;

    @Mock
    private UserMappedDto userMappedDto;

    @Mock
    private UserSaveDto userSaveDto;

    @BeforeEach
    void init() {
        this.user = UserFixture.singleUser();
        this.userDto = UserFixture.singleUserDto();
        this.userMappedDto = UserFixture.singleUserMappedDto();
        this.userSaveDto = UserFixture.singleUserSaveDto();
    }

    @Nested
    @DisplayName("Create User")
    class createUserTest {
        @Test
        @DisplayName("should return the correct type & value of UserMappedDto")
        void checkType() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
            Mockito.when(userRepository.save(Mockito.any())).thenReturn(user);
            Mockito.when(userMappedMapper.toDto(Mockito.any())).thenReturn(userMappedDto);
            Assertions.assertInstanceOf(userMappedDto.getClass(), userService.create(userSaveDto));
            Assertions.assertEquals(userMappedDto, userService.create(userSaveDto));
            System.out.println(userService.create(userSaveDto));
        }
    }

    @Nested
    @DisplayName("Find All User")
    class findAllUserTest {
        List<User> list = new ArrayList<>();
        List<UserMappedDto> listDto = new ArrayList<>();

        @BeforeEach
        void stub() {
            list.add(user);
            listDto.add(userMappedDto);
        }

        @Test
        @DisplayName("should return the correct type & value of List<UserMappedDto>")
        void checkType() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
            Mockito.when(userRepository.findAll()).thenReturn(list);
            Mockito.when(userMappedMapper.toList(list)).thenReturn(listDto);
            Assertions.assertInstanceOf(listDto.getClass(), userService.findAll());
            Assertions.assertEquals(listDto, userService.findAll());
            System.out.println(userService.findAll());
        }
    }

    @Nested
    @DisplayName("Find One User")
    class findOneUserTest {
        @BeforeEach
        void stub() {
            Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(user));
            Mockito.when(userMappedMapper.toDto(user)).thenReturn(userMappedDto);
        }

        @Test
        @DisplayName("should return the correct type & value of UserMappedDto")
        void checkType() {
            Assertions.assertInstanceOf(userMappedDto.getClass(), userService.findOne(userMappedDto.getId()));
            Assertions.assertEquals(userMappedDto, userService.findOne(userMappedDto.getId()));
            System.out.println(userService.findOne(UUID.randomUUID()));
        }

        @Test
        @DisplayName("should return null when ID is not found")
        void checkDataNull() {
            UUID givenID = UUID.fromString("e2852c67-9db2-4e9c-8550-c11a05048654");
            Assertions.assertNotEquals(givenID, userService.findOne(userMappedDto.getId()).getId());
            // Null check
            Mockito.when(userService.findOne(givenID)).thenReturn(null);
            Assertions.assertNull(userService.findOne(userMappedDto.getId()));
        }
    }

    @Nested
    @DisplayName("Update User")
    class updateUserTest {
        @BeforeEach
        void stub() {
            Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(user));
            Mockito.when(userRepository.save(Mockito.any())).thenReturn(user);
            Mockito.when(userMappedMapper.toDto(user)).thenReturn(userMappedDto);
        }

        @Test
        @DisplayName("should return the correct type & value of UserMappedDto")
        void checkType() {
            Assertions.assertInstanceOf(userMappedDto.getClass(), userService.update(userDto));
            Assertions.assertEquals(userMappedDto, userService.update(userDto));
            System.out.println(userService.update(userDto));
        }

        @Test
        @DisplayName("should return null when ID is not found")
        void checkDataNull() {
            UUID givenID = UUID.fromString("e2852c67-9db2-4e9c-8550-c11a05048654");
            Assertions.assertNotEquals(givenID, userService.update(userDto).getId());
            // Null check
            userDto.setId(givenID);
            Mockito.when(userService.update(userDto)).thenReturn(null);
            Assertions.assertNull(userService.update(userDto));
        }
    }

    @Nested
    @DisplayName("Delete User")
    class deleteUserTest {
        @Test
        @DisplayName("should return the correct type & value of UserMappedDto")
        void checkType() {
            Assertions.assertInstanceOf(Boolean.class, userService.delete(userMappedDto.getId()));
            Assertions.assertEquals(Boolean.TRUE, userService.delete(userMappedDto.getId()));
            System.out.println(userService.delete(userMappedDto.getId()));
        }
    }
}
