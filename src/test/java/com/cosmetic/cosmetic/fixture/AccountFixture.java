package com.cosmetic.cosmetic.fixture;

import com.cosmetic.cosmetic.dto.account.AccountDto;
import com.cosmetic.cosmetic.entity.Account;
import com.cosmetic.cosmetic.entity.ERole;

import java.util.UUID;

public class AccountFixture {
    public static Account singleAccount() {
        Account account = new Account();
        account.setId(UUID.randomUUID());
        account.setRole(ERole.ROLE_CUSTOMER);
        account.setEmail("EMAIL");
        account.setAvatar("AVATAR");
        return account;
    }

    public static AccountDto singleAccountDto() {
        AccountDto accountDto = new AccountDto();
        accountDto.setId(UUID.randomUUID());
        accountDto.setEmail("ACCOUNT_EMAIL");
        accountDto.setIsActive(false);
        return accountDto;
    }
}
