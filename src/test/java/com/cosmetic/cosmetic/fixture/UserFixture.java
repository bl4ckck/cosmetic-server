package com.cosmetic.cosmetic.fixture;

import com.cosmetic.cosmetic.dto.user.UserDto;
import com.cosmetic.cosmetic.dto.user.UserMappedDto;
import com.cosmetic.cosmetic.dto.user.UserSaveDto;
import com.cosmetic.cosmetic.entity.User;

import java.util.UUID;

public class UserFixture {
    public static User singleUser() {
        User user = new User("USER_ENTITY_NAME",
                "USER_ENTITY_PHONE", "USER_SKIN_TYPE");
        user.setId(UUID.randomUUID());
        user.setAccount(AccountFixture.singleAccount());
        return user;
    }

    public static UserDto singleUserDto() {
        UserDto userDto = new UserDto();
        userDto.setId(UUID.randomUUID());
        userDto.setName("USER_NAME");
        userDto.setPhone("USER_PHONE");
        return userDto;
    }

    public static UserMappedDto singleUserMappedDto() {
        UserMappedDto userMappedDto = new UserMappedDto();
        userMappedDto.setId(UUID.randomUUID());
        userMappedDto.setName("USER_NAME");
        userMappedDto.setPhone("USER_PHONE");
        userMappedDto.setAddresses(null);
        return userMappedDto;
    }

    public static UserSaveDto singleUserSaveDto() {
        UserSaveDto userSaveDto = new UserSaveDto();
        userSaveDto.setName("USER_NAME");
        userSaveDto.setPhone("USER_PHONE");
        return userSaveDto;
    }
}
