package com.cosmetic.cosmetic.component;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.cosmetic.cosmetic.dto.JWTDto;
import com.cosmetic.cosmetic.entity.Account;
import com.cosmetic.cosmetic.mapper.auth.JwtMapper;
import io.github.cdimascio.dotenv.Dotenv;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Date;
import java.util.Map;

@Component
public class JwtComponent {
    @Value( "${jwt.secret:null}" )
    private String jwtSecret;

    @Autowired
    private JwtMapper jwtMapper;

    private final Dotenv dotenv = Dotenv.configure().ignoreIfMissing().load();

    private Algorithm getAlgorithm() {
        String secret;
        if (dotenv.get("JWT_SECRET").isEmpty()) secret = this.jwtSecret;
        else secret = dotenv.get("JWT_SECRET");
        return Algorithm.HMAC256(secret);
    }

    @Bean
    public JwtDecoder jwkDecoder() {
        return NimbusJwtDecoder
                .withJwkSetUri("https://www.googleapis.com/oauth2/v3/certs")
                .build();
    }

    public String jwtEncoder(Map<String, Object> payload) {
        long expiry = 36000L;
        Instant now = Instant.now();
        System.out.println("asd " + payload);
        return JWT.create()
                .withIssuer("auth0")
                .withExpiresAt(Date.from(now.plusSeconds(expiry)))
                .withPayload(payload)
                .sign(this.getAlgorithm());
    }

    public DecodedJWT jwtDecoder(String token) {
        JWTVerifier verifier = JWT.require(this.getAlgorithm())
                .withIssuer("auth0")
                .build();
        return verifier.verify(token);
    }

    public String getToken(Account account) {
        JWTDto jwtDto = jwtMapper.toJwtDto(account);
        return this.jwtEncoder(jwtDto.getPayload());
    }
}
