package com.cosmetic.cosmetic.component;

import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@Component
public class JwtFilter extends OncePerRequestFilter {
    @Autowired
    private JwtComponent jwtComponent;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    @NonNull HttpServletResponse response,
                                    @NonNull FilterChain filterChain) throws ServletException, IOException {
        String header = request.getHeader("Authorization");
        System.out.println("header " + header);

        if (header == null || !header.startsWith("Bearer ")) {
            filterChain.doFilter(request, response);
        } else {
            String token = header.split(" ")[1].trim();
            DecodedJWT decodedToken = jwtComponent.jwtDecoder(token);
            if (decodedToken.getIssuer().isEmpty()) {
                filterChain.doFilter(request, response);
            }
            System.out.println("woy " + decodedToken.getClaims());
            SimpleGrantedAuthority role =
                    new SimpleGrantedAuthority(decodedToken.getClaims().get("role").asString());
            GrantedAuthority[] grantedAuthorities = {role};
            UsernamePasswordAuthenticationToken credential =
                    new UsernamePasswordAuthenticationToken(
                            decodedToken.getClaims(),
                            null,
                            List.of(grantedAuthorities)
                    );
            SecurityContextHolder.getContext().setAuthentication(credential);
            filterChain.doFilter(request, response);
        }
    }
}
