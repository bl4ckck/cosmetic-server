package com.cosmetic.cosmetic.component;

import com.midtrans.Config;
import com.midtrans.ConfigFactory;
import com.midtrans.httpclient.CoreApi;
import com.midtrans.httpclient.SnapApi;
import com.midtrans.httpclient.error.MidtransError;
import com.midtrans.service.MidtransCoreApi;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class MidtransComponent {
    @Autowired
    private Config config;

    public JSONObject snapTransaction(Map<String, Object> payload) throws MidtransError {
        return SnapApi.createTransaction(payload, config);
    }
    public JSONObject coreTransaction(Map<String, Object> payload) throws MidtransError {
        return CoreApi.chargeTransaction(payload, config);
    }

    private MidtransCoreApi getCoreApi() {
        return new ConfigFactory(config).getCoreApi();
    }

    public JSONObject getStatusTransaction(String orderId) throws MidtransError {
        return getCoreApi().checkTransaction(orderId);
    }
}
