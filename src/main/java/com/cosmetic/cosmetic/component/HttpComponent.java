package com.cosmetic.cosmetic.component;

import okhttp3.*;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class HttpComponent {
    OkHttpClient okHttpClient = new OkHttpClient();

    public static final MediaType JSON = MediaType.get("application/json; charset=utf-8");

    public static final MediaType URL_ENCODED = MediaType.parse("application/x-www-form-urlencoded");


    public String callGet(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();
        try (Response response = okHttpClient.newCall(request).execute()) {
            assert response.body() != null;
            return response.body().string();
        }
    }

    public String callGetWithHeader(String url, String key, String value) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .addHeader(key, value)
                .build();
        return this.responseString(request);
    }

    private String responseString(Request request) throws IOException {
        try (Response response = okHttpClient.newCall(request).execute()) {
            assert response.body() != null;
            return response.body().string();
        }
    }

    public String callPost(String url, String payload) throws IOException {
        RequestBody requestBody = RequestBody.create(JSON, payload);
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        try (Response response = okHttpClient.newCall(request).execute()) {
            assert response.body() != null;
            return response.body().string();
        }
    }

    public String callPostWithHeader(String url, String payload, String key, String value) throws IOException {
        RequestBody requestBody = RequestBody.create(URL_ENCODED, payload);
        Request request = new Request.Builder()
                .url(url)
                .addHeader(key, value)
                .addHeader("content-type", "application/x-www-form-urlencoded")
                .post(requestBody)
                .build();
        try (Response response = okHttpClient.newCall(request).execute()) {
            assert response.body() != null;
            return response.body().string();
        }
    }
}
