package com.cosmetic.cosmetic.mapper.order;

import com.cosmetic.cosmetic.dto.cart.CartDto;
import com.cosmetic.cosmetic.dto.midtrans.CoreResponseDto;
import com.cosmetic.cosmetic.dto.order.ItemDetailDto;
import com.cosmetic.cosmetic.dto.order.ItemSaveDto;
import com.cosmetic.cosmetic.entity.OrderVariant;
import com.cosmetic.cosmetic.entity.Variant;
import com.cosmetic.cosmetic.mapper.cart.CartInfo;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface CheckoutMapper {
    CheckoutMapper INSTANCE = Mappers.getMapper( CheckoutMapper.class );

    @Mapping(source = "variantId", target = "id")
    Variant toVariant(ItemSaveDto itemSaveDto);

    List<ItemDetailDto> toListItemDetailDto(List<Variant> variantList);

    CartDto toCartDto(CartInfo cart);

    @Mapping(source = "variant.id", target = "id")
    @Mapping(source = "variant.name", target = "name")
    @Mapping(source = "variant.price", target = "price")
    ItemDetailDto toItemDetailFromVariant(OrderVariant variantSet);
    List<ItemDetailDto> toItemDetailListFromVariantSet(Set<OrderVariant> variantSet);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void mergeToVariant(ItemSaveDto dto, @MappingTarget Variant entity);

    default CoreResponseDto toCoreResponse(Map<String, Object> map) throws ParseException {
        CoreResponseDto result = new CoreResponseDto();
        SimpleDateFormat dateFormat =
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("id", "ID"));

        result.setOrderId((String) map.get("order_id"));
        result.setGrossAmount(Double.valueOf((String) map.get("gross_amount")));
        result.setPaymentType((String) map.get("payment_type"));
        result.setTransactionId((String) map.get("transaction_id"));
        result.setTransactionTime(dateFormat.parse((String) map.get("transaction_time")));
        result.setTransactionStatus((String) map.get("transaction_status"));
        return result;
    }
}
