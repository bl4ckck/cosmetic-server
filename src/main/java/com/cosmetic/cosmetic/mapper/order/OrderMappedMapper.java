package com.cosmetic.cosmetic.mapper.order;

import com.cosmetic.cosmetic.dto.order.OrderDetailDto;
import com.cosmetic.cosmetic.dto.order.OrderDto;
import com.cosmetic.cosmetic.dto.order.OrderMappedDto;
import com.cosmetic.cosmetic.entity.Order;
import com.cosmetic.cosmetic.mapper.BasicMapper;
import com.cosmetic.cosmetic.mapper.address.AddressMapper;
import com.cosmetic.cosmetic.mapper.ordervariant.OrderVariantMapper;
import com.cosmetic.cosmetic.mapper.user.UserMapper;
import com.cosmetic.cosmetic.mapper.variant.VariantMapper;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring",
        uses = { VariantMapper.class, UserMapper.class, AddressMapper.class, OrderVariantMapper.class })
public interface OrderMappedMapper extends BasicMapper<Order, OrderMappedDto, OrderDto, OrderDto> {
    OrderMappedMapper INSTANCE = Mappers.getMapper( OrderMappedMapper.class );

    OrderMappedDto toDto(Order order);

    List<OrderMappedDto> toList(List<Order> orders);

    OrderDetailDto toDetailDto(Order order);

    List<OrderDetailDto> toDetailList(List<Order> orders);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void create(OrderDto dto, @MappingTarget Order entity);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void update(OrderDto dto, @MappingTarget Order entity);
}
