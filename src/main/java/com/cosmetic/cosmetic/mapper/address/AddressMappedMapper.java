package com.cosmetic.cosmetic.mapper.address;

import com.cosmetic.cosmetic.entity.Address;
import com.cosmetic.cosmetic.dto.address.AddressDto;
import com.cosmetic.cosmetic.dto.address.AddressMappedDto;
import com.cosmetic.cosmetic.dto.address.AddressSaveDto;
import com.cosmetic.cosmetic.mapper.BasicMapper;
import com.cosmetic.cosmetic.mapper.user.UserMapper;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring",
        uses = { UserMapper.class })
public interface AddressMappedMapper extends BasicMapper<Address, AddressMappedDto, AddressDto, AddressSaveDto> {
    @Override
    AddressMappedDto toDto(Address entity);

    @Override
    List<AddressMappedDto> toList(List<Address> entity);

    @Override
    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void create(AddressSaveDto saveDto,  @MappingTarget Address entity);

    @Override
    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void update(AddressDto updateDto,  @MappingTarget Address entity);
}
