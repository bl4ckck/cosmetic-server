package com.cosmetic.cosmetic.mapper.address;

import com.cosmetic.cosmetic.dto.address.AddressSaveDto;
import com.cosmetic.cosmetic.entity.Address;
import com.cosmetic.cosmetic.dto.address.AddressDto;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface AddressMapper {
    AddressMapper INSTANCE = Mappers.getMapper(AddressMapper.class);

    AddressDto toDto(Address address);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void saveUserFromDto(AddressSaveDto addressSaveDtoSaveDto, @MappingTarget Address entity);
}
