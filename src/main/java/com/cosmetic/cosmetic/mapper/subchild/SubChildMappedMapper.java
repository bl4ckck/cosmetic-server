package com.cosmetic.cosmetic.mapper.subchild;

import com.cosmetic.cosmetic.dto.subchild.SubChildDto;
import com.cosmetic.cosmetic.dto.subchild.SubChildMappedDto;
import com.cosmetic.cosmetic.dto.subchild.SubChildSaveDto;
import com.cosmetic.cosmetic.entity.SubChild;
import com.cosmetic.cosmetic.mapper.product.ProductMapper;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;
import java.util.List;

@Mapper(componentModel = "spring",
        uses = { ProductMapper.class })
public interface SubChildMappedMapper {
    SubChildMappedMapper INSTANCE = Mappers.getMapper(SubChildMappedMapper.class);

    SubChildMappedDto toDto(SubChild subChild);
    List<SubChildMappedDto>toList(List<SubChild> subCategories);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void create(SubChildSaveDto dto, @MappingTarget SubChild entity);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    @Mapping(source = "subCategory", target = "subCategory.name")
    void update(SubChildDto dto , @MappingTarget SubChild entity);
}