package com.cosmetic.cosmetic.mapper.subchild;

import com.cosmetic.cosmetic.dto.article.ArticleCategoriesDto;
import com.cosmetic.cosmetic.dto.subchild.SubChildDto;
import com.cosmetic.cosmetic.dto.subchild.SubChildSaveDto;
import com.cosmetic.cosmetic.entity.SubChild;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface SubChildMapper {
    SubChildMapper INSTANCE = Mappers.getMapper(SubChildMapper.class);

    @Mapping(source = "subCategory.name", target = "subCategory")
    @Mapping(source = "subCategory.id", target = "subCategoryId")
    @Mapping(source = "subCategory.category.id", target = "categoryId")
    @Mapping(source = "subCategory.category.name", target ="category" )
    @Mapping(source = "id", target = "subChildId")
    @Mapping(source = "name", target = "subChild")
    SubChildDto toDto(SubChild subChild);

    SubChildSaveDto toSaveDto(SubChild subChild);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void mergeToSubchild(ArticleCategoriesDto dto, @MappingTarget SubChild entity);
}
