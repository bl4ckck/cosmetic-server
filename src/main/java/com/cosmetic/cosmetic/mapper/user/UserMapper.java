package com.cosmetic.cosmetic.mapper.user;

import com.cosmetic.cosmetic.dto.user.UserDto;
import com.cosmetic.cosmetic.dto.user.UserSaveDto;
import com.cosmetic.cosmetic.entity.User;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper( UserMapper.class );

    UserDto toDto(User user);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void saveUserFromDto(UserSaveDto userSaveDto, @MappingTarget User entity);
}
