package com.cosmetic.cosmetic.mapper.user;

import com.cosmetic.cosmetic.dto.user.UserDto;
import com.cosmetic.cosmetic.dto.user.UserMappedDto;
import com.cosmetic.cosmetic.dto.user.UserSaveDto;
import com.cosmetic.cosmetic.entity.User;
import com.cosmetic.cosmetic.mapper.BasicMapper;
import com.cosmetic.cosmetic.mapper.account.AccountMapper;
import com.cosmetic.cosmetic.mapper.address.AddressMapper;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring",
        uses = { AccountMapper.class, AddressMapper.class })
public interface UserMappedMapper extends BasicMapper<User, UserMappedDto, UserDto, UserSaveDto> {
    UserMappedMapper INSTANCE = Mappers.getMapper( UserMappedMapper.class );

    @Override
    UserMappedDto toDto(User user);
    
    @Override
    List<UserMappedDto> toList(List<User> accounts);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void create(UserSaveDto dto, @MappingTarget User entity);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void update(UserDto dto, @MappingTarget User entity);
}
