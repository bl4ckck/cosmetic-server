package com.cosmetic.cosmetic.mapper;

public interface ToDtoMapper<E, D> {
    D toDto(E entity);
}
