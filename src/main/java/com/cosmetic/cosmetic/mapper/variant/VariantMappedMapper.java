package com.cosmetic.cosmetic.mapper.variant;

import com.cosmetic.cosmetic.dto.variant.VariantDto;
import com.cosmetic.cosmetic.dto.variant.VariantMappedDto;
import com.cosmetic.cosmetic.dto.variant.VariantSaveDto;
import com.cosmetic.cosmetic.entity.Variant;
import com.cosmetic.cosmetic.mapper.BasicMapper;
import com.cosmetic.cosmetic.mapper.product.ProductMapper;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring",
        uses = { ProductMapper.class })
public interface VariantMappedMapper extends BasicMapper<Variant, VariantMappedDto, VariantDto, VariantSaveDto> {
    VariantMappedMapper INSTANCE = Mappers.getMapper( VariantMappedMapper.class );

    VariantMappedDto toDto(Variant variant);

    List<VariantMappedDto> toList(List<Variant> variants);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void create(VariantSaveDto dto, @MappingTarget Variant entity);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void update(VariantDto dto, @MappingTarget Variant entity);
}
