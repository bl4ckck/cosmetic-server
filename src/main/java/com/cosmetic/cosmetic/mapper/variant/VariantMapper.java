package com.cosmetic.cosmetic.mapper.variant;

import com.cosmetic.cosmetic.dto.variant.VariantDto;
import com.cosmetic.cosmetic.entity.Variant;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface VariantMapper {
    VariantMapper INSTANCE = Mappers.getMapper( VariantMapper.class );

    VariantDto toDto(Variant variant);
}
