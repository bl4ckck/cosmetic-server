package com.cosmetic.cosmetic.mapper.rajaongkir;

import com.cosmetic.cosmetic.dto.rajaongkir.ROCity;
import com.cosmetic.cosmetic.dto.rajaongkir.ROProvince;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.Map;

@Mapper(componentModel = "spring")
public interface RajaOngkirMapper {
    RajaOngkirMapper INSTANCE = Mappers.getMapper( RajaOngkirMapper.class );

    default ROProvince toProvince(Map<String, Object> map){
        ROProvince result = new ROProvince();
        result.setProvinceId((String) map.get("province_id"));
        result.setProvince((String) map.get("province"));
        return result;
    }
    List<ROProvince> toProvinceList(List<Map<String, Object>> mapList);

    default ROCity toCity(Map<String, Object> map){
        ROCity result = new ROCity();
        result.setCityId((String) map.get("city_id"));
        result.setProvinceId((String) map.get("province_id"));
        result.setProvince((String) map.get("province"));
        result.setType((String) map.get("type"));
        result.setCityName((String) map.get("city_name"));
        result.setPostalCode((String) map.get("postal_code"));
        return result;
    }
    List<ROCity> toCityList(List<Map<String, Object>> mapList);
}
