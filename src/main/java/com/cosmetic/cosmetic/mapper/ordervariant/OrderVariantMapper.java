package com.cosmetic.cosmetic.mapper.ordervariant;

import com.cosmetic.cosmetic.dto.ordervariant.OrderVariantDto;
import com.cosmetic.cosmetic.dto.variant.VariantDto;
import com.cosmetic.cosmetic.entity.OrderVariant;
import com.cosmetic.cosmetic.entity.Variant;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface OrderVariantMapper {
    OrderVariantMapper INSTANCE = Mappers.getMapper( OrderVariantMapper.class );

    OrderVariantDto toDto(OrderVariant orderVariant);
}
