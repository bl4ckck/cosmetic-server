package com.cosmetic.cosmetic.mapper.brand;

import com.cosmetic.cosmetic.dto.brand.BrandMappedDto;
import com.cosmetic.cosmetic.dto.brand.BrandDto;
import com.cosmetic.cosmetic.dto.brand.BrandSaveDto;
import com.cosmetic.cosmetic.entity.Brand;
import com.cosmetic.cosmetic.mapper.BasicMapper;
import com.cosmetic.cosmetic.mapper.product.ProductMapper;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring",
        uses = { ProductMapper.class })
public interface BrandMappedMapper extends BasicMapper<Brand, BrandMappedDto, BrandDto, BrandSaveDto> {
    BrandMappedMapper INSTANCE = Mappers.getMapper( BrandMappedMapper.class );

    @Mapping(source = "products", target = "product")
    BrandMappedDto toDto(Brand brand);

    @Mapping(source = "products", target = "product")
    List<BrandMappedDto> toList(List<Brand> brands);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void create(BrandSaveDto dto, @MappingTarget Brand entity);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void update(BrandDto dto, @MappingTarget Brand entity);
}
