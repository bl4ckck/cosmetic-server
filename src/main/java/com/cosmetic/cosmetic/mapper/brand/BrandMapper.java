package com.cosmetic.cosmetic.mapper.brand;

import com.cosmetic.cosmetic.dto.brand.BrandDto;
import com.cosmetic.cosmetic.entity.Brand;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface BrandMapper {
    BrandMapper INSTANCE = Mappers.getMapper( BrandMapper.class );

    BrandDto toDto(Brand brand);
}
