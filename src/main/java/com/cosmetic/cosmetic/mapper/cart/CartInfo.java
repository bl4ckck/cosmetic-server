package com.cosmetic.cosmetic.mapper.cart;

import com.cosmetic.cosmetic.entity.CartVariantKey;
import com.cosmetic.cosmetic.entity.EDelivery;

import java.util.Set;
import java.util.UUID;

public interface CartInfo {
    UUID getId();

    UUID[] getPromo();

    EDelivery getDelivery();

    AddressInfo getAddress();

    Double getTotal();

    Double getWeight();

    Set<CartVariantInfo> getCarts();

    interface CartVariantInfo {
        CartVariantKey getId();

        VariantInfo getVariant();

        Boolean getIsChecked();

        Integer getQuantity();

        Double getSubTotal();
    }

    interface AddressInfo {
        UUID getId();

        String getPhone();

        String getAddressDetail();

        String getLabel();
    }

    interface VariantInfo {
        UUID getId();
        ProductInfo getProduct();
        String getName();
        Double getPrice();
        Integer getQuantity();
        Integer getImageIndex();
    }

    interface ProductInfo {
        UUID getId();
        BrandInfo getBrand();
        String getName();
        String[] getImages();
    }

    interface BrandInfo {
        UUID getId();
        String getName();
        String getBanner();
    }
}
