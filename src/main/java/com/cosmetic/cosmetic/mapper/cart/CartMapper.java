package com.cosmetic.cosmetic.mapper.cart;

import com.cosmetic.cosmetic.dto.cart.CartDto;
import com.cosmetic.cosmetic.entity.Cart;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface CartMapper {
    CartMapper INSTANCE = Mappers.getMapper( CartMapper.class );

    CartDto toDto(Cart cart);
}
