package com.cosmetic.cosmetic.mapper.cart;

import com.cosmetic.cosmetic.dto.cart.CartDto;
import com.cosmetic.cosmetic.dto.cart.CartItemDto;
import com.cosmetic.cosmetic.dto.cart.CartMappedDto;
import com.cosmetic.cosmetic.dto.cart.CartSaveDto;
import com.cosmetic.cosmetic.entity.Cart;
import com.cosmetic.cosmetic.entity.OrderVariant;
import com.cosmetic.cosmetic.mapper.BasicMapper;
import com.cosmetic.cosmetic.mapper.cartvariant.CartVariantMapper;
import com.cosmetic.cosmetic.mapper.user.UserMapper;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring",
        uses = { UserMapper.class, CartVariantMapper.class }, builder = @Builder(disableBuilder = true))
public interface CartMappedMapper extends BasicMapper<Cart, CartMappedDto, CartDto, CartSaveDto> {
    CartMappedMapper INSTANCE = Mappers.getMapper( CartMappedMapper.class );

    CartMappedDto toDto(Cart cart);

    CartDto toCartDto(CartInfo cart);

    List<CartMappedDto> toList(List<Cart> carts);

    @BeforeMapping
    default void mapVariantImage(CartInfo.CartVariantInfo variantInfo, @MappingTarget CartItemDto cartItemDto) {
        String[] productImages = variantInfo.getVariant().getProduct().getImages();
        Integer imageIndex = variantInfo.getVariant().getImageIndex();
        cartItemDto.setImage(productImages[imageIndex]);
    }

    @BeforeMapping
    default void mapVariantImageOrder(OrderVariant orderVariant, @MappingTarget CartItemDto cartItemDto) {
        String[] productImages = orderVariant.getVariant().getProduct().getImages();
        Integer imageIndex = orderVariant.getVariant().getImageIndex();
        cartItemDto.setImage(productImages[imageIndex]);
    }

    @Mapping(source = "variant.id", target = "variantId")
    @Mapping(source = "variant.product.id", target = "productId")
    @Mapping(source = "variant.price", target = "price")
    @Mapping(source = "variant.name", target = "name")
    CartItemDto toCartItemDto(CartInfo.CartVariantInfo variantInfo);

    List<CartItemDto> toListCartItemDto(List<CartInfo.CartVariantInfo> variantInfo);

    @Mapping(source = "variant.id", target = "variantId")
    @Mapping(source = "variant.product.id", target = "productId")
    @Mapping(source = "variant.price", target = "price")
    @Mapping(source = "variant.name", target = "name")
    CartItemDto toOrderItem(OrderVariant variant);

    List<CartItemDto> toListOrderItem(List<OrderVariant> variantInfo);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void create(CartSaveDto dto, @MappingTarget Cart entity);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void update(CartDto dto, @MappingTarget Cart entity);
}
