package com.cosmetic.cosmetic.mapper.cart;

import java.util.UUID;

public interface CartAddressInfo {
    AddressInfo getAddress();

    interface AddressInfo {
        UUID getId();

        String getPhone();

        String getReceiver();

        String getLabel();

        Boolean getIsDefault();

        String getCityId();

        String getPostalCode();

        String getAddressDetail();
    }
}
