package com.cosmetic.cosmetic.mapper.product;

import com.cosmetic.cosmetic.dto.product.ProductDto;
import com.cosmetic.cosmetic.dto.product.ProductSaveDto;
import com.cosmetic.cosmetic.entity.Product;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ProductMapper {
    ProductMapper INSTANCE = Mappers.getMapper( ProductMapper.class );

    ProductDto toDto(Product product);
    ProductSaveDto toProductSaveDto(Product product);
}
