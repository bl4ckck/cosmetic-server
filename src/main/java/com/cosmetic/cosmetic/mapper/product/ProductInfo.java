package com.cosmetic.cosmetic.mapper.product;

import java.util.Set;
import java.util.UUID;

public interface ProductInfo {
    ProductDetail getProduct();

    Double getAverage();

    interface ProductDetail {
        UUID getId();

        String getName();

        String[] getImages();

        Boolean getIsOrganic();

        BrandInfo getBrand();

        Set<VariantInfo> getVariants();
    }

    interface BrandInfo {
        UUID getId();

        String getName();

        String getDescription();

        String getLogo();

        String getBanner();
    }

    interface VariantInfo {
        UUID getId();

        String getName();

        Double getPrice();

        Integer getQuantity();

        Integer getImageIndex();
    }
}
