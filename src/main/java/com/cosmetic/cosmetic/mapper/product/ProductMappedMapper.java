package com.cosmetic.cosmetic.mapper.product;

import com.cosmetic.cosmetic.dto.product.ProductDto;
import com.cosmetic.cosmetic.dto.product.ProductMappedDto;
import com.cosmetic.cosmetic.dto.product.ProductSaveDto;
import com.cosmetic.cosmetic.dto.product.ProductTrendingDto;
import com.cosmetic.cosmetic.entity.Product;
import com.cosmetic.cosmetic.mapper.BasicMapper;
import com.cosmetic.cosmetic.mapper.brand.BrandMapper;
import com.cosmetic.cosmetic.mapper.variant.VariantMapper;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;

import java.util.List;

@Mapper(componentModel = "spring",
        uses = { BrandMapper.class, VariantMapper.class }, builder = @Builder(disableBuilder = true))
public interface ProductMappedMapper extends BasicMapper<Product, ProductMappedDto, ProductDto, ProductSaveDto> {
    ProductMappedMapper INSTANCE = Mappers.getMapper( ProductMappedMapper.class );

    @Override
    @Mapping(source = "variants", target = "variant")
    ProductMappedDto toDto(Product products);

    // product trending mapper
    @Mapping(source = "product.id", target = "id")
    @Mapping(source = "product.brand", target = "brand")
    @Mapping(source = "product.variants", target = "variant")
    @Mapping(source = "product.name", target = "name")
    @Mapping(source = "product.images", target = "images")
    @Mapping(source = "product.isOrganic", target = "isOrganic")
    ProductTrendingDto toTrendingDto(ProductInfo productInfo);

    List<ProductTrendingDto> toTrendingListDto(List<ProductInfo> productInfo);

    @Override
    List<ProductMappedDto> toList(List<Product> products);
    List<ProductMappedDto> toListFromPage(Page<Product> productsPaginate);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void create(ProductSaveDto dto, @MappingTarget Product entity);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void update(ProductDto dto, @MappingTarget Product entity);
}
