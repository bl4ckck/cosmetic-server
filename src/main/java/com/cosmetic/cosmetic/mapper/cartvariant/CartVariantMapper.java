package com.cosmetic.cosmetic.mapper.cartvariant;

import com.cosmetic.cosmetic.dto.cartvariant.CartVariantDto;
import com.cosmetic.cosmetic.entity.CartVariant;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface CartVariantMapper {
    CartVariantMapper INSTANCE = Mappers.getMapper( CartVariantMapper.class );

    CartVariantDto toDto(CartVariant cartVariant);
}
