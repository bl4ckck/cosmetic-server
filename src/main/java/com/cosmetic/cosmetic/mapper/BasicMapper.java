package com.cosmetic.cosmetic.mapper;

import org.mapstruct.BeanMapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

import java.util.List;

/**
 * @param <E> Entity
 * @param <D> Dto object
 * @param <U> Update dto object
 * @param <S> Save dto object
 */
public interface BasicMapper<E, D, U, S> extends ToDtoMapper<E, D> {
    D toDto(E entity);
    List<D> toList(List<E> entity);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void create(S saveDto, @MappingTarget E entity);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void update(U updateDto, @MappingTarget E entity);
}
