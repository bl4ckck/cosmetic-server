package com.cosmetic.cosmetic.mapper.promo;

import com.cosmetic.cosmetic.dto.promo.PromoMappedDto;
import com.cosmetic.cosmetic.dto.promo.PromoSaveDto;
import com.cosmetic.cosmetic.entity.Promo;
import com.cosmetic.cosmetic.mapper.BasicMapper;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PromoMappedMapper extends BasicMapper<Promo, PromoMappedDto, PromoMappedDto, PromoSaveDto> {
    PromoMappedMapper INSTANCE = Mappers.getMapper( PromoMappedMapper.class );

    @Override
    PromoMappedDto toDto(Promo promo);
    
    @Override
    List<PromoMappedDto> toList(List<Promo> accounts);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void create(PromoSaveDto dto, @MappingTarget Promo entity);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void update(PromoSaveDto dto, @MappingTarget Promo entity);
}
