package com.cosmetic.cosmetic.mapper.auth;

import com.cosmetic.cosmetic.dto.JWTDto;
import com.cosmetic.cosmetic.entity.Account;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface JwtMapper {
    JwtMapper INSTANCE = Mappers.getMapper(JwtMapper.class);

    @Mapping(source = "id", target = "accountId")
    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.name", target = "name")
    JWTDto toJwtDto(Account account);
}
