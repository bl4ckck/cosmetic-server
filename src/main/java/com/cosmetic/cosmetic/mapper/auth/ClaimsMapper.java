package com.cosmetic.cosmetic.mapper.auth;

import com.cosmetic.cosmetic.dto.account.AccountUserDto;
import com.cosmetic.cosmetic.mapper.user.UserMapper;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.Map;

@Mapper(componentModel = "spring", uses = { UserMapper.class })
public interface ClaimsMapper {
    ClaimsMapper INSTANCE = Mappers.getMapper(ClaimsMapper.class);

    @Mapping(source = "name", target = "user.name")
    @Mapping(source = "picture", target = "avatar")
    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void toAccountUser(Map<String, String> claims, @MappingTarget AccountUserDto accountUserDto);
}
