package com.cosmetic.cosmetic.mapper.account.provider;

import com.cosmetic.cosmetic.dto.account.provider.AccountProviderDto;
import com.cosmetic.cosmetic.entity.AccountProvider;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface AccountProviderMapper {
    AccountProviderMapper INSTANCE = Mappers.getMapper( AccountProviderMapper.class );

    AccountProviderDto toDto(AccountProvider accountProvider);
}
