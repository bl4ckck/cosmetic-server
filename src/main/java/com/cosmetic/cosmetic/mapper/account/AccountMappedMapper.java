package com.cosmetic.cosmetic.mapper.account;

import com.cosmetic.cosmetic.dto.account.AccountMappedDto;
import com.cosmetic.cosmetic.dto.account.AccountSaveDto;
import com.cosmetic.cosmetic.dto.account.AccountUpdateDto;
import com.cosmetic.cosmetic.dto.account.AccountUserDto;
import com.cosmetic.cosmetic.entity.Account;
import com.cosmetic.cosmetic.mapper.BasicMapper;
import com.cosmetic.cosmetic.mapper.account.basic.AccountBasicMapper;
import com.cosmetic.cosmetic.mapper.account.provider.AccountProviderMapper;
import com.cosmetic.cosmetic.mapper.user.UserMapper;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring",
        uses = { UserMapper.class, AccountBasicMapper.class, AccountProviderMapper.class })
public interface AccountMappedMapper extends BasicMapper<Account, AccountMappedDto, AccountUpdateDto, AccountSaveDto> {
    AccountMappedMapper INSTANCE = Mappers.getMapper( AccountMappedMapper.class );

    AccountMappedDto toDto(Account account);
    List<AccountMappedDto> toList(List<Account> accounts);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void create(AccountSaveDto dto, @MappingTarget Account entity);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void createAccountProvider(AccountUserDto dto, @MappingTarget Account entity);

    @Mapping(source = "password", target = "accountBasic.password")
    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void update(AccountUpdateDto dto, @MappingTarget Account entity);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void updateNoPassword(AccountUpdateDto dto, @MappingTarget Account entity);
}
