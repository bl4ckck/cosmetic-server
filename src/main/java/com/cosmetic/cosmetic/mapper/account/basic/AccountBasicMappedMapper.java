package com.cosmetic.cosmetic.mapper.account.basic;

import com.cosmetic.cosmetic.dto.account.basic.AccountBasicDto;
import com.cosmetic.cosmetic.dto.account.basic.AccountBasicMappedDto;
import com.cosmetic.cosmetic.dto.account.basic.AccountBasicSaveDto;
import com.cosmetic.cosmetic.entity.AccountBasic;
import com.cosmetic.cosmetic.mapper.BasicMapper;
import com.cosmetic.cosmetic.mapper.account.AccountMapper;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring",
        uses = { AccountMapper.class })
public interface AccountBasicMappedMapper extends BasicMapper<AccountBasic, AccountBasicMappedDto,
        AccountBasicDto, AccountBasicSaveDto> {
    AccountBasicMappedMapper INSTANCE = Mappers.getMapper( AccountBasicMappedMapper.class );

    AccountBasicMappedDto toDto(AccountBasic accountBasic);
    List<AccountBasicMappedDto> toList(List<AccountBasic> accountBasics);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void create(AccountBasicSaveDto dto, @MappingTarget AccountBasic entity);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void update(AccountBasicDto dto, @MappingTarget AccountBasic entity);
}
