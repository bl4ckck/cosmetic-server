package com.cosmetic.cosmetic.mapper.account;

import com.cosmetic.cosmetic.dto.account.AccountDto;
import com.cosmetic.cosmetic.entity.Account;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface AccountMapper {
    AccountMapper INSTANCE = Mappers.getMapper( AccountMapper.class );

    AccountDto toDto(Account account);
}
