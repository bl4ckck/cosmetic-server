package com.cosmetic.cosmetic.mapper.account.provider;

import com.cosmetic.cosmetic.dto.account.provider.AccountProviderDto;
import com.cosmetic.cosmetic.dto.account.provider.AccountProviderMappedDto;
import com.cosmetic.cosmetic.dto.account.provider.AccountProviderSaveDto;
import com.cosmetic.cosmetic.entity.AccountProvider;
import com.cosmetic.cosmetic.mapper.BasicMapper;
import com.cosmetic.cosmetic.mapper.account.AccountMapper;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring",
        uses = { AccountMapper.class })
public interface AccountProviderMappedMapper extends BasicMapper<AccountProvider, AccountProviderMappedDto,
        AccountProviderDto, AccountProviderSaveDto> {
    AccountProviderMappedMapper INSTANCE = Mappers.getMapper( AccountProviderMappedMapper.class );

    AccountProviderMappedDto toDto(AccountProvider accountProvider);
    List<AccountProviderMappedDto> toList(List<AccountProvider> accountProviders);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void create(AccountProviderSaveDto dto, @MappingTarget AccountProvider entity);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void update(AccountProviderDto dto, @MappingTarget AccountProvider entity);
}