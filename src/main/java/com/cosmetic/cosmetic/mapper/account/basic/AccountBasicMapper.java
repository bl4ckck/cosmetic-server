package com.cosmetic.cosmetic.mapper.account.basic;

import com.cosmetic.cosmetic.dto.account.basic.AccountBasicDto;
import com.cosmetic.cosmetic.entity.AccountBasic;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface AccountBasicMapper {
    AccountBasicMapper INSTANCE = Mappers.getMapper( AccountBasicMapper.class );

    AccountBasicDto toDto(AccountBasic accountBasic);
}
