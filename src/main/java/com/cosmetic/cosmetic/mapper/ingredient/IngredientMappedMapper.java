package com.cosmetic.cosmetic.mapper.ingredient;

import com.cosmetic.cosmetic.dto.ingredient.IngredientMappedDto;
import com.cosmetic.cosmetic.entity.Ingredient;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring",
        uses = {IngredientMapper.class})
public interface IngredientMappedMapper {
    IngredientMappedMapper INSTANCE = Mappers.getMapper( IngredientMappedMapper.class);

    IngredientMappedDto toMappedDto(Ingredient ingredient);
}