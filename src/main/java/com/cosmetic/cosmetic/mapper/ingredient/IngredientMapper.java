package com.cosmetic.cosmetic.mapper.ingredient;

import com.cosmetic.cosmetic.dto.article.ArticleIngredientsDto;
import com.cosmetic.cosmetic.dto.ingredient.IngredientDto;
import com.cosmetic.cosmetic.dto.ingredient.IngredientSaveDto;
import com.cosmetic.cosmetic.entity.Ingredient;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface IngredientMapper {
    IngredientMapper INSTANCE = Mappers.getMapper(IngredientMapper.class);

    @Mapping(source = "id", target = "ingredientId")
    IngredientDto toDto(Ingredient ingredient);

    Ingredient saveDtoToEntity(IngredientSaveDto saveDto);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void create(IngredientDto dto, @MappingTarget Ingredient entity);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void mergeToIngredient(ArticleIngredientsDto dto, @MappingTarget Ingredient entity);
}
