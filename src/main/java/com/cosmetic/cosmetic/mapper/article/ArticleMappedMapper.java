package com.cosmetic.cosmetic.mapper.article;

import com.cosmetic.cosmetic.dto.article.ArticleDto;
import com.cosmetic.cosmetic.dto.article.ArticleMappedDto;
import com.cosmetic.cosmetic.dto.article.ArticleSaveDto;
import com.cosmetic.cosmetic.entity.Article;
import com.cosmetic.cosmetic.mapper.BasicMapper;
import com.cosmetic.cosmetic.mapper.ingredient.IngredientMapper;
import com.cosmetic.cosmetic.mapper.subchild.SubChildMapper;
import com.cosmetic.cosmetic.mapper.user.UserMapper;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;
import java.util.List;

@Mapper(componentModel = "spring",
        uses = {ArticleMapper.class, SubChildMapper.class, IngredientMapper.class, UserMapper.class})
public interface ArticleMappedMapper extends BasicMapper<Article, ArticleMappedDto, ArticleDto, ArticleSaveDto> {
    ArticleMappedMapper INSTANCE = Mappers.getMapper( ArticleMappedMapper.class );

    @Mapping(source = "articleIngredients", target = "ingredientCategories")
    @Mapping(source = "articleCategories", target = "productCategories")
    @Mapping(source = "user.id", target = "userid")
    ArticleMappedDto toDto(Article entity);

    Article toEntity(ArticleSaveDto articleSaveDto);

    List<ArticleMappedDto> toList(List<Article> entity);

    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)

    @Mapping(source = "userid", target = "user.id")
    @Mapping(source = "productCategories", target = "articleCategories")
    @Mapping(source = "ingredientCategories", target = "articleIngredients")
    void create(ArticleSaveDto saveDto, @MappingTarget Article entity);

    @Override
    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void update(ArticleDto updateDto, @MappingTarget Article entity);
}