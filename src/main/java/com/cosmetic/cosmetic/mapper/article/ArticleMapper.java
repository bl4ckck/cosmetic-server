package com.cosmetic.cosmetic.mapper.article;

import com.cosmetic.cosmetic.dto.article.ArticleDto;
import com.cosmetic.cosmetic.entity.Article;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ArticleMapper {
    ArticleMapper INSTANCE = Mappers.getMapper(ArticleMapper.class);
    ArticleDto toDto(Article article);
}