package com.cosmetic.cosmetic.mapper.review;

import com.cosmetic.cosmetic.dto.review.ReviewDto;
import com.cosmetic.cosmetic.dto.review.ReviewSaveDto;
import com.cosmetic.cosmetic.entity.Review;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ReviewMapper {
    ReviewMapper INSTANCE = Mappers.getMapper(ReviewMapper.class);

    Review saveDtoToEntity(ReviewSaveDto saveDto);

    ReviewDto toDto(Review review);
}
