package com.cosmetic.cosmetic.mapper.review;

import com.cosmetic.cosmetic.dto.review.ReviewDto;
import com.cosmetic.cosmetic.dto.review.ReviewMappedDto;
import com.cosmetic.cosmetic.dto.review.ReviewSaveDto;
import com.cosmetic.cosmetic.entity.Review;
import com.cosmetic.cosmetic.mapper.BasicMapper;
import com.cosmetic.cosmetic.mapper.product.ProductMapper;
import org.mapstruct.*;
import org.springframework.data.domain.Page;

import java.util.List;

@Mapper(componentModel = "spring",
        uses = {ProductMapper.class} )
public interface ReviewMappedMapper extends BasicMapper<Review, ReviewMappedDto, ReviewDto, ReviewSaveDto> {
    @Override
    @Mapping(source = "product.id", target = "productId")
    @Mapping(source = "user", target = "user")
    @Mapping(source = "user.account.avatar", target = "user.avatar")
    ReviewMappedDto toDto(Review entity);

    @Override
    List<ReviewMappedDto> toList(List<Review> entity);
    List<ReviewMappedDto> toListFromPage(Page<Review> reviewPaginate);

    @Override
    @Mapping(source = "productId", target = "product.id")
    @Mapping(source = "userId", target = "user.id")
    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void create(ReviewSaveDto saveDto,  @MappingTarget Review entity);

    @Override
    @Mapping(source = "productId", target = "product.id")
    @Mapping(source = "userId", target = "user.id")
    @BeanMapping(nullValuePropertyMappingStrategy =
            NullValuePropertyMappingStrategy.IGNORE)
    void update(ReviewDto updateDto,  @MappingTarget Review entity);
}
