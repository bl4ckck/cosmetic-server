package com.cosmetic.cosmetic.mapper.role;

import com.cosmetic.cosmetic.dto.role.RoleDto;
import com.cosmetic.cosmetic.entity.Role;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface RoleMapper {
    RoleMapper INSTANCE = Mappers.getMapper( RoleMapper.class );

    RoleDto toDto(Role role);
}
