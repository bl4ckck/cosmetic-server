package com.cosmetic.cosmetic.mapper.role;

import com.cosmetic.cosmetic.dto.role.RoleDto;
import com.cosmetic.cosmetic.dto.role.RoleMappedDto;
import com.cosmetic.cosmetic.entity.Role;
import com.cosmetic.cosmetic.mapper.account.AccountMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring", uses = { AccountMapper.class })
public interface RoleMappedMapper {
    RoleMappedMapper INSTANCE = Mappers.getMapper( RoleMappedMapper.class );

    List<RoleMappedDto> toList(List<Role> role);
    RoleMappedDto toDto(Role role);
}
