package com.cosmetic.cosmetic.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Embeddable
public class CartVariantKey implements Serializable {
    @Column(name = "`cartId`")
    private UUID cartId;

    @Column(name = "`variantId`")
    private UUID variantId;
}
