package com.cosmetic.cosmetic.entity;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum EStatus {
    WAITING_FOR_PAYMENT,
    PAID,
    ON_DELIVERY,
    DELIVERED,
    CANCELED
}
