package com.cosmetic.cosmetic.entity;

import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@RequiredArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "account_providers")
public class AccountProvider {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private UUID id;

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "`accountId`", unique = true)
    private Account account;

    @NonNull
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private EProvider provider;
}