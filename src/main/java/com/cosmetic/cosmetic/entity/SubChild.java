package com.cosmetic.cosmetic.entity;

import lombok.*;
import org.hibernate.annotations.Cascade;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@RequiredArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "sub_child")
public class SubChild {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @NonNull
    @Id
    @Column(name = "id", nullable = false)
    private UUID id;

    @NonNull
    private String name;

    @ManyToMany(mappedBy = "articleCategories")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE,  org.hibernate.annotations.CascadeType.MERGE, org.hibernate.annotations.CascadeType.PERSIST})
    private Set<Article> articles = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "sub_category_id")
    private SubCategory subCategory;

    @OneToMany(mappedBy = "subChild", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Product> products = new HashSet<>();

}

