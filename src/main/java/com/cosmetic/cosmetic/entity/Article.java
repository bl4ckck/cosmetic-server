package com.cosmetic.cosmetic.entity;

import lombok.*;
import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@RequiredArgsConstructor
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@ToString
@Table(name = "articles")
public class Article {
    @NonNull
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private UUID id;

    @NonNull
    @ManyToOne
    @JoinColumn(name = "`userId`")
    private User user;

    @ManyToMany
    @JoinTable(
            name = "article_ingredients",
            joinColumns = @JoinColumn(name = "`articleId`", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "`ingredientId`", referencedColumnName = "id"))
    private Set<Ingredient> articleIngredients = new HashSet<>();

    @ManyToMany
    @JoinTable(
            name = "article_categories",
            joinColumns = @JoinColumn(name = "`articleId`", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "`subChildId`", referencedColumnName = "id"))
    private Set<SubChild> articleCategories = new HashSet<>();

    @NonNull
    private String title;

    @NonNull
    @Column(columnDefinition="TEXT")
    private String content;

    @NonNull
    @Column(columnDefinition="TEXT")
    private String photo;

    @NonNull
    private Date date;
}
