package com.cosmetic.cosmetic.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "promos", indexes = @Index(columnList = "name"))
public class Promo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private UUID id;

    @Column(unique = true)
    private String name;

    @Column(unique = true)
    private String voucher;

    private Integer discount;

    @Column(name = "`minimumPrice`")
    private Double minimumPrice;

    @Column(name = "`startDate`")
    private Date startDate;

    @Column(name = "`endDate`")
    private Date endDate;
}
