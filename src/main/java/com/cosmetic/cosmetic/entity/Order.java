package com.cosmetic.cosmetic.entity;

import lombok.*;

import javax.persistence.*;
import java.util.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "orders")
public class Order {
    @Id
    @Column(name = "id", nullable = false)
    private UUID id;

    @ManyToOne
    @JoinColumn(name = "`userId`")
    private User user;

    @ManyToOne
    @JoinColumn(name = "`addressId`")
    private Address address;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<OrderVariant> orders;

    private UUID[] promo;

    @Enumerated
    private EDelivery delivery;

    @Enumerated
    private EStatus status;
    private Date date;
    private Double total;
}
