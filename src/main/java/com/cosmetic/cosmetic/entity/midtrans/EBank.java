package com.cosmetic.cosmetic.entity.midtrans;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum EBank {
    ALL_BANK("bni"),
    PERMATA("permata"),
    BCA("bca"),
    BNI("bni"),
    BRI("bri");

    public final String name;
}
