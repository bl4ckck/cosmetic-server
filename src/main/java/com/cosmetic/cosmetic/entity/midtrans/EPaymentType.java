package com.cosmetic.cosmetic.entity.midtrans;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum EPaymentType {
    BANK_TRANSFER("bank_transfer"),
    GOPAY("gopay");
    public final String paymentType;
}
