package com.cosmetic.cosmetic.entity;

import lombok.*;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

@AllArgsConstructor
@RequiredArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "brands")
public class Brand {
    @NonNull
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private UUID id;

    @OneToMany(mappedBy = "brand", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Product> products = new LinkedHashSet<>();

    @NonNull
    private String name;

    @NonNull
    @Column(columnDefinition="TEXT")
    private String description;

    @NonNull
    @Column(columnDefinition="TEXT")
    private String logo;

    @NonNull
    @Column(columnDefinition="TEXT")
    private String banner;
}