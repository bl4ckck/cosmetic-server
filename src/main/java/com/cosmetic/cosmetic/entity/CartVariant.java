package com.cosmetic.cosmetic.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "cart_variants")
@Entity
public class CartVariant {
    @EmbeddedId
    private CartVariantKey id;

    @ManyToOne
    @MapsId("cartId")
    @JoinColumn(name = "`cartId`")
    private Cart cart;

    @ManyToOne
    @MapsId("variantId")
    @JoinColumn(name = "`variantId`")
    private Variant variant;

    @Column(name = "`isChecked`")
    private Boolean isChecked;

    private Integer quantity;

    @Column(name = "`subTotal`")
    private Double subTotal;
}
