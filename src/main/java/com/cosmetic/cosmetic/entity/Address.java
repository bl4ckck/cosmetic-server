package com.cosmetic.cosmetic.entity;

import lombok.*;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

@RequiredArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "addresses")
public class Address {
    @NonNull
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private UUID id;

    @ManyToOne
    @JoinColumn(name = "`userId`")
    private User user;

    @OneToOne(mappedBy = "address", cascade = CascadeType.ALL, orphanRemoval = true)
    private Cart cart;

    @OneToMany(mappedBy = "address", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Order> orders = new LinkedHashSet<>();

    @NonNull
    private String phone;

    @NonNull
    private String receiver;

    @NonNull
    private String label;

    @NonNull
    @Column(name = "`isDefault`")
    private Boolean isDefault;

    @NonNull
    @JoinColumn(name = "`cityId`")
    private String cityId;

    @NonNull
    @JoinColumn(name = "`postalCode`")
    private String postalCode;

    @NonNull
    @Column(columnDefinition="TEXT")
    private String addressDetail;
}
