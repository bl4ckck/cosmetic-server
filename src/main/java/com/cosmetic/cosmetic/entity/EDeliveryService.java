package com.cosmetic.cosmetic.entity;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum EDeliveryService {
    REGULAR("regular"),
    INSTANT("instant");

    public final String name;
}
