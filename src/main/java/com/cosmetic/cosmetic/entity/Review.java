package com.cosmetic.cosmetic.entity;

import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.Instant;
import java.util.Date;
import java.util.UUID;

@AllArgsConstructor
@RequiredArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "reviews")
public class Review {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    @NotNull(message = "Value shouldn't be null")
    @NonNull
    @Column(name = "id", nullable = false)
    private UUID id;

    @NonNull
    @NotNull(message = "Value shouldn't be null")
    @ManyToOne
    @JoinColumn(name = "productId")
    private Product product;

    @NonNull
    @NotNull(message = "Value shouldn't be null")
    @ManyToOne
    @JoinColumn(name = "userId")
    private User user;

    @NonNull
    @Max(value = 5, message = "Value shouldn't be more than 5")
    @Min(value = 1, message = "Value shouldn't be less than 1")
    private Integer effective;

    @NonNull
    @Max(value = 5, message = "Value shouldn't be more than 5")
    @Min(value = 1, message = "Value shouldn't be less than 1")
    private Integer texture;

    @NonNull
    @Max(value = 5, message = "Value shouldn't be less than 5")
    @Min(value = 1, message = "Value shouldn't be less than 1")
    private Integer price;

    @NonNull
    @Max(value = 5, message = "Value shouldn't be more than 5")
    @Min(value = 1, message = "Value shouldn't be less than 1")
    private Integer packaging;

    @NonNull
    private Double averageStar;

    @NonNull
    private Date date = Date.from(Instant.now());

    @NonNull
    @Size(min = 5, max = 500, message = "Content must be between 5 - 500 characters")
    private String content;

    @NonNull

    private String[] images;

    @NonNull
    private Integer imagesCount;
}
