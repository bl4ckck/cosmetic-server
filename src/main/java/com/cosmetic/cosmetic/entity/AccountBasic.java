package com.cosmetic.cosmetic.entity;

import lombok.*;

import javax.persistence.*;
import java.util.UUID;


@RequiredArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name ="account_basics")
public class AccountBasic {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private UUID id;

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "`accountId`", unique = true)
    private Account account;

    @NonNull
    @Column(nullable = false)
    private String password;
}
