package com.cosmetic.cosmetic.entity;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

@RequiredArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "products")
public class Product {
    @NonNull
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private UUID id;

    @ManyToOne
    @JoinColumn(name = "`brandId`")
    private Brand brand;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Variant> variants = new LinkedHashSet<>();

    @NonNull
    private String name;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Review> reviews;

    @ManyToMany
    @JoinTable(
            name = "product_ingredients",
            joinColumns = @JoinColumn(name = "`productId`", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "`ingredientId`", referencedColumnName = "id"))
    private Set<Ingredient> productIngredients = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "`subChildId`")
    private SubChild subChild;

    @NonNull
    private String[] images;

    @NonNull
    @Column(name = "`isOrganic`")
    private Boolean isOrganic;
}
