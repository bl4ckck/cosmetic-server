package com.cosmetic.cosmetic.entity;

import lombok.*;
import org.hibernate.annotations.Cascade;
import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@RequiredArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "ingredients")
public class Ingredient {
    @NonNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    @Id
    private UUID id;

    @NonNull
    @Column(columnDefinition="TEXT")
    private String name;

    @ManyToMany(mappedBy = "articleIngredients")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE,  org.hibernate.annotations.CascadeType.MERGE, org.hibernate.annotations.CascadeType.PERSIST})
    private Set<Article> articles;

    @ManyToMany(mappedBy = "productIngredients")
    private Set<Product> products;
}

