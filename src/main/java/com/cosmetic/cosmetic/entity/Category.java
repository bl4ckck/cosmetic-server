package com.cosmetic.cosmetic.entity;


import lombok.*;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "categories")
public class Category {
    @NonNull
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    @Column(name = "id",nullable = false)
    private UUID id;

    @OneToMany(mappedBy = "category",cascade = CascadeType.ALL,orphanRemoval = true)
    private Set<SubCategory> subCategories = new LinkedHashSet<>();

    @NonNull
    private String name;
}