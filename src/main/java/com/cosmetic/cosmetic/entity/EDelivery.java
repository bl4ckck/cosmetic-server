package com.cosmetic.cosmetic.entity;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum EDelivery {
    JNE("jne"),
    POS("pos"),
    TIKI("tiki");

    public final String name;
}
