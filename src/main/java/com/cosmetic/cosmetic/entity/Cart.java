package com.cosmetic.cosmetic.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@RequiredArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "carts")
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private UUID id;

    @NonNull
    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "`userId`", unique = true, nullable = false)
    private User user;

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "`addressId`")
    private Address address;

    @OneToMany(mappedBy = "cart", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<CartVariant> carts;

    private UUID[] promo;

    @Enumerated
    private EDelivery delivery;

    private Double total = 0D;

    private Double weight = 0D;
}
