package com.cosmetic.cosmetic.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@ToString
@RequiredArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "variants")
public class Variant {
    @NonNull
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private UUID id;

    @ManyToOne
    @JoinColumn(name = "`productId`")
    private Product product;

    @OneToMany(mappedBy = "variant", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<OrderVariant> orders;

    @OneToMany(mappedBy = "variant", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<CartVariant> carts;

    @NonNull
    private String name;

    @NonNull
    private Double price;

    @NonNull
    private Integer quantity;

    @NonNull
    private Double weight;

    @NonNull
    @Column(name = "`imageIndex`")
    private Integer imageIndex;
}