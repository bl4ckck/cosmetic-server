package com.cosmetic.cosmetic.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "order_variants")
@Entity
public class OrderVariant {
    @EmbeddedId
    private OrderVariantKey id;

    @ManyToOne
    @MapsId("orderId")
    @JoinColumn(name = "`orderId`")
    private Order order;

    @ManyToOne
    @MapsId("variantId")
    @JoinColumn(name = "`variantId`")
    private Variant variant;

    @Column(name = "`productPrice`")
    private Double productPrice;

    private Integer quantity;

    @Column(name = "`subTotal`")
    private Double subTotal;
}
