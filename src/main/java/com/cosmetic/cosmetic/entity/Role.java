package com.cosmetic.cosmetic.entity;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "roles")
public class Role {
    @Id
    @NonNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private UUID id;

    @OneToMany(mappedBy = "role", orphanRemoval = true)
    private Set<Account> accounts = new LinkedHashSet<>();

    @NonNull
    @Enumerated(EnumType.STRING)
    @Column(unique = true)
    private ERole name;
}