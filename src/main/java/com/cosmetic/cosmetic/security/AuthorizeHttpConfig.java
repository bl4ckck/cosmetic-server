package com.cosmetic.cosmetic.security;

import lombok.Data;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AuthorizeHttpRequestsConfigurer;

@Data
public class AuthorizeHttpConfig {
    private AuthorizeHttpRequestsConfigurer<HttpSecurity>
            .AuthorizationManagerRequestMatcherRegistry matcherRegistry;

    public AuthorizeHttpConfig(AuthorizeHttpRequestsConfigurer<HttpSecurity>
                                       .AuthorizationManagerRequestMatcherRegistry matcherRegistry) {
        this.matcherRegistry = matcherRegistry;
        this.authorizedPaths();
    }

    public void authorizedPaths() {
        this.globalPath();
        this.userPaths();
        this.orderPaths();
        this.addressPaths();
        this.cartPaths();
        this.checkoutPaths();
    }

    private void globalPath() {
        this.matcherRegistry
                .antMatchers("/",
                        "/api/v1/auth/login",
                        "/api/v1/auth/oauth2").permitAll()
                // Springfox
                .antMatchers(
                        "/v2/api-docs/**",
                        "/swagger-resources/**",
                        "/swagger-ui.html**"
                ).permitAll()
                .antMatchers("/swagger/**", "/v3/api-docs/**").permitAll()
                .antMatchers(HttpMethod.PUT).hasAnyRole("CUSTOMER", "ADMIN")
                .antMatchers(HttpMethod.DELETE).hasAnyRole("CUSTOMER", "ADMIN");
    }

    private void userPaths() {
        this.matcherRegistry
                // TODO: change to hasRole("ADMIN")
//                .antMatchers("/api/v1/user").hasRole("ADMIN")
                .antMatchers("/api/v1/user/**").permitAll();
    }

    private void orderPaths() {
        this.matcherRegistry
                .antMatchers(HttpMethod.GET, "/api/v1/order/").hasAnyRole("CUSTOMER");
    }

    private void addressPaths() {
        this.matcherRegistry
                .antMatchers(HttpMethod.GET, "/api/v1/address/q/user").hasAnyRole("CUSTOMER")
                .antMatchers(HttpMethod.POST, "/api/v1/address/").hasAnyRole("CUSTOMER");
    }

    private void cartPaths() {
        this.matcherRegistry
                .antMatchers("/api/v1/carts/**").hasAnyRole("CUSTOMER");
    }

    private void checkoutPaths() {
        this.matcherRegistry
                .antMatchers("/api/v1/checkout/**").hasAnyRole("CUSTOMER");
    }
}
