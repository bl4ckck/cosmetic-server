package com.cosmetic.cosmetic.controller;

import com.cosmetic.cosmetic.dto.ResponseDto;
import com.cosmetic.cosmetic.dto.article.ArticleDto;
import com.cosmetic.cosmetic.dto.article.ArticleMappedDto;
import com.cosmetic.cosmetic.dto.article.ArticleSaveDto;
import com.cosmetic.cosmetic.service.ArticleService;
import com.cosmetic.cosmetic.util.DtoUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.UUID;

@Api(tags = "Article")
@RestController
@RequestMapping("/api/v1/article")
public class ArticleController {
    @Autowired
    private ArticleService articleService;


    @PostMapping
    public ResponseEntity<ResponseDto<ArticleMappedDto>> createArticle(@RequestBody ArticleSaveDto payload) {
        try{
            ResponseDto<ArticleMappedDto> response =
                    DtoUtil.responseDtoSuccess(articleService.create(payload));
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        }catch (Exception e){
            ResponseDto<ArticleMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    public ResponseEntity<ResponseDto<List<ArticleMappedDto>>> findAllArticles(){
        try{
            ResponseDto<List<ArticleMappedDto>> response =
                    DtoUtil.responseDtoSuccess(articleService.findAll());
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<List<ArticleMappedDto>> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDto<ArticleMappedDto>> findOneArticle(@PathVariable UUID id){
        try{

            ResponseDto<ArticleMappedDto> response =
                    DtoUtil.responseDtoSuccess(articleService.findOne(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<ArticleMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "", authorizations = {@Authorization(value = "Bearer")})
    @PutMapping
    public ResponseEntity<ResponseDto<ArticleMappedDto>> updateArticle(@RequestBody ArticleDto payload){
        try{
            ResponseDto<ArticleMappedDto> response =
                    DtoUtil.responseDtoSuccess(articleService.update(payload));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<ArticleMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "", authorizations = {@Authorization(value = "Bearer")})
    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseDto<Boolean>> deleteArticle(@PathVariable UUID id){
        try {
            ResponseDto<Boolean> response =
                    DtoUtil.responseDtoSuccess(articleService.delete(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<Boolean> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/related-article/{productId}")
    public ResponseEntity<ResponseDto<List<ArticleMappedDto>>> getRelatedArticle(@PathVariable UUID productId){
        try {
             ResponseDto<List<ArticleMappedDto>> response =
                     DtoUtil.responseDtoSuccess(articleService.findRelatedArticles(productId));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<List<ArticleMappedDto>> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}