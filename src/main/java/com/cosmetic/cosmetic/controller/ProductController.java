package com.cosmetic.cosmetic.controller;

import com.cosmetic.cosmetic.dto.ResponseDto;
import com.cosmetic.cosmetic.dto.product.*;
import com.cosmetic.cosmetic.mapper.product.ProductDetailInfo;
import com.cosmetic.cosmetic.service.ProductService;
import com.cosmetic.cosmetic.util.DtoUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Api(tags = "Prodcut")
@RequestMapping("/api/v1/product")
@RestController
public class ProductController {
    @Autowired
    private ProductService productService;

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDto<ProductDetailInfo>> findOneProduct(@PathVariable UUID id) {
        try {
            ResponseDto<ProductDetailInfo> response =
                    DtoUtil.responseDtoSuccess(productService.findOneProduct(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<ProductDetailInfo> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    public ResponseEntity<ResponseDto<List<ProductMappedDto>>> findAllProduct() {
//        ObjectMapper mapper = new ObjectMapper();
//        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        Map<?, ?> map = mapper.convertValue(principal, Map.class);
//        System.out.println("dari " + map);
        try {
            ResponseDto<List<ProductMappedDto>> response =
                    DtoUtil.responseDtoSuccess(productService.findAll());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<List<ProductMappedDto>> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/q/trending")
    public ResponseEntity<ResponseDto<List<ProductTrendingDto>>> findAllProductTrending() {
        try {
            ResponseDto<List<ProductTrendingDto>> response =
                    DtoUtil.responseDtoSuccess(productService.findAllProductTrending());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<List<ProductTrendingDto>> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "", authorizations = { @Authorization(value="Bearer") })
    @PutMapping
    public ResponseEntity<ResponseDto<ProductMappedDto>> updateProduct(@RequestBody ProductDto product) {
        try {
            ResponseDto<ProductMappedDto> response =
                    DtoUtil.responseDtoSuccess(productService.update(product));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<ProductMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "", authorizations = { @Authorization(value="Bearer") })
    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseDto<Boolean>> deleteProduct(@PathVariable UUID id) {
        try {
            ResponseDto<Boolean> response =
                    DtoUtil.responseDtoSuccess(productService.delete(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<Boolean> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/p")
    public ResponseEntity<ResponseDto<PaginatedProductResponse>> findAllProductPaginated(@PageableDefault(size = 12) @RequestParam Integer page, @RequestParam Integer size) {
        ResponseDto<PaginatedProductResponse> response =
                DtoUtil.responseDtoSuccess(productService.findAllPaginated(page, size));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/categories")
    public ResponseEntity<ResponseDto<PaginatedProductResponse>>findAllProductByCategories(ProductFilterDto filter, Integer page, Integer size) {
        ResponseDto<PaginatedProductResponse> response =
                DtoUtil.responseDtoSuccess(productService.findAllProductByCategories(filter, page, size));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/filter")
    public ResponseEntity<ResponseDto<PaginatedProductResponse>>findAllProductByReviewsAndPrice(ProductFilterByReviewAndPriceDto filter, Integer page, Integer size) {
        ResponseDto<PaginatedProductResponse> response =
                DtoUtil.responseDtoSuccess(productService.findAllProductByReviewAndPrice(filter, page, size));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/search")
    public ResponseEntity<ResponseDto<PaginatedProductResponse>> findAllProductByKeywordSearch(@RequestParam String keyword, Integer page, Integer size) {
        ResponseDto<PaginatedProductResponse> response =
                DtoUtil.responseDtoSuccess(productService.findAllProductAndBrandBySearchKeyword(keyword, page, size));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
