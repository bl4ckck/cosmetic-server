package com.cosmetic.cosmetic.controller;

import com.cosmetic.cosmetic.dto.ResponseDto;
import com.cosmetic.cosmetic.dto.TokenDto;
import com.cosmetic.cosmetic.dto.account.AccountMappedDto;
import com.cosmetic.cosmetic.dto.account.AccountSaveDto;
import com.cosmetic.cosmetic.dto.account.AccountUpdateDto;
import com.cosmetic.cosmetic.service.AccountService;
import com.cosmetic.cosmetic.util.DtoUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Api(tags = "Account")
@RequestMapping("/api/v1/account")
@RestController
public class AccountController {
    @Autowired
    private AccountService accountService;

    @ApiOperation(value = "Create Account & User")
    @PostMapping
    public ResponseEntity<ResponseDto<TokenDto>> createAccount(
            @RequestBody AccountSaveDto account) {
        try {
            ResponseDto<TokenDto> response =
                    DtoUtil.responseDtoSuccess(accountService.createAccount(account));
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } catch (Exception e) {
            ResponseDto<TokenDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    public ResponseEntity<ResponseDto<List<AccountMappedDto>>> findAllAccount() {
        try {
            ResponseDto<List<AccountMappedDto>> response =
                    DtoUtil.responseDtoSuccess(accountService.findAll());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<List<AccountMappedDto>> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDto<AccountMappedDto>> findOneAccount(@PathVariable UUID id) {
        try {
            ResponseDto<AccountMappedDto> response =
                    DtoUtil.responseDtoSuccess(accountService.findOne(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<AccountMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "", authorizations = {@Authorization(value = "Bearer")})
    @PutMapping
    public ResponseEntity<ResponseDto<AccountMappedDto>> updateAccount(@RequestBody AccountUpdateDto account) {
        try {
            ResponseDto<AccountMappedDto> response =
                    DtoUtil.responseDtoSuccess(accountService.update(account));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<AccountMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "", authorizations = {@Authorization(value = "Bearer")})
    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseDto<Boolean>> deleteAccount(@PathVariable UUID id) {
        try {
            ResponseDto<Boolean> response =
                    DtoUtil.responseDtoSuccess(accountService.delete(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<Boolean> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
