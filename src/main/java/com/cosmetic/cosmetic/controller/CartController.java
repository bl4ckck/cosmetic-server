package com.cosmetic.cosmetic.controller;

import com.cosmetic.cosmetic.dto.ResponseDto;
import com.cosmetic.cosmetic.dto.address.AddressDto;
import com.cosmetic.cosmetic.dto.article.ArticleDto;
import com.cosmetic.cosmetic.dto.article.ArticleMappedDto;
import com.cosmetic.cosmetic.dto.cart.CartDto;
import com.cosmetic.cosmetic.dto.cart.CartMappedDto;
import com.cosmetic.cosmetic.dto.cart.CartSaveDto;
import com.cosmetic.cosmetic.entity.ECartAction;
import com.cosmetic.cosmetic.mapper.cart.CartAddressInfo;
import com.cosmetic.cosmetic.mapper.cart.CartInfo;
import com.cosmetic.cosmetic.service.CartService;
import com.cosmetic.cosmetic.util.DtoUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.UUID;

@Api(tags = "Cart")
@RestController
@RequestMapping("/api/v1/carts")
public class CartController {
    @Autowired
    private CartService cartService;

    @ApiOperation(value = "", authorizations = {@Authorization(value = "Bearer")})
    @PostMapping
    public ResponseEntity<ResponseDto<CartMappedDto>> addToCart(@RequestBody CartSaveDto payload) {
        try {
            ResponseDto<CartMappedDto> response =
                    DtoUtil.responseDtoSuccess(cartService.create(payload));
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } catch (Exception e) {
            ResponseDto<CartMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "", authorizations = {@Authorization(value = "Bearer")})
    @GetMapping
    public ResponseEntity<ResponseDto<Map<String, Object>>> findAllCarts() {
        try {
            ResponseDto<Map<String, Object>> response =
                    DtoUtil.responseDtoSuccess(cartService.cartByUserId());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<Map<String, Object>> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "", authorizations = {@Authorization(value = "Bearer")})
    @GetMapping("/overview")
    public ResponseEntity<ResponseDto<CartDto>> findOverviewCart() {
        try {
            ResponseDto<CartDto> response =
                    DtoUtil.responseDtoSuccess(cartService.getOverview());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<CartDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "", authorizations = {@Authorization(value = "Bearer")})
    @PostMapping("/action/{variantId}")
    public ResponseEntity<ResponseDto<Boolean>> actionCart(
            @PathVariable UUID variantId,
            @ApiParam(name = "isChecked")
                @RequestParam(value = "isChecked", required = false)
                Boolean isChecked,
            @ApiParam(name = "actionType")
                @RequestParam(value = "actionType", required = true)
                ECartAction action) {
        try {
            ResponseDto<Boolean> response =
                    DtoUtil.responseDtoSuccess(cartService.actionItem(variantId, isChecked, action));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<Boolean> response =
                    DtoUtil.responseDtoFailed(Boolean.FALSE, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "", authorizations = {@Authorization(value = "Bearer")})
    @PutMapping("/address/{addressId}")
    public ResponseEntity<ResponseDto<AddressDto>> updateAddressCart(@PathVariable UUID addressId){
        try{
            ResponseDto<AddressDto> response =
                    DtoUtil.responseDtoSuccess(cartService.updateCartAddress(addressId));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<AddressDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "", authorizations = {@Authorization(value = "Bearer")})
    @GetMapping("/q/address")
    public ResponseEntity<ResponseDto<CartAddressInfo>> findAddressCart() {
        try {
            ResponseDto<CartAddressInfo> response =
                    DtoUtil.responseDtoSuccess(cartService.getAddressCart());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<CartAddressInfo> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
