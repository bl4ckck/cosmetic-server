package com.cosmetic.cosmetic.controller;

import com.cosmetic.cosmetic.dto.ResponseDto;
import com.cosmetic.cosmetic.dto.ingredient.IngredientMappedDto;
import com.cosmetic.cosmetic.dto.ingredient.IngredientSaveDto;
import com.cosmetic.cosmetic.service.IngredientService;
import com.cosmetic.cosmetic.util.DtoUtil;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Ingredient")
@RestController
@RequestMapping("/api/v1/ingredient")
public class IngredientController {

    @Autowired
    private IngredientService ingredientService;

    @PostMapping
    public ResponseEntity<ResponseDto<IngredientMappedDto>> createIngredient(@RequestBody IngredientSaveDto payload){
        try{
            ResponseDto<IngredientMappedDto> response =
                    DtoUtil.responseDtoSuccess(this.ingredientService.create(payload));
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        }catch (Exception e){
            ResponseDto<IngredientMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}