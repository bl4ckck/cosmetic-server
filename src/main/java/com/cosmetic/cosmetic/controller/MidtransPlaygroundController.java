package com.cosmetic.cosmetic.controller;

import com.cosmetic.cosmetic.component.MidtransComponent;
import com.cosmetic.cosmetic.dto.ResponseDto;
import com.cosmetic.cosmetic.dto.order.CheckoutDto;
import com.cosmetic.cosmetic.entity.midtrans.EBank;
import com.cosmetic.cosmetic.service.CheckoutService;
import com.cosmetic.cosmetic.util.DtoUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Api(tags = "Midtrans Testing")
@RequestMapping("/api/v1/midtrans")
@RestController
public class MidtransPlaygroundController {
    @Autowired
    private MidtransComponent midtrans;

    @Autowired
    private CheckoutService checkoutService;

    @PostMapping
    public ResponseEntity<ResponseDto<Map<String, Object>>> snapTransaction(@RequestBody CheckoutDto payload,
                                                                            @ApiParam(name = "bank")
                                                                            @RequestParam(value = "bank", required = false) EBank bank) {
        try {
            ResponseDto<Map<String, Object>> response =
                    DtoUtil.responseDtoSuccess(null);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<Map<String, Object>> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
