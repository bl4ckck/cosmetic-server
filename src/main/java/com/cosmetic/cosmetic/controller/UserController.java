package com.cosmetic.cosmetic.controller;

import com.cosmetic.cosmetic.dto.ResponseDto;
import com.cosmetic.cosmetic.dto.user.UserDto;
import com.cosmetic.cosmetic.dto.user.UserMappedDto;
import com.cosmetic.cosmetic.service.UserService;
import com.cosmetic.cosmetic.util.DtoUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Api(tags = "User")
@RequestMapping("/api/v1/user")
@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping
    public ResponseEntity<ResponseDto<List<UserMappedDto>>> findAllUser() {
        try {
            ResponseDto<List<UserMappedDto>> response =
                    DtoUtil.responseDtoSuccess(userService.findAll());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch(Exception e) {
            ResponseDto<List<UserMappedDto>> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDto<UserMappedDto>> findOneUser(@PathVariable UUID id) {
        try {
            ResponseDto<UserMappedDto> response =
                    DtoUtil.responseDtoSuccess(userService.findOne(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch(Exception e) {
            ResponseDto<UserMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "", authorizations = { @Authorization(value="Bearer") },
            notes = "activate account when address & phone are not null")
    @PutMapping
    public ResponseEntity<ResponseDto<UserMappedDto>> updateUser(@RequestBody UserDto user) {
        try {
            ResponseDto<UserMappedDto> response =
                    DtoUtil.responseDtoSuccess(userService.update(user));
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } catch (Exception e) {
            ResponseDto<UserMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
