package com.cosmetic.cosmetic.controller;

import com.cosmetic.cosmetic.dto.ResponseDto;
import com.cosmetic.cosmetic.dto.promo.PromoMappedDto;
import com.cosmetic.cosmetic.dto.promo.PromoSaveDto;
import com.cosmetic.cosmetic.service.PromoService;
import com.cosmetic.cosmetic.util.DtoUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Api(tags = "Promo")
@RequestMapping("/api/v1/promo")
@RestController
public class PromoController {
    @Autowired
    private PromoService promoService;

    @PostMapping
    public ResponseEntity<ResponseDto<PromoMappedDto>> createPromo(@RequestBody PromoSaveDto promoSaveDto) {
        try {
            ResponseDto<PromoMappedDto> response =
                    DtoUtil.responseDtoSuccess(promoService.create(promoSaveDto));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<PromoMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDto<PromoMappedDto>> findOnePromo(@PathVariable UUID id) {
        try {
            ResponseDto<PromoMappedDto> response =
                    DtoUtil.responseDtoSuccess(promoService.findOne(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<PromoMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    public ResponseEntity<ResponseDto<List<PromoMappedDto>>> findAllPromo() {
        try {
            ResponseDto<List<PromoMappedDto>> response =
                    DtoUtil.responseDtoSuccess(promoService.findAll());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<List<PromoMappedDto>> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "", authorizations = {@Authorization(value = "Bearer")})
    @PutMapping
    public ResponseEntity<ResponseDto<PromoMappedDto>> updatePromo(@RequestBody PromoMappedDto promo) {
        try {
            ResponseDto<PromoMappedDto> response =
                    DtoUtil.responseDtoSuccess(promoService.update(promo));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<PromoMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "", authorizations = {@Authorization(value = "Bearer")})
    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseDto<Boolean>> deletePromo(@PathVariable UUID id) {
        try {
            ResponseDto<Boolean> response =
                    DtoUtil.responseDtoSuccess(promoService.delete(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<Boolean> response =
                    DtoUtil.responseDtoFailed(Boolean.FALSE, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
