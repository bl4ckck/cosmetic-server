package com.cosmetic.cosmetic.controller;

import com.cosmetic.cosmetic.dto.ResponseDto;
import com.cosmetic.cosmetic.dto.rajaongkir.ROCity;
import com.cosmetic.cosmetic.dto.rajaongkir.ROCost;
import com.cosmetic.cosmetic.dto.rajaongkir.ROCostSave;
import com.cosmetic.cosmetic.dto.rajaongkir.ROProvince;
import com.cosmetic.cosmetic.service.external.RajaOngkirService;
import com.cosmetic.cosmetic.util.DtoUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "Raja Ongkir Testing")
@RequestMapping("/api/v1/raja-ongkir")
@RestController
public class RajaOngkirPlaygroundController {
    @Autowired
    private RajaOngkirService rajaOngkirService;

    @ApiOperation(value = "Find all provinces", notes = "")
    @GetMapping("/province")
    public ResponseEntity<ResponseDto<List<ROProvince>>> getAllProvince() {
        try {
            ResponseDto<List<ROProvince>> response =
                    DtoUtil.responseDtoSuccess(rajaOngkirService.getProvinces());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<List<ROProvince>> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/province/q")
    public ResponseEntity<ResponseDto<ROProvince>> getProvince(@ApiParam(name = "provinceId")
                                                                            @RequestParam(value = "provinceId", required = false) String provinceId) {
        try {
            ResponseDto<ROProvince> response =
                    DtoUtil.responseDtoSuccess(rajaOngkirService.getProvinces(provinceId));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<ROProvince> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "Find all city", notes = "")
    @GetMapping("/city")
    public ResponseEntity<ResponseDto<List<ROCity>>> getAllCity() {
        try {
            ResponseDto<List<ROCity>> response =
                    DtoUtil.responseDtoSuccess(rajaOngkirService.getCity());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<List<ROCity>> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/city/q")
    public ResponseEntity<ResponseDto<ROCity>> getCity(@ApiParam(name = "cityId")
                                                           @RequestParam(value = "cityId") String cityId,
                                                       @ApiParam(name = "provinceId")
                                                       @RequestParam(value = "provinceId", required = false) String provinceId) {
        try {
            ResponseDto<ROCity> response =
                    DtoUtil.responseDtoSuccess(rajaOngkirService.getCity(cityId, provinceId));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<ROCity> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/cost")
    public ResponseEntity<ResponseDto<List<ROCost>>> getCost(@RequestBody ROCostSave costSave) {
        try {
            ResponseDto<List<ROCost>> response =
                    DtoUtil.responseDtoSuccess(rajaOngkirService.getCost(costSave));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<List<ROCost>> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
