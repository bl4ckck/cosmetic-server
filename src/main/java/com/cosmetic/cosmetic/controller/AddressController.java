package com.cosmetic.cosmetic.controller;

import com.cosmetic.cosmetic.dto.ResponseDto;
import com.cosmetic.cosmetic.dto.address.AddressDto;
import com.cosmetic.cosmetic.dto.address.AddressMappedDto;
import com.cosmetic.cosmetic.dto.address.AddressSaveDto;
import com.cosmetic.cosmetic.service.AddressService;
import com.cosmetic.cosmetic.util.DtoUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Api(tags = "Address")
@RestController
@RequestMapping("/api/v1/address")
public class AddressController {
    @Autowired
    private AddressService addressService;

    @ApiOperation(value = "", authorizations = {@Authorization(value = "Bearer")})
    @PostMapping
    public ResponseEntity<ResponseDto<AddressMappedDto>> createAddress(@RequestBody AddressSaveDto payload) {
        try {
            ResponseDto<AddressMappedDto> response =
                    DtoUtil.responseDtoSuccess(addressService.create(payload));
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } catch (Exception e) {
            ResponseDto<AddressMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    public ResponseEntity<ResponseDto<List<AddressMappedDto>>> findAllAddresses() {
        try {
            ResponseDto<List<AddressMappedDto>> response =
                    DtoUtil.responseDtoSuccess(addressService.findAll());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<List<AddressMappedDto>> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "", authorizations = {@Authorization(value = "Bearer")})
    @GetMapping("/q/user")
    public ResponseEntity<ResponseDto<List<AddressMappedDto>>> findAllAddressesByUser() {
        try {
            ResponseDto<List<AddressMappedDto>> response =
                    DtoUtil.responseDtoSuccess(addressService.findByUserId());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<List<AddressMappedDto>> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDto<AddressMappedDto>> findOneAddress(@PathVariable UUID id) {
        try {
            ResponseDto<AddressMappedDto> response =
                    DtoUtil.responseDtoSuccess(addressService.findOne(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<AddressMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "", authorizations = {@Authorization(value = "Bearer")})
    @PutMapping
    public ResponseEntity<ResponseDto<AddressMappedDto>> updateAddress(@RequestBody AddressDto payload) {
        try {
            ResponseDto<AddressMappedDto> response =
                    DtoUtil.responseDtoSuccess(addressService.update(payload));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<AddressMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/delete/{id}")
    public ResponseEntity<ResponseDto<Boolean>> deleteAddress(@PathVariable UUID id) {
        try {
            ResponseDto<Boolean> response =
                    DtoUtil.responseDtoSuccess(addressService.delete(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<Boolean> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
