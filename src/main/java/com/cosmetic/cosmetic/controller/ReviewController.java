package com.cosmetic.cosmetic.controller;

import com.cosmetic.cosmetic.dto.ResponseDto;
import com.cosmetic.cosmetic.dto.review.ReviewDto;
import com.cosmetic.cosmetic.dto.review.ReviewMappedDto;
import com.cosmetic.cosmetic.dto.review.ReviewPaginatedDto;
import com.cosmetic.cosmetic.dto.review.ReviewSaveDto;
import com.cosmetic.cosmetic.service.ReviewService;
import com.cosmetic.cosmetic.util.DtoUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.UUID;

@Api(tags = "Review")
@RequestMapping("/api/v1/review")
@RestController
@RequiredArgsConstructor
public class ReviewController {
    @Autowired
    private ReviewService reviewService;

    @PostMapping(consumes = {"multipart/form-data"})
    public ResponseEntity<ResponseDto<ReviewMappedDto>> createReviewWithUpload(@ModelAttribute ReviewSaveDto payload ){
        try {
            ResponseDto<ReviewMappedDto> response =
                    DtoUtil.responseDtoSuccess(reviewService.createReview(payload));
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        }catch (Exception e){
            ResponseDto<ReviewMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    public ResponseEntity<ResponseDto<List<ReviewMappedDto>>> findAllReviews(){
        try {
            ResponseDto<List<ReviewMappedDto>> response =
                    DtoUtil.responseDtoSuccess(reviewService.findAll());
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<List<ReviewMappedDto>> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDto<ReviewMappedDto>> findOneReviews(@PathVariable UUID id){
        try {
            ResponseDto<ReviewMappedDto> response =
                    DtoUtil.responseDtoSuccess(reviewService.findOne(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<ReviewMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "", authorizations = {@Authorization(value = "Bearer")})
    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseDto<Boolean>> deleteReviews(@PathVariable UUID id){
        try {
            ResponseDto<Boolean> response =
                    DtoUtil.responseDtoSuccess(reviewService.delete(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<Boolean> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "", authorizations = {@Authorization(value = "Bearer")})
    @PutMapping
    public ResponseEntity<ResponseDto<ReviewMappedDto>> updateReview(@RequestBody ReviewDto payload) {
        try {
            ResponseDto<ReviewMappedDto> response =
                    DtoUtil.responseDtoSuccess(reviewService.update(payload));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<ReviewMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/review/{productId}")
    public ResponseEntity<ResponseDto<Integer>> countReviewByProdId(@PathVariable UUID productId) {
        try {
            ResponseDto<Integer> response =
                    DtoUtil.responseDtoSuccess(reviewService.countReviewByProdId(productId));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<Integer> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{productId}/product")
    public ResponseEntity<ResponseDto<ReviewPaginatedDto>> findAllReviewByProduct(@PathVariable UUID productId, Integer pageNo, Integer pageSize) {
        try {
            ResponseDto<ReviewPaginatedDto> response =
                    DtoUtil.responseDtoSuccess(reviewService.findAllReviewByProductId(productId, pageNo, pageSize));
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (Exception e){
            ResponseDto<ReviewPaginatedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}

