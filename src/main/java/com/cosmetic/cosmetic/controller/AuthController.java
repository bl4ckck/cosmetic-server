package com.cosmetic.cosmetic.controller;

import com.cosmetic.cosmetic.component.JwtComponent;
import com.cosmetic.cosmetic.dto.ResponseDto;
import com.cosmetic.cosmetic.dto.TokenDto;
import com.cosmetic.cosmetic.dto.account.AccountSaveDto;
import com.cosmetic.cosmetic.dto.account.LoginDto;
import com.cosmetic.cosmetic.service.AccountService;
import com.cosmetic.cosmetic.service.auth.AuthService;
import com.cosmetic.cosmetic.util.DtoUtil;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

@Api(tags="Auth")
@RequestMapping("/api/v1/auth")
@RestController
public class AuthController {
    @Autowired
    private BCryptPasswordEncoder bcrypt;

    @Autowired
    private AuthService authService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private JwtComponent jwtComponent;

    @PostMapping("/oauth2")
    public ResponseEntity<ResponseDto<TokenDto>> oAuth2(@RequestBody TokenDto token) {
        try {
            TokenDto claims = authService.loginWithOAuth2(token);
            ResponseDto<TokenDto> response = DtoUtil.responseDtoSuccess(claims);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<TokenDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.UNAUTHORIZED);
            return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("/login")
    public ResponseEntity<ResponseDto<TokenDto>> login(@RequestBody LoginDto payload) {
        try {
            TokenDto token = authService.loginWithEmailPassword(payload);
            ResponseDto<TokenDto> response = DtoUtil.responseDtoSuccess(token);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<TokenDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.UNAUTHORIZED);
            return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("/register")
    public ResponseEntity<ResponseDto<TokenDto>> registerWithEmailPassword(@RequestBody AccountSaveDto account) {
        try {
            ResponseDto<TokenDto> response =
                    DtoUtil.responseDtoSuccess(accountService.createAccount(account));
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } catch (Exception e) {
            ResponseDto<TokenDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
