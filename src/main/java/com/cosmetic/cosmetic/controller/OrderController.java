package com.cosmetic.cosmetic.controller;

import com.cosmetic.cosmetic.dto.ResponseDto;
import com.cosmetic.cosmetic.dto.order.OrderDetailDto;
import com.cosmetic.cosmetic.dto.order.OrderDto;
import com.cosmetic.cosmetic.dto.order.OrderMappedDto;
import com.cosmetic.cosmetic.service.OrderService;
import com.cosmetic.cosmetic.util.DtoUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@Api(tags = "Order")
@RequestMapping("/api/v1/order")
@RestController
public class OrderController {
    @Autowired
    private OrderService orderService;

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDto<OrderDetailDto>> findOneOrder(@PathVariable UUID id) {
        try {
            ResponseDto<OrderDetailDto> response =
                    DtoUtil.responseDtoSuccess(orderService.findOneOrder(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<OrderDetailDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "", authorizations = {@Authorization(value = "Bearer")})
    @GetMapping
    public ResponseEntity<ResponseDto<List<OrderMappedDto>>> findAllOrder() {
        try {
            ResponseDto<List<OrderMappedDto>> response =
                    DtoUtil.responseDtoSuccess(orderService.findAll());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<List<OrderMappedDto>> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "", authorizations = {@Authorization(value = "Bearer")})
    @PutMapping
    public ResponseEntity<ResponseDto<OrderMappedDto>> updateOrder(@RequestBody OrderDto order) {
        try {
            ResponseDto<OrderMappedDto> response =
                    DtoUtil.responseDtoSuccess(orderService.update(order));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<OrderMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }


    // Midtrans Handling Notification
    @PostMapping(value = "/notification", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto<String>> handleNotification(@RequestBody Map<String, Object> res) {
        try {
            ResponseDto<String> response =
                    DtoUtil.responseDtoSuccess(orderService.notificationHooks(res));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<String> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
