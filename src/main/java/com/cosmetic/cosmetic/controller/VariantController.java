package com.cosmetic.cosmetic.controller;

import com.cosmetic.cosmetic.dto.ResponseDto;
import com.cosmetic.cosmetic.dto.variant.VariantDto;
import com.cosmetic.cosmetic.dto.variant.VariantMappedDto;
import com.cosmetic.cosmetic.dto.variant.VariantSaveDto;
import com.cosmetic.cosmetic.service.VariantService;
import com.cosmetic.cosmetic.util.DtoUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Api(tags = "Variant")
@RequestMapping("/api/v1/variant")
@RestController
public class VariantController {
    @Autowired
    private VariantService variantService;

    @PostMapping
    public ResponseEntity<ResponseDto<VariantMappedDto>> createVariant(@RequestBody VariantSaveDto variantSaveDto) {
        try {
            ResponseDto<VariantMappedDto> response =
                    DtoUtil.responseDtoSuccess(variantService.create(variantSaveDto));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<VariantMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDto<VariantMappedDto>> findOneVariant(@PathVariable UUID id) {
        try {
            ResponseDto<VariantMappedDto> response =
                    DtoUtil.responseDtoSuccess(variantService.findOne(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<VariantMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    public ResponseEntity<ResponseDto<List<VariantMappedDto>>> findAllVariant() {
        try {
            ResponseDto<List<VariantMappedDto>> response =
                    DtoUtil.responseDtoSuccess(variantService.findAll());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<List<VariantMappedDto>> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "", authorizations = {@Authorization(value = "Bearer")})
    @PutMapping
    public ResponseEntity<ResponseDto<VariantMappedDto>> updateVariant(@RequestBody VariantDto variant) {
        try {
            ResponseDto<VariantMappedDto> response =
                    DtoUtil.responseDtoSuccess(variantService.update(variant));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<VariantMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "", authorizations = {@Authorization(value = "Bearer")})
    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseDto<Boolean>> deleteVariant(@PathVariable UUID id) {
        try {
            ResponseDto<Boolean> response =
                    DtoUtil.responseDtoSuccess(variantService.delete(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<Boolean> response =
                    DtoUtil.responseDtoFailed(Boolean.FALSE, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
