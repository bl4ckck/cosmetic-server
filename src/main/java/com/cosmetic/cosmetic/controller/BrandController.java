package com.cosmetic.cosmetic.controller;

import com.cosmetic.cosmetic.dto.ResponseDto;
import com.cosmetic.cosmetic.dto.brand.BrandDto;
import com.cosmetic.cosmetic.dto.brand.BrandMappedDto;
import com.cosmetic.cosmetic.dto.brand.BrandSaveDto;
import com.cosmetic.cosmetic.service.BrandService;
import com.cosmetic.cosmetic.util.DtoUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Api(tags = "Brand")
@RequestMapping("/api/v1/brand")
@RestController
public class BrandController {
    @Autowired
    private BrandService brandService;

    @PostMapping
    public ResponseEntity<ResponseDto<BrandMappedDto>> createBrand(@RequestBody BrandSaveDto brandSaveDto) {
        try {
            ResponseDto<BrandMappedDto> response =
                    DtoUtil.responseDtoSuccess(brandService.create(brandSaveDto));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<BrandMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDto<BrandMappedDto>> findOneBrand(@PathVariable UUID id) {
        try {
            ResponseDto<BrandMappedDto> response =
                    DtoUtil.responseDtoSuccess(brandService.findOne(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<BrandMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    public ResponseEntity<ResponseDto<List<BrandMappedDto>>> findAllBrand() {
        try {
            ResponseDto<List<BrandMappedDto>> response =
                    DtoUtil.responseDtoSuccess(brandService.findAll());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<List<BrandMappedDto>> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "", authorizations = {@Authorization(value = "Bearer")})
    @PutMapping
    public ResponseEntity<ResponseDto<BrandMappedDto>> updateBrand(@RequestBody BrandDto brand) {
        try {
            ResponseDto<BrandMappedDto> response =
                    DtoUtil.responseDtoSuccess(brandService.update(brand));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<BrandMappedDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "", authorizations = {@Authorization(value = "Bearer")})
    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseDto<Boolean>> deleteBrand(@PathVariable UUID id) {
        try {
            ResponseDto<Boolean> response =
                    DtoUtil.responseDtoSuccess(brandService.delete(id));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<Boolean> response =
                    DtoUtil.responseDtoFailed(Boolean.FALSE, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
