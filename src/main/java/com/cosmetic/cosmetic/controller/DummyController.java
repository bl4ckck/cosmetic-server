package com.cosmetic.cosmetic.controller;

import com.cosmetic.cosmetic.dto.ResponseDto;
import com.cosmetic.cosmetic.dto.dummy.UploadFileDto;
import com.cosmetic.cosmetic.util.DtoUtil;
import com.cosmetic.cosmetic.util.ExcelUtil;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Dummy")
@RequestMapping("/api/v1/dummy")
@RestController
public class DummyController {
    @PostMapping(consumes = {"multipart/form-data"})
    public ResponseEntity<ResponseDto<Object>> createDummyUploadFile(@ModelAttribute UploadFileDto payload){
        try {
            ResponseDto<Object> response = DtoUtil.responseDtoSuccess(null);
            ExcelUtil excelUtil = new ExcelUtil();
            excelUtil.processOneSheet("/home/alvin/Downloads/coba-100mb.xlsx");

            return new ResponseEntity<>(response, HttpStatus.CREATED);
        }catch (Exception e){
            ResponseDto<Object> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
