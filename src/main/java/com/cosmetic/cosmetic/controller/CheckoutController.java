package com.cosmetic.cosmetic.controller;

import com.cosmetic.cosmetic.dto.ResponseDto;
import com.cosmetic.cosmetic.dto.midtrans.CoreResponseDto;
import com.cosmetic.cosmetic.dto.order.CheckoutCartDto;
import com.cosmetic.cosmetic.dto.order.CheckoutDto;
import com.cosmetic.cosmetic.service.CheckoutService;
import com.cosmetic.cosmetic.service.OrderService;
import com.cosmetic.cosmetic.util.DtoUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Api(tags = "Checkout")
@RequestMapping("/api/v1/checkout")
@RestController
public class CheckoutController {
    @Autowired
    private CheckoutService checkoutService;

    @Autowired
    private OrderService orderService;

    @ApiOperation(value = "", authorizations = { @Authorization(value="Bearer") })
    @PostMapping
    public ResponseEntity<ResponseDto<CoreResponseDto>> checkout(@RequestBody CheckoutDto payload) {
        try {
            ResponseDto<CoreResponseDto> response =
                    DtoUtil.responseDtoSuccess(orderService.checkout(payload));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<CoreResponseDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "", authorizations = { @Authorization(value="Bearer") })
    @GetMapping
    public ResponseEntity<ResponseDto<CheckoutCartDto>> getCheckout() {
        try {
            ResponseDto<CheckoutCartDto> response =
                    DtoUtil.responseDtoSuccess(checkoutService.getCheckout());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            ResponseDto<CheckoutCartDto> response =
                    DtoUtil.responseDtoFailed(null, e.getMessage(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
    }
}
