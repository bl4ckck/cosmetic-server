package com.cosmetic.cosmetic.util;

import java.util.Map;
import java.util.stream.Collectors;

public class MapUtil {
    public static Map<String, String> mapObjectToString(Map<String, Object> map) {
        return map.entrySet().stream()
                .filter(entry -> entry.getValue() != null)
                .collect(Collectors.toMap(Map.Entry::getKey,
                        entry -> entry.getValue().toString()));
    }
}
