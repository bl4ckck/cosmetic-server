package com.cosmetic.cosmetic.util;

import com.cosmetic.cosmetic.dto.ResponseDto;
import org.springframework.http.HttpStatus;

public class DtoUtil {
    public static <T> ResponseDto<T> responseDtoSuccess(T data) {
        return new ResponseDto<>(
                true,
                HttpStatus.OK.toString(),
                "Data Found",
                data
        );
    }

    public static <T> ResponseDto<T> responseDtoFailed(T data) {
        return new ResponseDto<>(
                false,
                HttpStatus.OK.toString(),
                "Data Not Found",
                data
        );
    }
    public static <T> ResponseDto<T> responseDtoFailed(T data, String errorMessage, HttpStatus httpStatus) {
        return new ResponseDto<>(
                false,
                httpStatus.toString(),
                errorMessage,
                data
        );
    }
}
