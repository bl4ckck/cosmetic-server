package com.cosmetic.cosmetic.util;

import com.cosmetic.cosmetic.dto.DummyDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.poi.xssf.model.SharedStrings;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class SheetHandler extends DefaultHandler {
    private SharedStrings sst;
    private String lastContents;
    private boolean nextIsString;
    private long rowNumber = 0L;
    private long tmpRowNumber = 0L;

    private List<DummyDto> dummyDtoList = new ArrayList<>();
    private DummyDto dummyDto = new DummyDto();

    private int countCell = 0;

    SheetHandler(SharedStrings sst) {
        this.sst = sst;
    }

    @Override
    public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
        // c => cell
        if (name.equals("row")) {
            String r = attributes.getValue("r");
            this.tmpRowNumber = this.rowNumber;
            this.rowNumber = Long.parseLong(r);
            System.out.println("#######urut: " + rowNumber);
        }

        if(name.equals("c")) {
            // Print the cell reference
            System.out.print(attributes.getValue("r") + " - ");
            // Figure out if the value is an index in the SST
            String cellType = attributes.getValue("t");
            if (cellType != null && cellType.equals("s")) {
                nextIsString = true;
                this.countCell++;
            }
            else nextIsString = false;
        }
        // Clear contents cache
        lastContents = "";
    }

    @Override
    public void endElement(String uri, String localName, String name)
            throws SAXException {
        // Process the last contents as required.
        // Do now, as characters() may be called more than once
        if(nextIsString) {
            int idx = Integer.parseInt(lastContents);
            lastContents = sst.getItemAt(idx).getString();
            nextIsString = false;
        }
        // reset to 0 if different row
        if (this.rowNumber != this.tmpRowNumber) {
            System.out.println("reset to 0");
            this.countCell = 0;
            this.tmpRowNumber = this.rowNumber;
        }
        // v => contents of a cell
        // Output after we've seen the string contents
        if (name.equals("v")) {
            System.out.print(lastContents);
            System.out.println("COUNT CELL : " + this.countCell);

            if (this.countCell <= 2) {
                // process value
                if (this.countCell == 0) {
                    this.dummyDto.setVarcharCustom1(lastContents);
                } else if (this.countCell == 1) {
                    this.dummyDto.setVarcharCustom2(lastContents);
                } else if (this.countCell == 2) {
                    this.dummyDto.setVarcharCustom3(lastContents);
                    this.dummyDtoList.add(this.dummyDto);
                    this.dummyDto = new DummyDto();
                }
            }
        }

    }

    @Override
    public void endDocument() throws SAXException {
        super.endDocument();
        System.out.println("==== index FIRST = " + this.getDummyDtoList().get(0));
        System.out.println("==== index last = " + this.getDummyDtoList().get(1024480));
        System.out.println("==== size = " + this.getDummyDtoList().size());
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        lastContents += new String(ch, start, length);
    }
}
