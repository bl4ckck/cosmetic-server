package com.cosmetic.cosmetic.exception;

public class GeneralException extends RuntimeException {
    public GeneralException(String message) {
        super(message);
    }
}
