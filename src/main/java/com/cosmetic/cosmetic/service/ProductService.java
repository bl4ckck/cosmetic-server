package com.cosmetic.cosmetic.service;

import ch.qos.logback.core.hook.DelayingShutdownHook;
import com.cosmetic.cosmetic.dto.product.*;
import com.cosmetic.cosmetic.entity.Product;
import com.cosmetic.cosmetic.mapper.product.ProductDetailInfo;
import com.cosmetic.cosmetic.mapper.product.ProductInfo;
import com.cosmetic.cosmetic.mapper.product.ProductMappedMapper;
import com.cosmetic.cosmetic.repository.ProductRepository;
import com.github.javafaker.Bool;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import com.cosmetic.cosmetic.service.database.DBCrudServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductService extends DBCrudServiceImpl<ProductMappedDto, Product, UUID, ProductDto,
        ProductSaveDto, ProductRepository, ProductMappedMapper> {
    public ProductService() {
        super(Product.class);
    }

    public List<ProductTrendingDto> findAllProductTrending() {
        List<ProductInfo> productTrending = this.getRepository().findAllProductTrending();
        return this.getMapper().toTrendingListDto(productTrending);
    }

    public PaginatedProductResponse findAllPaginated(Integer page, Integer size){
        Pageable paging = PageRequest.of(page, size);
        Page<Product> products = this.getRepository().findAll(paging);
        return PaginatedProductResponse.builder()
                .numberOfItems(products.getTotalElements()).numberOfPages(products.getTotalPages())
                .products(this.getMapper().toListFromPage(products))
                .build();
    }

    public PaginatedProductResponse findAllProductByCategories(ProductFilterDto filterCategories, Integer page, Integer size) {
        Pageable paging = PageRequest.of(page, size);
        List<String> category = filterCategories.getCategory();
        List<String> subcategory = filterCategories.getSubcategory();
        List<String> subChildCategory = filterCategories.getSubChildCategory();

        Page<Product> products = this.getRepository().findAllProductByCategoriesIn(category, subcategory, subChildCategory, paging);
        return PaginatedProductResponse.builder()
                .numberOfItems(products.getTotalElements()).numberOfPages(products.getTotalPages())
                .products(this.getMapper().toListFromPage(products))
                .build();
    }

    public PaginatedProductResponse findAllProductByReviewAndPrice(ProductFilterByReviewAndPriceDto filter, Integer page, Integer size) {
        Pageable paging = PageRequest.of(page, size);
        List<Double> reviews = filter.getReview();
        Boolean lowPrice = filter.getLowPrice();
        Boolean highPrice = filter.getHightPrice();
        Boolean isOrganic = filter.getIsOrganic();
        Page<Product> products;

        if (Boolean.TRUE.equals(highPrice)){
            products = this.getRepository().findAllProductByReviewAndPriceToLow(reviews, isOrganic, paging);
        }else if (Boolean.TRUE.equals(lowPrice)){
            products = this.getRepository().findAllProductByReviewAndPriceToHigh(reviews,isOrganic, paging);
        }else {
            products = this.getRepository().findAllProductByReviewAndPriceToLow(reviews,isOrganic, paging);
        }

        return PaginatedProductResponse.builder()
                .numberOfItems(products.getTotalElements()).numberOfPages(products.getTotalPages())
                .products(this.getMapper().toListFromPage(products))
                .build();
    }

    public PaginatedProductResponse findAllProductAndBrandBySearchKeyword(String keyword,
                                                                          Integer page,
                                                                          Integer size) {
        Pageable paging = PageRequest.of(page, size);
        Page<Product> products = this.getRepository().findAllProductAndBrandBySearchKeyword(keyword, paging);
        return PaginatedProductResponse.builder()
                .numberOfItems(products.getTotalElements()).numberOfPages(products.getTotalPages())
                .products(this.getMapper().toListFromPage(products))
                .build();
    }

    public ProductDetailInfo findOneProduct(UUID id) {
        Optional<ProductDetailInfo> data = this.getRepository().findOneProductWithRating(id);
        return data.orElse(null);
    }
}
