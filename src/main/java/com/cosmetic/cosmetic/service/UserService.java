package com.cosmetic.cosmetic.service;

import com.cosmetic.cosmetic.dto.user.UserDto;
import com.cosmetic.cosmetic.dto.user.UserMappedDto;
import com.cosmetic.cosmetic.dto.user.UserSaveDto;
import com.cosmetic.cosmetic.entity.User;
import com.cosmetic.cosmetic.mapper.user.UserMappedMapper;
import com.cosmetic.cosmetic.repository.AccountRepository;
import com.cosmetic.cosmetic.repository.UserRepository;
import com.cosmetic.cosmetic.service.database.DBCrudServiceImpl;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Service
public class UserService extends DBCrudServiceImpl<UserMappedDto, User, UUID,
        UserDto, UserSaveDto, UserRepository, UserMappedMapper> {
    @Autowired
    private AccountRepository accountRepository;

    public UserService() {
        super(User.class);
    }

    @Override
    public UserMappedDto update(UserDto payload) {
        Optional<User> data = this.getRepository().findById(payload.getId());
        if (data.isPresent()) {
            this.getMapper().update(payload, data.get());
            // Set isActive to true if data is not null
            Boolean isActive = data.get().getAddresses() != null &&
                    !StringUtils.isEmpty(data.get().getPhone());
            data.get().getAccount().setIsActive(isActive);
            return this.getMapper().toDto(this.getRepository().save(data.get()));
        }
        return null;
    }
}
