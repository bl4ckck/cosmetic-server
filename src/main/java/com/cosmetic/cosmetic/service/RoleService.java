package com.cosmetic.cosmetic.service;

import com.cosmetic.cosmetic.dto.role.RoleMappedDto;
import com.cosmetic.cosmetic.entity.Role;
import com.cosmetic.cosmetic.mapper.role.RoleMappedMapper;
import com.cosmetic.cosmetic.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class RoleService {
    @Autowired
    private RoleMappedMapper mapper;

    @Autowired
    private RoleRepository roleRepository;

    public List<RoleMappedDto> findAll() {
        List<Role> data = roleRepository.findAll();
        return mapper.toList(data);
    }

    public RoleMappedDto findOne(UUID id) {
        Optional<Role> data = roleRepository.findById(id);
        return data.map(role -> mapper.toDto(role)).orElse(null);
    }

    public RoleMappedDto create(Role role) {
//        role.getAccount().setRole(role);
        return mapper.toDto(roleRepository.save(role));
    }
}
