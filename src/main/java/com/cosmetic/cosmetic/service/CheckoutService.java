package com.cosmetic.cosmetic.service;

import com.cosmetic.cosmetic.component.MidtransComponent;
import com.cosmetic.cosmetic.dto.cart.CartDto;
import com.cosmetic.cosmetic.dto.midtrans.CoreResponseDto;
import com.cosmetic.cosmetic.dto.order.*;
import com.cosmetic.cosmetic.dto.rajaongkir.ROCost;
import com.cosmetic.cosmetic.dto.rajaongkir.ROCostList;
import com.cosmetic.cosmetic.dto.rajaongkir.ROCostSave;
import com.cosmetic.cosmetic.entity.*;
import com.cosmetic.cosmetic.entity.midtrans.EBank;
import com.cosmetic.cosmetic.entity.midtrans.EPaymentType;
import com.cosmetic.cosmetic.exception.GeneralException;
import com.cosmetic.cosmetic.mapper.cart.CartInfo;
import com.cosmetic.cosmetic.mapper.cart.CartMappedMapper;
import com.cosmetic.cosmetic.mapper.order.CheckoutMapper;
import com.cosmetic.cosmetic.repository.CartRepository;
import com.cosmetic.cosmetic.repository.VariantRepository;
import com.cosmetic.cosmetic.service.auth.AuthService;
import com.cosmetic.cosmetic.service.external.RajaOngkirService;
import com.midtrans.httpclient.error.MidtransError;
import lombok.Getter;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Getter
@Service
public class CheckoutService {
    @Autowired
    private MidtransComponent midtrans;

    @Autowired
    private VariantRepository variantRepository;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private CheckoutMapper checkoutMapper;

    @Autowired
    private CartMappedMapper cartMappedMapper;

    @Autowired
    private AuthService authService;

    @Autowired
    private PromoService promoService;

    @Autowired
    private RajaOngkirService rajaOngkirService;

    @Autowired
    private PaymentResponseService paymentResponseService;

    private List<Variant> variantList;
    private List<ItemDetailDto> itemDetailDtoList;
    private Double totalPrice;

    private final String CITY_ID = "501";

    public CheckoutCartDto getCheckout() {
        Optional<CartInfo> cartByUserId = this.cartRepository
                .findCartByUserId(this.authService.getUserSession().getId());

        if (cartByUserId.isPresent()) {
            CheckoutCartDto checkoutCartDto = new CheckoutCartDto();
            CartDto cartDto = this.cartMappedMapper.toCartDto(cartByUserId.get());
            Double weight = cartByUserId.get().getWeight();
            checkoutCartDto.setAddress(cartByUserId.get().getAddress());
            checkoutCartDto.setDelivery(this.calculateCost(weight > 10_000D ? 10_000D : weight));
            checkoutCartDto.setOverview(cartDto);
            return checkoutCartDto;
        }
        return null;
    }

    private List<DeliveryMethodDto> calculateCost(Double weight) {
        List<EDelivery> courierList = Arrays.asList(EDelivery.values());
        // iterate courier
        return courierList.stream().map(courier -> {
                    // parent dto
                    DeliveryMethodDto deliveryMethodDto = new DeliveryMethodDto();
                    // call RO API get cost
                    ROCost cost;
                    ROCostSave roCost = this.rajaOngkirService.defaultPayloadOngkir(courier, weight.intValue());
                    try {
                        cost = this.rajaOngkirService.getCost(roCost).get(0);
                    } catch (IOException e) {
                        throw new GeneralException(e.getMessage());
                    }
                    // iterate service
                    List<EDeliveryService> serviceList = Arrays.asList(EDeliveryService.values());
                    List<CheckoutDeliveryServiceDto> deliveryServices = serviceList.stream()
                            .map(val -> {
                                CheckoutDeliveryServiceDto deliveryItem = new CheckoutDeliveryServiceDto();
                                String courierService = this.rajaOngkirService.getCourierService(val, courier);
                                ROCostList roCostList = cost.getCosts().stream()
                                        .filter(fil -> fil.getService().equals(courierService))
                                        .collect(Collectors.toList()).get(0);
                                deliveryItem.setServiceType(val);
                                deliveryItem.setCost(roCostList.getCost().get(0).getValue());
                                return deliveryItem;
                            })
                            .collect(Collectors.toList());
                    deliveryMethodDto.setCourier(courier);
                    deliveryMethodDto.setServices(deliveryServices);
                    return deliveryMethodDto;
                })
                .collect(Collectors.toList());
    }

    // payment gateway related
    public CoreResponseDto checkout(CheckoutDto checkoutDto, UUID orderId,
                                    Double totalPrice, Double ongkirPrice,
                                    Set<OrderVariant> orderVariants) throws MidtransError, ParseException {
        // Send request token to midtrans
        Map<String, Object> payload = new HashMap<>();
        this.itemDetail(
                payload,
                checkoutDto,
                orderVariants,
                totalPrice,
                ongkirPrice
        );
        this.transactionDetail(payload, orderId);
        this.paymentType(payload, checkoutDto);
        JSONObject res = midtrans.coreTransaction(payload);
        return paymentResponseService.getResponse(res.toMap());
    }

    private void paymentType(Map<String, Object> payload, CheckoutDto checkoutDto) {
        String payment;
        EPaymentType type = checkoutDto.getPaymentType();
        EBank bank = checkoutDto.getBank();

        Map<String, Object> bankName = new HashMap<>();
        switch (type) {
            case BANK_TRANSFER:
                payment = EPaymentType.BANK_TRANSFER.paymentType;
                bankName.put("bank", bank != null ? bank.name : EBank.ALL_BANK.name);
                payload.put("bank_transfer", bankName);
                break;
            case GOPAY:
                payment = EPaymentType.GOPAY.paymentType;
                break;
            default:
                payment = "";
        }

        payload.put("payment_type", payment);
    }

    public void transactionDetail(Map<String, Object> map, UUID orderId) {
        Map<String, Object> transaction = new HashMap<>();
        transaction.put("order_id", orderId);
        transaction.put("gross_amount", map.get("transient_price"));
        map.put("transaction_details", transaction);
        map.remove("transient_price");
    }

    public void itemDetail(Map<String, Object> map, CheckoutDto checkoutDto,
                           Set<OrderVariant> variantSet, Double totalPrice, Double ongkirPrice) {
        List<ItemDetailDto> itemDetailList = this.checkoutMapper.toItemDetailListFromVariantSet(variantSet);
        this.ongkirItem(itemDetailList, checkoutDto, ongkirPrice);
        Double discountPrice = this.promoItem(itemDetailList, totalPrice, checkoutDto.getVoucherId());
        map.put("transient_price",  discountPrice);
        map.put("item_details", itemDetailList);
    }

    private void ongkirItem(List<ItemDetailDto> itemDetailList, CheckoutDto checkoutDto, Double ongkirPrice) {
        EDelivery delivery = checkoutDto.getDelivery();
        EDeliveryService deliveryService = checkoutDto.getDeliveryService();
        ItemDetailDto itemDetailDto = new ItemDetailDto(
                UUID.randomUUID(),
                "Ongkos Kirim " + delivery + " (" + deliveryService + ")",
                ongkirPrice,
                1
        );
        itemDetailList.add(itemDetailDto);
    }

    private Double promoItem(List<ItemDetailDto> itemDetailList, double total, List<UUID> voucherId) {
        if (voucherId == null) {
            return total;
        }
        List<Promo> isValidPromo = this.promoService.validityPromo(voucherId, total);
        AtomicReference<Double> discountedPrice = new AtomicReference<>(total);
        isValidPromo.forEach(val -> {
            Double price = -(total * (val.getDiscount() / 100D));
            ItemDetailDto itemDetailDto = new ItemDetailDto(
                    val.getId(),
                    val.getVoucher(),
                    price,
                    1
            );
            discountedPrice.updateAndGet(v -> v + price);
            itemDetailList.add(itemDetailDto);
        });
        if (discountedPrice.get() < 0) throw new GeneralException("Price cannot less than 0");
        return discountedPrice.get();
    }
}
