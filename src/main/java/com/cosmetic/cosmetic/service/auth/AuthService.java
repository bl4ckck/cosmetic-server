package com.cosmetic.cosmetic.service.auth;

import com.auth0.jwt.interfaces.Claim;
import com.cosmetic.cosmetic.component.JwtComponent;
import com.cosmetic.cosmetic.dto.TokenDto;
import com.cosmetic.cosmetic.dto.account.AccountUserDto;
import com.cosmetic.cosmetic.dto.account.LoginDto;
import com.cosmetic.cosmetic.dto.user.UserDetailsDto;
import com.cosmetic.cosmetic.entity.Account;
import com.cosmetic.cosmetic.entity.ERole;
import com.cosmetic.cosmetic.entity.User;
import com.cosmetic.cosmetic.exception.GeneralException;
import com.cosmetic.cosmetic.mapper.auth.ClaimsMapper;
import com.cosmetic.cosmetic.repository.UserRepository;
import com.cosmetic.cosmetic.service.AccountService;
import com.cosmetic.cosmetic.util.MapUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
public class AuthService {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private AccountService accountService;

    @Autowired
    private JwtDecoder jwtDecoder;

    @Autowired
    private JwtComponent jwtComponent;

    @Autowired
    private ClaimsMapper claimsMapper;

    @Autowired
    private UserRepository userRepository;

    public TokenDto loginWithOAuth2(TokenDto token) throws BadCredentialsException {
        Jwt decode = jwtDecoder.decode(token.getToken());
        // Map object to map string
        Map<String, String> claimsToString = MapUtil.mapObjectToString(decode.getClaims());
        Optional<Account> account = accountService.findByEmail(claimsToString.get("email"));
        if (account.isPresent()) {
            // User already have email password account
            if (account.get().getAccountBasic() != null) {
                if (claimsToString.get("email").equals(account.get().getEmail())) {
                    throw new BadCredentialsException("Please login with email and password");
                }
            } else {
                // Do log in
                return new TokenDto(jwtComponent.getToken(account.get()));
            }
        }
        // Do register
        AccountUserDto accountUserDto = new AccountUserDto();
        this.claimsMapper.toAccountUser(claimsToString, accountUserDto);
        accountUserDto.setRole(ERole.ROLE_CUSTOMER);
        Account accountProvider = this.accountService.createAccountProvider(accountUserDto);
        return new TokenDto(this.jwtComponent.getToken(accountProvider));
    }

    public TokenDto loginWithEmailPassword(LoginDto payload) {
        UsernamePasswordAuthenticationToken credential =
                new UsernamePasswordAuthenticationToken(
                        payload.getEmail(),
                        payload.getPassword()
                );
        Authentication authenticate = authenticationManager.authenticate(credential);
        UserDetailsDto userDetail = ((UserDetailsDto) authenticate.getPrincipal());
        return new TokenDto(jwtComponent.getToken(userDetail.getAccount()));
    }

    @SuppressWarnings(value="unchecked")
    public Map<String, Claim> getPrincipal() {
        return (Map<String, Claim>) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public User getUserSession() {
        UUID userId = UUID.fromString(
                this.getPrincipal().get("userId").asString());
        Optional<User> user = this.userRepository.findById(userId);
        if (user.isPresent()) return user.get();
        throw new GeneralException("User not found");
    }
}
