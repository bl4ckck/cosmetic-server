package com.cosmetic.cosmetic.service.auth;

import com.cosmetic.cosmetic.dto.user.UserDetailsDto;
import com.cosmetic.cosmetic.entity.Account;
import com.cosmetic.cosmetic.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserDetailService implements UserDetailsService {
    @Autowired
    private AccountRepository accountRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws BadCredentialsException {
        Optional<Account> account = accountRepository.findByEmail(email);
        if (account.isPresent()) {
            // login with provider
            if (account.get().getAccountBasic() == null) {
                throw new BadCredentialsException("Please login with google");
            }
            List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
            String role = account.get().getRole().toString();
            grantedAuthorities.add(new SimpleGrantedAuthority(role));

            return new UserDetailsDto(
                    account.get(),
                    grantedAuthorities
            );
        }

        throw new BadCredentialsException("Email / password is invalid");
    }
}
