package com.cosmetic.cosmetic.service.database.seeder;

import com.cosmetic.cosmetic.entity.Promo;
import com.cosmetic.cosmetic.repository.PromoRepository;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class PromoSeeder extends SeederImpl<Promo, PromoRepository> {
    @Override
    public void seed() {
        int maxPromo = 10;

        for (int i = 0; i < maxPromo; i++) {
            Promo promo = new Promo(
                    UUID.randomUUID(),
                    this.getFaker().commerce().productName(),
                    this.getFaker().commerce().promotionCode(),
                    this.getFaker().number().numberBetween(50, 90),
                    100000D,
                    new Date(),
                    new Date(1659998832191L)
            );
            this.getRepository().save(promo);
        }
    }
}
