package com.cosmetic.cosmetic.service.database.seeder;

import com.github.javafaker.Faker;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * @param <E> - Entity
 * @param <R> - Repository
 */
@Data
public abstract class SeederImpl<E, R extends JpaRepository<E, UUID>> {
    @Autowired
    private R repository;

    @Autowired
    private Faker faker;

    public abstract void seed();
}
