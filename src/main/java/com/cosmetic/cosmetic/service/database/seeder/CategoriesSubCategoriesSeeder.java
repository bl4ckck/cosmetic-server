package com.cosmetic.cosmetic.service.database.seeder;


import com.cosmetic.cosmetic.entity.Category;
import com.cosmetic.cosmetic.entity.SubCategory;
import com.cosmetic.cosmetic.repository.CategoryRepository;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Component
public class CategoriesSubCategoriesSeeder extends SeederImpl<Category, CategoryRepository>{
    
    @Override
    public void seed() {
        for (int i = 0; i < 10; i++) {
            Category category = new Category();
            category.setId(UUID.randomUUID());
            category.setName(this.getFaker().food().vegetable());

            Set<SubCategory> categories = new HashSet<>();
            for (int j = 0; j < i; j++) {
                SubCategory subCategory = new SubCategory();
                subCategory.setId(UUID.randomUUID());
                subCategory.setName(this.getFaker().food().fruit());
                subCategory.setCategory(category);
                categories.add(subCategory);
            }
            category.setSubCategories(categories);
            this.getRepository().save(category);
        }
    }
}