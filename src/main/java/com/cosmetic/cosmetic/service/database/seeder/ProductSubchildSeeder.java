package com.cosmetic.cosmetic.service.database.seeder;

import com.cosmetic.cosmetic.entity.Product;
import com.cosmetic.cosmetic.repository.ProductRepository;
import com.cosmetic.cosmetic.repository.SubChildRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductSubchildSeeder extends SeederImpl<Product, ProductRepository> {

    @Autowired
    private SubChildRepository subChildRepository;

    @Override
    public void seed() {
        List<Product> getProducts = this.getRepository().findAll();
        getProducts.stream()
                .peek(val -> val.setSubChild(subChildRepository.findAll().get(getFaker().random().nextInt(0,4))))
                .collect(Collectors.toList());
    }
}
