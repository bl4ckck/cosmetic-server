package com.cosmetic.cosmetic.service.database.seeder;

import com.cosmetic.cosmetic.entity.Article;
import com.cosmetic.cosmetic.entity.Ingredient;
import com.cosmetic.cosmetic.entity.SubChild;
import com.cosmetic.cosmetic.repository.*;
import com.cosmetic.cosmetic.service.external.UnsplashGeneratedService;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.time.Instant;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@Component
public class ArticleCategoriesSeeder extends SeederImpl<Article, ArticleRepository>{

    @Autowired
    private IngredientRepository ingredientRepository;

    @Autowired
    private SubChildRepository subChildRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SubCategoryRepositroy subCategoryRepositroy;

    private final String[] imagesURL = UnsplashGeneratedService.IMAGES_URL;

    @Override
    public void seed() {
        for (int j = 0; j < 5; j++) {
            SubChild subChild = new SubChild();
            subChild.setId(UUID.randomUUID());
            subChild.setName(this.getFaker().food().vegetable());
            subChild.setSubCategory(subCategoryRepositroy.findAll().get(j));
            subChildRepository.save(subChild);
        }

        for (int j = 0; j < 5; j++) {
            Ingredient ingredient = new Ingredient();
            ingredient.setId(UUID.randomUUID());
            ingredient.setName(this.getFaker().food().ingredient());
            ingredientRepository.save(ingredient);
        }

        Set<Ingredient> ingredientSet = new HashSet<>(ingredientRepository.findAll());
        Set<SubChild> subChildSet = new HashSet<>(subChildRepository.findAll());

        for (int i = 0; i < 10; i++) {
            Article article = new Article();
            article.setId(UUID.randomUUID());
            article.setDate(Date.from(Instant.now()));
            article.setUser(userRepository.findAll().get(i));
            article.setContent(this.getFaker().lorem().paragraph(10));
            article.setTitle(this.getFaker().lorem().paragraph(1));
            article.setPhoto(imagesURL[this.getFaker().random().nextInt(0, 70)]);
            article.setArticleIngredients(ingredientSet);
            article.setArticleCategories(subChildSet);
            this.getRepository().save(article);
        }
    }
}