package com.cosmetic.cosmetic.service.database.seeder;

import com.cosmetic.cosmetic.entity.Review;
import com.cosmetic.cosmetic.repository.ProductRepository;
import com.cosmetic.cosmetic.repository.ReviewRepository;
import com.cosmetic.cosmetic.repository.UserRepository;
import com.cosmetic.cosmetic.service.external.UnsplashGeneratedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Date;
import java.util.UUID;

@Component
public class ReviewSeeder extends SeederImpl<Review, ReviewRepository> {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserRepository userRepository;

    private final String[] imagesURL = UnsplashGeneratedService.IMAGES_URL;

    @Override
    public void seed() {
        String[] images = new String[5];
        for (int i = 0; i < 5; i++) {
            images[i] = imagesURL[this.getFaker().random().nextInt(0, 70)];
        }
        for (int i = 0; i < 10; i++) {
            Review review = new Review();
            review.setId(UUID.randomUUID());
            review.setContent(this.getFaker().lorem().paragraph());
            review.setDate(Date.from(Instant.now()));
            review.setUser(userRepository.findAll().get(i));
            review.setProduct(productRepository.findAll().get(i));
            review.setEffective(this.getFaker().random().nextInt(1, 5));
            review.setTexture(this.getFaker().random().nextInt(1, 5));
            review.setPackaging(this.getFaker().random().nextInt(1, 5));
            review.setPrice(this.getFaker().random().nextInt(1, 5));
            review.setImages(images);
            review.setImagesCount(images.length);
            review.setAverageStar((review.getEffective()+review.getTexture()+review.getPrice()+review.getPackaging()) / 4.);
            this.getRepository().save(review);
        }
    }
}
