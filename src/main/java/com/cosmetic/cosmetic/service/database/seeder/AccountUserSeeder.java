package com.cosmetic.cosmetic.service.database.seeder;

import com.cosmetic.cosmetic.entity.*;
import com.cosmetic.cosmetic.repository.AccountRepository;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@Component
public class AccountUserSeeder extends SeederImpl<Account, AccountRepository> {
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private Set<Address> addressSeed(User user) {
        int max = this.getFaker().number().numberBetween(1, 3);
        Set<Address> addressSet = new HashSet<>();
        for (int i = 0; i < max; i++) {
            int cityId = this.getFaker().number().numberBetween(1, 501);
            int postalCode = this.getFaker().number().numberBetween(1000, 17134);

            Address address = new Address(
                    UUID.randomUUID(),
                    this.getFaker().phoneNumber().cellPhone(),
                    this.getFaker().name().fullName(),
                    this.getFaker().address().streetName(),
                    false,
                    String.valueOf(cityId),
                    String.valueOf(postalCode),
                    this.getFaker().address().fullAddress()
            );

            if (i == 0) {
                address.setPhone(user.getPhone());
                address.setReceiver(user.getName());
                address.setIsDefault(true);
            }
            addressSet.add(address);
        }
        return addressSet;
    }

    @Override
    public void seed() {
        int max = 10;
        for (int i = 0; i < max; i++) {
            User user = new User(
                    this.getFaker().name().fullName(),
                    this.getFaker().phoneNumber().cellPhone(),
                    this.getFaker().color().name()
            );
            user.setCart(new Cart(user));

            AccountBasic accountBasic = new AccountBasic(
                    bCryptPasswordEncoder.encode("12345678")
            );
            AccountProvider accountProvider = new AccountProvider(
                    EProvider.GOOGLE
            );
            Account account = new Account(
                    UUID.randomUUID(),
                    user,
                    null,
                    null,
                    this.getFaker().internet().emailAddress(),
                    ERole.ROLE_CUSTOMER,
                    "https://ui-avatars.com/api/?name="+user.getName()+"&background=random",
                    this.getFaker().bool().bool()
            );
            if (i % 2 == 0 || i > 6) {
                // Admin role
                if (i > 6) {
                    account.setRole(ERole.ROLE_ADMIN);
                }
                account.setAccountBasic(accountBasic);
                account.getAccountBasic().setAccount(account);
            } else {
                account.setAccountProvider(accountProvider);
                account.getAccountProvider().setAccount(account);
            }
            account.getUser().setAccount(account);
            user.setAddresses(this.addressSeed(user));
            int x = 0;
            for (Address address : user.getAddresses()) {
                address.setUser(user);
                if (x == 0) {
                    Cart cart = user.getCart();
                    cart.setAddress(address);
                    user.setCart(cart);
                }
                x++;
            }
            this.getRepository().save(account);
        }
    }
}
