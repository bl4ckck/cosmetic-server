package com.cosmetic.cosmetic.service.database;

import com.cosmetic.cosmetic.dto.DtoID;
import com.cosmetic.cosmetic.mapper.BasicMapper;
import com.cosmetic.cosmetic.service.CrudService;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;

/**
 *  @param <T> Dto response
 *  @param <E> Entity
 *  @param <I> ID type
 *  @param <U> Dto for update
 *  @param <D> Dto for save
 *  @param <R> JpaRepository type
 *  @param <M> Mapper
 */
@RequiredArgsConstructor
@Data
public class DBCrudServiceImpl<T, E, I,
        U extends DtoID<I>, D,
        R extends JpaRepository<E, I>,
        M extends BasicMapper<E, T, U, D>> implements CrudService<T, I, U, D> {
    @Autowired
    private R repository;

    @Autowired
    private M mapper;

    @NonNull
    private Class<E> entityClass;

    @Override
    public T create(D payload) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        // Create instance from Class<E>
        Constructor<E> constructor = entityClass.getDeclaredConstructor();
        E entity = constructor.newInstance();
        // Mapping dto to entity
        mapper.create(payload, entity);
        this.repository.save(entity);
        return mapper.toDto(entity);
    }

    @Override
    public List<T> findAll() {
        List<E> data = this.repository.findAll();
        return mapper.toList(data);
    }

    public List<T> findAll(List<E> list) {
        return mapper.toList(list);
    }

    @Override
    public T findOne(I id) {
        Optional<E> data = repository.findById(id);
        return data.map(mapper::toDto).orElse(null);
    }

    @Override
    public T update(U payload) {
        Optional<E> data = repository.findById(payload.getId());
        if (data.isPresent()) {
            mapper.update(payload, data.get());
            return mapper.toDto(repository.save(data.get()));
        }
        return null;
    }

    @Override
    public Boolean delete(I id) {
        repository.deleteById(id);
        return Boolean.TRUE;
    }
}
