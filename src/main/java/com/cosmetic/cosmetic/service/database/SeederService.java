package com.cosmetic.cosmetic.service.database;

import com.cosmetic.cosmetic.service.database.seeder.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class SeederService {
    @Autowired
    private ReviewSeeder reviewSeeder;

    @Autowired
    private AccountUserSeeder accountUserSeeder;

    @Autowired
    private BrandProductSeeder brandProductSeeder;

    @Autowired
    private PromoSeeder promoSeeder;

    @Autowired
    private ArticleCategoriesSeeder articleCategoriesSeeder;

    @Autowired
    private CategoriesSubCategoriesSeeder categoriesSubCategoriesSeeder;

    @Autowired
    private ProductSubchildSeeder productSubchildSeeder;
    @Transactional
    public Boolean execute(String ddlType) {
        if (ddlType.equals("create")) {
            this.accountUserSeeder.seed();
            this.brandProductSeeder.seed();
            this.reviewSeeder.seed();
            this.promoSeeder.seed();
            this.categoriesSubCategoriesSeeder.seed();
            this.articleCategoriesSeeder.seed();
            this.productSubchildSeeder.seed();
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
}
