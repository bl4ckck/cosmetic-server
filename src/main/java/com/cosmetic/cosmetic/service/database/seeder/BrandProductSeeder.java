package com.cosmetic.cosmetic.service.database.seeder;

import com.cosmetic.cosmetic.entity.*;
import com.cosmetic.cosmetic.repository.BrandRepository;
import com.cosmetic.cosmetic.service.external.UnsplashGeneratedService;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@EqualsAndHashCode(callSuper = true)
@Data
@Component
public class BrandProductSeeder extends SeederImpl<Brand, BrandRepository> {
    private final String[] imagesURL = UnsplashGeneratedService.IMAGES_URL;

    @Override
    public void seed() {
        int maxBrand = 5;
        int maxProduct = 10;

        for (int i = 0; i < maxBrand; i++) {
            Set<Product> products = new HashSet<>();

            for (int j = 0; j < maxProduct; j++) {
                int randVariant = this.getFaker().random().nextInt(1, 3);
                int startIdx = this.getFaker().random().nextInt(0, 70);
                String defaultProduct = "";
                Set<Variant> variantSet = new HashSet<>();
                List<String> productImages = Arrays.asList(imagesURL).subList(startIdx, startIdx + randVariant);

                for (int k = 0; k < randVariant; k++) {
                    Variant variant = new Variant();
                    String variantName = this.getFaker().commerce().productName();
                    if (k == 0) defaultProduct = variantName;
                    variant.setId(UUID.randomUUID());
                    variant.setName(variantName);
                    variant.setPrice(20000D);
                    variant.setWeight((double) this.getFaker().number().numberBetween(200, 1000));
                    variant.setQuantity(this.getFaker().number().numberBetween(0, 50));
                    variant.setImageIndex(k);
                    variantSet.add(variant);
                }
                Product product = new Product(
                        UUID.randomUUID(),
                        defaultProduct,
                        productImages.toArray(String[]::new),
                        this.getFaker().bool().bool()
                );
                product.setVariants(variantSet);
                product.getVariants().stream()
                        .peek(val -> val.setProduct(product))
                        .collect(Collectors.toSet());
                products.add(product);
            }

            Brand brand = new Brand(
                    UUID.randomUUID(),
                    products,
                    this.getFaker().company().name(),
                    this.getFaker().lorem().paragraph(),
                    imagesURL[this.getFaker().random().nextInt(0, 70)],
                    imagesURL[this.getFaker().random().nextInt(0, 70)]
            );
            brand.getProducts().stream()
                    .peek(val -> val.setBrand(brand))
                    .collect(Collectors.toSet());
            this.getRepository().save(brand);
        }
    }
}
