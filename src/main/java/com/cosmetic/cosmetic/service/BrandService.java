package com.cosmetic.cosmetic.service;

import com.cosmetic.cosmetic.dto.brand.BrandDto;
import com.cosmetic.cosmetic.dto.brand.BrandMappedDto;
import com.cosmetic.cosmetic.dto.brand.BrandSaveDto;
import com.cosmetic.cosmetic.entity.Brand;
import com.cosmetic.cosmetic.mapper.brand.BrandMappedMapper;
import com.cosmetic.cosmetic.repository.BrandRepository;
import com.cosmetic.cosmetic.service.database.DBCrudServiceImpl;
import lombok.EqualsAndHashCode;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.util.UUID;
import java.util.stream.Collectors;

@EqualsAndHashCode(callSuper = true)
@Service
public class BrandService extends DBCrudServiceImpl<BrandMappedDto, Brand, UUID,
        BrandDto, BrandSaveDto, BrandRepository, BrandMappedMapper> {
    public BrandService() {
        super(Brand.class);
    }

    @Override
    public BrandMappedDto create(BrandSaveDto payload) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        System.out.println("masuk");
        Brand brand = new Brand();
        this.getMapper().create(payload, brand);
        brand.getProducts().stream()
                .peek(val -> val.setBrand(brand))
                .collect(Collectors.toList());
        this.getRepository().save(brand);
        return this.getMapper().toDto(brand);
    }
}
