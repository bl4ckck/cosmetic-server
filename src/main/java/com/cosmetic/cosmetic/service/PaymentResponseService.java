package com.cosmetic.cosmetic.service;

import com.cosmetic.cosmetic.dto.midtrans.CoreResponseDto;
import com.cosmetic.cosmetic.mapper.order.CheckoutMapper;
import com.cosmetic.cosmetic.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class PaymentResponseService {
    @Autowired
    private CheckoutMapper checkoutMapper;

    public CoreResponseDto getResponse(Map<String, Object> res) throws ParseException {
        CoreResponseDto coreResponseDto = this.checkoutMapper.toCoreResponse(res);
        coreResponseDto.setExpiryTime(
                this.getExpiryTime(coreResponseDto.getTransactionTime())
        );
        coreResponseDto.setVaNumber(this.getVaNumber(res));
        coreResponseDto.setBankName(this.getBankName(res));
        return coreResponseDto;
    }

    private String getFieldWrapper(Map<String, Object> res, String keyVA, String returnVal) {
        if (this.isAllBank(res)) {
            List<Map<String, Object>> va = (List<Map<String, Object>>) res.get("va_numbers");
            return (String) va.get(0).get(keyVA);
        }
        return returnVal;
    }

    private String getVaNumber(Map<String, Object> res) {
        return this.getFieldWrapper(res, "va_number", (String) res.get("permata_va_number"));
    }

    private String getBankName(Map<String, Object> res) {
        return this.getFieldWrapper(res, "bank", "permata");
    }

    private boolean isAllBank(Map<String, Object> res) {
        String isPermata = (String) res.getOrDefault("permata_va_number", null);
        return isPermata == null;
    }

    private Date getExpiryTime(Date date) {
        return DateUtil.addDays(date, 1);
    }
}
