package com.cosmetic.cosmetic.service;

import com.cosmetic.cosmetic.dto.article.*;
import com.cosmetic.cosmetic.entity.*;
import com.cosmetic.cosmetic.mapper.article.ArticleMappedMapper;
import com.cosmetic.cosmetic.mapper.ingredient.IngredientMapper;
import com.cosmetic.cosmetic.mapper.subchild.SubChildMapper;
import com.cosmetic.cosmetic.repository.*;
import com.cosmetic.cosmetic.service.database.DBCrudServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class ArticleService extends DBCrudServiceImpl<ArticleMappedDto, Article, UUID,
        ArticleDto, ArticleSaveDto, ArticleRepository, ArticleMappedMapper>{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private IngredientRepository ingredientRepository;

    @Autowired
    private SubChildRepository subChildRepository;

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private SubChildMapper subchildMapper;

    @Autowired
    private IngredientMapper ingredientMapper;

    @Autowired
    private ProductRepository productRepository;

    public ArticleService() {
        super(Article.class);
    }

    @Override
    public ArticleMappedDto create(ArticleSaveDto payload) {
        try{
            List<UUID> uuids = this.extractUuidFromCategoryList(payload.getProductCategories());
            List<UUID> uuidList = this.extractUuidFromIngredientList(payload.getIngredientCategories());
            Set<SubChild> subChildsByIdIn = subChildRepository.findAllByIdIn(uuids).orElseThrow();
            Set<Ingredient> ingredientsByIdIn = ingredientRepository.findAllByIdIn(uuidList).orElseThrow();
            this.mergeToCategoryList(subChildsByIdIn, payload.getProductCategories());
            this.mergeToIngredientList(ingredientsByIdIn, payload.getIngredientCategories());

            Article article = new Article();
            this.getMapper().create(payload, article);
            article.setArticleCategories(subChildsByIdIn);
            article.setArticleIngredients(ingredientsByIdIn);
            this.getRepository().save(article);
            return this.getMapper().toDto(article);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param articleCategoriesDtos
     * @return
     */
    public List<UUID> extractUuidFromCategoryList(List<ArticleCategoriesDto> articleCategoriesDtos) {
        return articleCategoriesDtos.stream()
                .map(ArticleCategoriesDto::getSubChildId)
                .collect(Collectors.toList());
    }

    private void mergeToCategoryList(Set<SubChild> subChildList, List<ArticleCategoriesDto> articleCategoriesDtos) {
        subChildList.stream()
                .map(val -> {
                    ArticleCategoriesDto articleCategoriesDto = articleCategoriesDtos
                            .stream()
                            .filter(article -> article.getSubChildId().equals(val.getId()))
                            .collect(Collectors.toList()).get(0);
                    subchildMapper.mergeToSubchild(articleCategoriesDto, val);
                    return val;
                })
                .collect(Collectors.toList());
    }

    /**
     *
     * @param articleIngredientsDtos
     * @return
     */
    public List<UUID> extractUuidFromIngredientList(List<ArticleIngredientsDto> articleIngredientsDtos) {
        return articleIngredientsDtos.stream()
                .map(ArticleIngredientsDto::getIngredientId)
                .collect(Collectors.toList());
    }
    private void mergeToIngredientList(Set<Ingredient> ingredients, List<ArticleIngredientsDto> articleIngredientsDtos) {
        ingredients.stream()
                .map(val -> {
                    ArticleIngredientsDto articleIngredientsDto = articleIngredientsDtos
                            .stream()
                            .filter(article -> article.getIngredientId().equals(val.getId()))
                            .collect(Collectors.toList()).get(0);
                    ingredientMapper.mergeToIngredient(articleIngredientsDto, val);
                    return val;
                })
                .collect(Collectors.toList());
    }

    public List<ArticleMappedDto> findRelatedArticles(UUID productId) {
        List<Article> allByIdProduct = articleRepository.findAllByIdProduct(productId);
        return this.getMapper().toList(allByIdProduct);
    }
}