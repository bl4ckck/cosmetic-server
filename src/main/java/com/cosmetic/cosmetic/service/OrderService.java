package com.cosmetic.cosmetic.service;

import com.cosmetic.cosmetic.component.MidtransComponent;
import com.cosmetic.cosmetic.dto.cart.CartBrandDto;
import com.cosmetic.cosmetic.dto.cart.CartItemDto;
import com.cosmetic.cosmetic.dto.midtrans.CoreResponseDto;
import com.cosmetic.cosmetic.dto.order.*;
import com.cosmetic.cosmetic.entity.*;
import com.cosmetic.cosmetic.mapper.cart.CartMappedMapper;
import com.cosmetic.cosmetic.mapper.order.OrderMappedMapper;
import com.cosmetic.cosmetic.repository.CartRepository;
import com.cosmetic.cosmetic.repository.OrderRepository;
import com.cosmetic.cosmetic.service.auth.AuthService;
import com.cosmetic.cosmetic.service.database.DBCrudServiceImpl;
import com.cosmetic.cosmetic.service.external.RajaOngkirService;
import com.midtrans.httpclient.error.MidtransError;
import lombok.EqualsAndHashCode;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

@EqualsAndHashCode(callSuper = true)
@Service
public class OrderService extends DBCrudServiceImpl<OrderMappedDto, Order, UUID, OrderDto,
        OrderDto, OrderRepository, OrderMappedMapper> {
    @Autowired
    private CheckoutService checkoutService;

    @Autowired
    private RajaOngkirService rajaOngkirService;

    @Autowired
    private AuthService authService;

    @Autowired
    private PaymentResponseService paymentResponseService;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private CartService cartService;

    @Autowired
    private CartMappedMapper cartMappedMapper;

    @Autowired
    private MidtransComponent midtransComponent;

    public OrderService() {
        super(Order.class);
    }

    @Transactional
    public CoreResponseDto checkout(CheckoutDto checkoutDto) throws MidtransError, IOException, ParseException {
        Optional<Cart> cartByUserId =
                this.cartRepository.findByUser_Id(this.authService.getUserSession().getId());

        if (cartByUserId.isPresent()) {
            Set<CartVariant> carts = cartByUserId.get().getCarts();
            // total price calculation
            Double ongkir = this.rajaOngkirService.ongkirCost(
                    checkoutDto.getDelivery(),
                    checkoutDto.getDeliveryService(),
                    cartByUserId.get().getWeight().intValue()
            );
            Double totalPrice = cartByUserId.get().getTotal() + ongkir;
            // order instance creation
            UUID orderId = UUID.randomUUID();
            Order order = new Order(
                    orderId,
                    this.authService.getUserSession(),
                    cartByUserId.get().getAddress(),
                    null,
                    null,
                    checkoutDto.getDelivery(),
                    EStatus.WAITING_FOR_PAYMENT,
                    new Date(),
                    totalPrice
            );
            // set promo if exist
            if (checkoutDto.getVoucherId() != null) {
                order.setPromo(checkoutDto.getVoucherId().toArray(UUID[]::new));
            }
            // update order variant from cart variant
            Set<OrderVariant> orderVariant = getOrderVariant(order, carts);
            order.setOrders(orderVariant);
            // send payload to midtrans
            CoreResponseDto checkout = this.checkoutService.checkout(
                    checkoutDto,
                    orderId,
                    totalPrice,
                    ongkir,
                    order.getOrders()
            );
            // set transaction date from midtrans
            order.setDate(checkout.getTransactionTime());
            order.setTotal(checkout.getGrossAmount());
            this.getRepository().save(order);
            this.cartService.resetCart(cartByUserId.get());
            return checkout;
        }

        return null;
    }

    public Set<OrderVariant> getOrderVariant(Order order, Set<CartVariant> cartVariants) {
        return cartVariants.stream()
                .map(val -> {
                    Variant variant = val.getVariant();
                    OrderVariantKey orderVariantKey = new OrderVariantKey(
                            order.getId(),
                            variant.getId()
                    );
                    return new OrderVariant(
                            orderVariantKey,
                            order,
                            variant,
                            variant.getPrice(),
                            val.getQuantity(),
                            val.getSubTotal()
                    );
                })
                .collect(Collectors.toSet());
    }

    @Override
    public List<OrderMappedDto> findAll() {
        List<Order> orderByUser = this.getRepository().findByUser_Id(this.authService.getUserSession().getId());
        return this.getMapper().toList(orderByUser);
    }

    public OrderDetailDto findOneOrder(UUID id) throws ParseException, MidtransError {
        Optional<Order> order = this.getRepository().findById(id);
        if (order.isPresent()) {
            Map<UUID, List<OrderVariant>> variantList = order.get().getOrders().stream()
                    .collect(groupingBy(val ->
                            val.getVariant().getProduct().getBrand().getId()
                    ));

            List<CartBrandDto> cartBrandList = new ArrayList<>();
            for (Map.Entry<UUID, List<OrderVariant>> entry : variantList.entrySet()) {
                List<CartItemDto> cartItem = this.cartMappedMapper.toListOrderItem(entry.getValue());
                CartBrandDto cartBrandDto = new CartBrandDto();
                Brand brand = entry.getValue().get(0).getVariant().getProduct().getBrand();
                cartBrandDto.setId(brand.getId());
                cartBrandDto.setBrandName(brand.getName());
                cartBrandDto.setBanner(brand.getBanner());
                cartBrandDto.setItems(cartItem);
                cartBrandList.add(cartBrandDto);
            }
            OrderDetailDto orderDetailDto = this.getMapper().toDetailDto(order.get());
            JSONObject statusTransaction = midtransComponent.getStatusTransaction(order.get().getId().toString());
            CoreResponseDto midtransResponse = paymentResponseService.getResponse(statusTransaction.toMap());
            orderDetailDto.setOrderItems(cartBrandList);
            orderDetailDto.setPayment(midtransResponse);
            return orderDetailDto;
        }
        return null;
    }

    public String notificationHooks(Map<String, Object> res) {
        String notifResponse = null;
        if (!(res.isEmpty())) {
            //Get Order ID from notification body
            String orderId = (String) res.get("order_id");
            String status = (String) res.get("transaction_status");

            Order order = this.getRepository().getById(UUID.fromString(orderId));
            if (!status.equals("pending")) {
                if (status.equals("settlement")) {
                    order.setStatus(EStatus.PAID);
                } else if (status.equals("cancel") || status.equals("deny") || status.equals("expire")) {
                    order.setStatus(EStatus.CANCELED);
                }
                this.getRepository().save(order);
            }
            notifResponse = "Transaction notification received. Order ID: " + orderId
                    + ". Transaction status: " + status;
        }
        return notifResponse;
    }
}
