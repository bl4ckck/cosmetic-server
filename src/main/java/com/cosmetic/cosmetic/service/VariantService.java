package com.cosmetic.cosmetic.service;

import com.cosmetic.cosmetic.dto.variant.VariantDto;
import com.cosmetic.cosmetic.dto.variant.VariantMappedDto;
import com.cosmetic.cosmetic.dto.variant.VariantSaveDto;
import com.cosmetic.cosmetic.entity.Product;
import com.cosmetic.cosmetic.entity.Variant;
import com.cosmetic.cosmetic.exception.GeneralException;
import com.cosmetic.cosmetic.mapper.variant.VariantMappedMapper;
import com.cosmetic.cosmetic.repository.ProductRepository;
import com.cosmetic.cosmetic.repository.VariantRepository;
import com.cosmetic.cosmetic.service.database.DBCrudServiceImpl;
import lombok.EqualsAndHashCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.util.Optional;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Service
public class VariantService extends DBCrudServiceImpl<VariantMappedDto, Variant, UUID,
        VariantDto, VariantSaveDto, VariantRepository, VariantMappedMapper> {
    @Autowired
    private ProductRepository productRepository;

    public VariantService() {
        super(Variant.class);
    }

    @Override
    public VariantMappedDto create(VariantSaveDto payload) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        // Map to dto
        Variant variant = new Variant();
        Optional<Product> product = productRepository.findById(payload.getProductId());
        if (product.isPresent()) {
            this.getMapper().create(payload, variant);
            variant.setProduct(product.get());
            // Map variant to product
            this.getRepository().save(variant);
            return this.getMapper().toDto(variant);
        }
        throw new GeneralException("No product found");
    }
}
