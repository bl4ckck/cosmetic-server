package com.cosmetic.cosmetic.service;

import com.cloudinary.utils.ObjectUtils;
import com.cosmetic.cosmetic.exception.FileStorageException;
import com.cosmetic.cosmetic.config.CloudinaryConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class FileUploadService{

    @Autowired
    private CloudinaryConfig cloudinaryConfig;

    public String upload(MultipartFile file) throws IOException {
        File newFile = validate(file);
        String publicId ="public_Id="+ LocalDateTime.now()+ newFile.getName().trim().toLowerCase();
        Map uploaded = cloudinaryConfig.cloudinary().uploader().upload(newFile,
                ObjectUtils.asMap("public_id", publicId));
        newFile.delete();
        String url = (String) uploaded.get("url");
        return url;
    }

    public List<String> uploadMultiple(MultipartFile[] files) throws IOException {
        System.err.println("Masukk");
        List<String> imageUrl = new ArrayList<>();
        for (MultipartFile file: files) {
            String upload = upload(file);
            imageUrl.add(upload);
        }
        return imageUrl;
    }

    public static File validate(MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            throw new FileStorageException("Failed to store empty file.");
        }
        else if (!Objects.equals(file.getContentType().substring(0,5), "image")){
            throw new FileStorageException("File not supported.");
        }
        File convFile = new File(file.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

    public Boolean deleteImage(String imgUrl) throws IOException {
        Pattern pattern = Pattern.compile("/public_Id=");
        Matcher matcher = pattern.matcher(imgUrl);
        boolean b = matcher.find();
        int end = matcher.end();
        String str = imgUrl.substring(end);
        String public_id = "public_Id="+str.substring(0, str.lastIndexOf('.'));
        Map destroy = cloudinaryConfig.cloudinary().uploader().destroy(public_id, ObjectUtils.emptyMap());
        if (destroy.get("result").equals("ok")){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
}