package com.cosmetic.cosmetic.service;

import com.cosmetic.cosmetic.dto.DtoID;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * @param <T> Dto response
 * @param <I> ID type
 * @param <U> Dto for update
 * @param <D> Dto for save
 */
public interface CrudService<T, I, U extends DtoID<I>, D> {
    T create(D payload) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;
    List<T> findAll();
    T findOne(I id);
    T update(U payload);
    Boolean delete(I id);
}
