package com.cosmetic.cosmetic.service;

import com.cosmetic.cosmetic.dto.address.AddressDto;
import com.cosmetic.cosmetic.dto.cart.*;
import com.cosmetic.cosmetic.entity.*;
import com.cosmetic.cosmetic.exception.GeneralException;
import com.cosmetic.cosmetic.mapper.address.AddressMapper;
import com.cosmetic.cosmetic.mapper.cart.CartAddressInfo;
import com.cosmetic.cosmetic.mapper.cart.CartInfo;
import com.cosmetic.cosmetic.mapper.cart.CartMappedMapper;
import com.cosmetic.cosmetic.repository.AddressRepository;
import com.cosmetic.cosmetic.repository.CartRepository;
import com.cosmetic.cosmetic.repository.CartVariantRepository;
import com.cosmetic.cosmetic.repository.VariantRepository;
import com.cosmetic.cosmetic.service.auth.AuthService;
import com.cosmetic.cosmetic.service.database.DBCrudServiceImpl;
import lombok.EqualsAndHashCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

import static java.util.stream.Collectors.groupingBy;

@EqualsAndHashCode(callSuper = true)
@Service
public class CartService extends DBCrudServiceImpl<CartMappedDto, Cart, UUID,
        CartDto, CartSaveDto, CartRepository, CartMappedMapper> {
    @Autowired
    private VariantRepository variantRepository;

    @Autowired
    private CartVariantRepository cartVariantRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private AuthService authService;

    @Autowired
    private AddressMapper addressMapper;

    public CartService() {
        super(Cart.class);
    }

    public Map<String, Object> cartByUserId() {
        Optional<CartInfo> cartByUserId =
                this.getRepository().findCartByUserId(this.authService.getUserSession().getId());
        if (cartByUserId.isPresent()) {
            Map<String, Object> map = new HashMap<>();
            Map<UUID, List<CartInfo.CartVariantInfo>> variantList =
                    cartByUserId.get().getCarts().stream()
                            .collect(groupingBy(val ->
                                            val.getVariant().getProduct().getBrand().getId()
                                    ));

            List<CartBrandDto> cartBrandList = new ArrayList<>();
            for (Map.Entry<UUID, List<CartInfo.CartVariantInfo>> entry : variantList.entrySet()) {
                List<CartItemDto> cartItem = this.getMapper().toListCartItemDto(entry.getValue());
                CartBrandDto cartBrandDto = new CartBrandDto();
                CartInfo.BrandInfo brand = entry.getValue().get(0).getVariant().getProduct().getBrand();
                cartBrandDto.setId(brand.getId());
                cartBrandDto.setBrandName(brand.getName());
                cartBrandDto.setBanner(brand.getBanner());
                cartBrandDto.setItems(cartItem);
                cartBrandList.add(cartBrandDto);
            }

            CartDto cartDto = this.getMapper().toCartDto(cartByUserId.get());
            map.put("address", cartByUserId.get().getAddress());
            map.put("overview", cartDto);
            map.put("cartItems", cartBrandList);
            return map;
        }
        return Collections.emptyMap();
    }

    public CartDto getOverview() {
        Optional<CartInfo> cartByUserId =
                this.getRepository().findCartByUserId(this.authService.getUserSession().getId());
        return cartByUserId.map(cartInfo -> this.getMapper().toCartDto(cartInfo)).orElse(null);
    }

    @Transactional
    @Override
    public CartMappedDto create(CartSaveDto payload) {
        if (payload.getQuantity() == 0D) {
            throw new GeneralException("Quantity cannot be 0");
        }
        Optional<Cart> userCart = this.getRepository()
                .findByUser_Id(this.authService.getUserSession().getId());
        if (userCart.isPresent()) {
            Cart cart = userCart.get();
            this.calculateTotal(cart, payload);
            this.getRepository().save(cart);
            return this.getMapper().toDto(cart);
        }
        return null;
    }

    private void calculateTotal(Cart cart, CartSaveDto payload) {
        Optional<Variant> variant = this.variantRepository.findById(payload.getVariantId());
        if (variant.isPresent()) {
            Double totalPrice = cart.getTotal();
            Double totalWeight = cart.getWeight();
            Map<String, Double> subTotalItem =
                    this.insertCartVariant(cart, payload, variant.get());
            // update sub total cart, if same variant updating its quantity
            if (payload.getVariantId().compareTo(variant.get().getId()) == 0) {
                cart.setTotal((totalPrice - subTotalItem.get("default_sub_total")) + subTotalItem.get("sub_total"));
                cart.setWeight((totalWeight - subTotalItem.get("default_variant_weight")) + subTotalItem.get("total_variant_weight"));
            } else {
                cart.setTotal(totalPrice + subTotalItem.get("sub_total"));
                cart.setWeight(totalWeight + subTotalItem.get("total_variant_weight"));
            }
        } else {
            throw new GeneralException("Cannot find variant ID");
        }
    }

    @Transactional
    public AddressDto updateCartAddress(UUID addressId) {
        Optional<Address> address = this.addressRepository.findById(addressId);
        if (address.isPresent()) {
            this.getRepository().updateAddressByUser(address.get(), this.authService.getUserSession());
            return this.addressMapper.toDto(address.get());
        }
        return null;
    }

    public CartAddressInfo getAddressCart() {
        Optional<CartAddressInfo> address = this.getRepository()
                .selectAddressCartByUserId(this.authService.getUserSession().getId());
        return address.orElse(null);
    }

    private Map<String, Double> insertCartVariant(Cart cart,
                                                  CartSaveDto payload,
                                                  Variant variant) {
        boolean isSame = false;
        Map<String, Double> variantSubTotal = new HashMap<>();
        Double subTotalItem = variant.getPrice() * payload.getQuantity();
        Double totalWeightItem = variant.getWeight() * payload.getQuantity();
        // Insert cart variant
        CartVariantKey cartVariantKey = new CartVariantKey(
                cart.getId(),
                payload.getVariantId()
        );
        CartVariant cartVariant = new CartVariant(
                cartVariantKey,
                cart,
                variant,
                true,
                payload.getQuantity(),
                subTotalItem
        );

        for (CartVariant cartSet : cart.getCarts()) {
            if (cartSet.getVariant().getId().compareTo(payload.getVariantId()) == 0) {
                isSame = true;
                variantSubTotal.put("default_sub_total", cartSet.getSubTotal());
                cartSet.setQuantity(payload.getQuantity());
                cartSet.setSubTotal(subTotalItem);
                variantSubTotal.put("sub_total", subTotalItem);
                variantSubTotal.put("default_variant_weight", cartSet.getVariant().getWeight());
                variantSubTotal.put("total_variant_weight", totalWeightItem);
                break;
            }
        }
        if (!isSame) {
            variantSubTotal.put("default_sub_total", 0D);
            variantSubTotal.put("sub_total", subTotalItem);
            variantSubTotal.put("default_variant_weight", 0D);
            variantSubTotal.put("total_variant_weight", totalWeightItem);
            cart.getCarts().add(cartVariant);
        }
        cart.setCarts(cart.getCarts());
        return variantSubTotal;
    }

    @Transactional
    public Boolean actionItem(UUID variantId, Boolean isChecked, ECartAction cartAction) {
        Optional<Cart> userCart = this.getRepository()
                .findByUser_Id(this.authService.getUserSession().getId());
        if (userCart.isPresent()) {
            UUID cartId = userCart.get().getId();
            if (cartAction == ECartAction.DELETE) {
                this.resetTotalFromDeletedItem(userCart.get(), cartId, variantId, false, "delete");
                long isDeleted =
                        this.cartVariantRepository.deleteById_CartIdAndId_VariantId(
                                cartId, variantId);
                if (isDeleted == 1) return Boolean.TRUE;
            } else {
                CartVariantKey cartVariantKey = new CartVariantKey(
                        cartId,
                        variantId
                );
                this.resetTotalFromDeletedItem(userCart.get(), cartId, variantId, isChecked, "check");
                int updateChecked = this.cartVariantRepository.updateIsCheckedById(isChecked, cartVariantKey);
                if (updateChecked == 1) return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    private void resetTotalFromDeletedItem(Cart cart, UUID cartId, UUID variantId, Boolean isChecked, String type) {
        Optional<CartVariant> variant =
                this.cartVariantRepository.findById_CartIdAndId_VariantId(cartId, variantId);
        if (variant.isPresent()) {
            Double totalPrice = cart.getTotal();
            Double totalWeight = cart.getWeight();
            Double subTotal = variant.get().getSubTotal();
            Double subTotalWeight = variant.get().getVariant().getWeight() * variant.get().getQuantity();
            boolean notSame = !isChecked.equals(variant.get().getIsChecked());
            if (type.equals("check") && notSame) {
                if (isChecked.equals(Boolean.FALSE) && variant.get().getIsChecked().equals(Boolean.TRUE)) {
                    cart.setTotal(totalPrice - subTotal);
                    cart.setWeight(totalWeight - subTotalWeight);
                } else {
                    cart.setTotal(totalPrice + subTotal);
                    cart.setWeight(totalWeight + subTotalWeight);
                }
            } else if (type.equals("delete")) {
                cart.setTotal(totalPrice - subTotal);
                cart.setWeight(totalWeight - subTotalWeight);
            }
            this.getRepository().save(cart);
        }
    }

    public void resetCart(Cart cartByUserId) {
        // remove cart items
        this.cartVariantRepository.deleteByCart(cartByUserId);
        // reset cart data
        cartByUserId.setWeight(0D);
        cartByUserId.setTotal(0D);
        cartByUserId.setDelivery(null);
        cartByUserId.setPromo(null);
        this.getRepository().save(cartByUserId);
    }
}
