package com.cosmetic.cosmetic.service;

import com.cosmetic.cosmetic.dto.review.*;
import com.cosmetic.cosmetic.entity.*;
import com.cosmetic.cosmetic.mapper.review.*;
import com.cosmetic.cosmetic.repository.ProductRepository;
import com.cosmetic.cosmetic.repository.ReviewRepository;
import com.cosmetic.cosmetic.repository.UserRepository;
import com.cosmetic.cosmetic.service.database.DBCrudServiceImpl;
import lombok.EqualsAndHashCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Service
public class ReviewService extends DBCrudServiceImpl<ReviewMappedDto, Review, UUID, ReviewDto, ReviewSaveDto, ReviewRepository, ReviewMappedMapper> {
    public ReviewService() {
        super(Review.class);
    }

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ReviewMappedMapper mappedMapper;

    @Autowired
    private ReviewMapper mapper;

    @Autowired
    private FileUploadService uploadService;

    public ReviewMappedDto createReview(ReviewSaveDto payload) throws IOException{
        Optional<User> user = this.userRepository.findById(payload.getUserId());
        Optional<Product> product = this.productRepository.findById(payload.getProductId());
        List<String> listImages = this.uploadService.uploadMultiple(payload.getFiles().toArray(new MultipartFile[0]));
        int total = payload.getEffective() + payload.getPackaging() + payload.getPrice() + payload.getTexture();
        Double averageStar = total/4.;
        String[] uploadedArr;

        // Convert list to array of string
        uploadedArr = listImages.stream().toArray(String[]::new);

        if (user.isPresent() && product.isPresent()){
            Review review = new Review();
            this.getMapper().create(payload, review);
            review.setAverageStar(averageStar);
            review.setImagesCount(payload.getFiles().size());
            review.setImages(uploadedArr);
            review.setAverageStar(averageStar);
            this.getRepository().save(review);
            return this.getMapper().toDto(review);
        }
        return null;
    }

    @Override
    public ReviewMappedDto update(ReviewDto payload){
        Optional<Review> byId = reviewRepository.findById(payload.getId());
        int total = payload.getEffective() + payload.getPackaging() + payload.getPrice() + payload.getTexture();
        Double averageStar = total/4.;
        int totalImage = byId.get().getImages().length;

        if (byId.isPresent()){
            Review review = new Review();
            this.getMapper().update(payload, review);
            review.setUser(byId.get().getUser());
            review.setProduct(byId.get().getProduct());
            review.setAverageStar(averageStar);
            review.setImages(byId.get().getImages());
            review.setImagesCount(totalImage);
            reviewRepository.save(review);
            return this.getMapper().toDto(review);
        }
        return null;
    }

    public Integer countReviewByProdId(UUID productId) {
        Optional<Product> productOptional = productRepository.findById(productId);
        if (productOptional.isPresent()){
            Set<Review> reviews = productOptional.get().getReviews();
            return  reviews.size();
        }
        return null;
    }

    public ReviewPaginatedDto findAllReviewByProductId(UUID productId, Integer pageNo, Integer pageSize){
        if (pageNo == null || pageSize == null){
            pageNo = 0;
            pageSize = 12;
        }
        Pageable paging = PageRequest.of(pageNo, pageSize);
        Page<Review> reviews = reviewRepository.findAllReviewByProductId(productId, paging);
        return ReviewPaginatedDto.builder()
                .numberOfItems(reviews.getTotalElements()).numberOfPages(reviews.getTotalPages())
                .reviews(this.getMapper().toListFromPage(reviews))
                .build();
    }
}
