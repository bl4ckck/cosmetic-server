package com.cosmetic.cosmetic.service;

import com.cosmetic.cosmetic.component.JwtComponent;
import com.cosmetic.cosmetic.dto.TokenDto;
import com.cosmetic.cosmetic.dto.account.AccountMappedDto;
import com.cosmetic.cosmetic.dto.account.AccountSaveDto;
import com.cosmetic.cosmetic.dto.account.AccountUpdateDto;
import com.cosmetic.cosmetic.dto.account.AccountUserDto;
import com.cosmetic.cosmetic.dto.address.AddressSaveDto;
import com.cosmetic.cosmetic.entity.*;
import com.cosmetic.cosmetic.exception.GeneralException;
import com.cosmetic.cosmetic.mapper.account.AccountMappedMapper;
import com.cosmetic.cosmetic.mapper.address.AddressMappedMapper;
import com.cosmetic.cosmetic.repository.AccountRepository;
import com.cosmetic.cosmetic.service.database.DBCrudServiceImpl;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Service
public class AccountService extends DBCrudServiceImpl<AccountMappedDto, Account, UUID,
        AccountUpdateDto, AccountSaveDto, AccountRepository, AccountMappedMapper> {
    @Autowired
    private BCryptPasswordEncoder encoder;

    @Autowired
    private JwtComponent jwtComponent;

    @Autowired
    private AddressMappedMapper addressMapper;

    private final String ERR_NO_DEFAULT = "User must have at least one default address";

    public AccountService() {
        super(Account.class);
    }

    @Transactional
    public TokenDto createAccount(AccountSaveDto payload) {
        if (StringUtils.isEmpty(payload.getUser().getName())) {
            throw new GeneralException("Name can't be empty");
        }
        String encoded = encoder.encode(payload.getPassword());
        Account account = new Account();
        AccountBasic accountBasic = new AccountBasic(encoded);
        // transform to Account & create User instance
        this.getMapper().create(payload, account);
        account.setAccountBasic(accountBasic);
        // connecting with Account
        account.getAccountBasic().setAccount(account);
        account.getUser().setAccount(account);
        if (payload.getUser().getAddress() != null) {
            account.getUser().setAddresses(
                    this.getAddressSet(payload.getUser().getAddress(), account.getUser()));
        }
        account.getUser().setCart(new Cart(account.getUser()));
        this.getRepository().save(account);
        // set response token
        return new TokenDto(jwtComponent.getToken(account));
    }

    private Set<Address> getAddressSet(AddressSaveDto addressSaveDto, User user) {
        if (addressSaveDto.getIsDefault() == Boolean.FALSE) {
            throw new GeneralException(ERR_NO_DEFAULT);
        }
        Address address = new Address();
        Set<Address> addressSet = new HashSet<>();
        addressMapper.create(addressSaveDto, address);
        address.setUser(user);
        addressSet.add(address);
        return addressSet;
    }

    @Transactional
    public Account createAccountProvider(AccountUserDto payload) {
        Account account = new Account();
        AccountProvider accountProvider = new AccountProvider(EProvider.GOOGLE);
        // transform to Account & create User instance
        this.getMapper().createAccountProvider(payload, account);
        account.setAccountProvider(accountProvider);
        // connecting with Account
        account.getAccountProvider().setAccount(account);
        account.getUser().setAccount(account);
        account.getUser().setCart(new Cart(account.getUser()));
        return this.getRepository().save(account);
    }

    public Optional<Account> findByEmail(String email) {
        return this.getRepository().findByEmail(email);
    }

    @Override
    public AccountMappedDto update(AccountUpdateDto accountDto) {
        Optional<Account> account =
                this.getRepository().findById(accountDto.getId());

        if (account.isPresent()) {
            if (account.get().getAccountBasic() != null) {
                if (accountDto.getPassword() != null) {
                    String encoded = encoder.encode(accountDto.getPassword());
                    accountDto.setPassword(encoded);
                }
                this.getMapper().update(accountDto, account.get());
            } else {
                this.getMapper().updateNoPassword(accountDto, account.get());
            }
            return this.getMapper()
                    .toDto(this.getRepository()
                    .save(account.get()));
        }
        return null;
    }
}
