package com.cosmetic.cosmetic.service;

import com.cosmetic.cosmetic.dto.promo.PromoMappedDto;
import com.cosmetic.cosmetic.dto.promo.PromoSaveDto;
import com.cosmetic.cosmetic.entity.Promo;
import com.cosmetic.cosmetic.exception.GeneralException;
import com.cosmetic.cosmetic.mapper.promo.PromoMappedMapper;
import com.cosmetic.cosmetic.repository.PromoRepository;
import com.cosmetic.cosmetic.service.database.DBCrudServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PromoService extends DBCrudServiceImpl<PromoMappedDto, Promo, UUID,
        PromoMappedDto, PromoSaveDto, PromoRepository, PromoMappedMapper> {
    public PromoService() {
        super(Promo.class);
    }

    public Boolean isValidPromo(Promo promo, Double total) {
        Date today = new Date();
        if (total < promo.getMinimumPrice()) {
            throw new GeneralException("Minimum price is not reached");
        } else if (today.before(promo.getStartDate())) {
            throw new GeneralException("Promo hasn't been started");
        } else if (today.after(promo.getEndDate())) {
            throw new GeneralException("Promo has been ended");
        }
        return true;
    }

    public List<Promo> validityPromo(List<UUID> voucherId, Double total) {
        List<Promo> promosByIdIn = this.getRepository().findAllByIdIn(voucherId);

        if (promosByIdIn.size() > 2) {
            throw new GeneralException("Promo can't more than 2");
        }
        promosByIdIn.stream()
                .map(val -> {
                    Boolean validPromo = isValidPromo(val, total);
                    if (validPromo.equals(Boolean.TRUE)) return val;
                    throw new GeneralException("Invalid promo");
                })
                .collect(Collectors.toList());
        return promosByIdIn;
    }
}
