package com.cosmetic.cosmetic.service.external;

import com.cosmetic.cosmetic.component.HttpComponent;
import com.cosmetic.cosmetic.dto.order.CheckoutDto;
import com.cosmetic.cosmetic.dto.rajaongkir.*;
import com.cosmetic.cosmetic.entity.EDelivery;
import com.cosmetic.cosmetic.entity.EDeliveryService;
import com.cosmetic.cosmetic.mapper.rajaongkir.RajaOngkirMapper;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import io.github.cdimascio.dotenv.Dotenv;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.cosmetic.cosmetic.entity.EDelivery.*;
import static com.cosmetic.cosmetic.entity.EDeliveryService.REGULAR;

@Service
public class RajaOngkirService {
    public static final String TIER = "starter";
    public static final String BASE_URL = "https://api.rajaongkir.com/" + TIER;

    @Value( "${rajaongkir.server.key:null}" )
    private String SERVER_KEY;

    @Autowired
    private HttpComponent http;

    @Autowired
    private RajaOngkirMapper mapper;

    private final Dotenv dotenv = Dotenv.configure().ignoreIfMissing().load();
    private final Gson gson = new Gson();

    private final String CITY_ID = "501";

    private String config() {
        String serverKey;
        if (dotenv.get("RAJAONGKIR_SERVER_KEY").isEmpty()) serverKey = this.SERVER_KEY;
        else serverKey = dotenv.get("RAJAONGKIR_SERVER_KEY");
        return serverKey;
    }

    private Type getType(boolean isList) {
        if (isList) return new TypeToken<List<HashMap<String, Object>>>() {}.getType();
        return new TypeToken<HashMap<String, Object>>() {}.getType();
    }

    private String getCall(String url) throws IOException {
        return http.callGetWithHeader(url, "key", this.config());
    }

    private String postCall(String url, String payload) throws IOException {
        return http.callPostWithHeader(url, payload, "key", this.config());
    }

    private Map<String, Object> getResponse(String api) {
        Map<String, Object> map = new HashMap<>();
        JsonObject jsonObject = gson.fromJson(api, JsonObject.class);
        JsonObject rajaongkir = jsonObject.getAsJsonObject("rajaongkir");
        Integer statusCode = rajaongkir.getAsJsonObject("status").get("code").getAsInt();
        map.put("rajaongkir", rajaongkir);
        if (rajaongkir.get("results").isJsonArray()) {
            map.put("results", rajaongkir.get("results"));
        } else {
            JsonObject results = rajaongkir.getAsJsonObject("results");
            map.put("results", !results.isJsonNull() ? results : null);
        }
        map.put("statusCode", statusCode);
        return map;
    }

    public List<ROProvince> getProvinces() throws IOException {
        String api = this.getCall(BASE_URL + "/province");
        Map<String, Object> response = this.getResponse(api);
        List<Map<String, Object>> results =
                gson.fromJson((JsonElement) response.get("results"), this.getType(true));
        return mapper.toProvinceList(results);
    }
    public ROProvince getProvinces(String provinceId) throws IOException {
        String api = this.getCall(BASE_URL + "/province?id=" + provinceId);
        Map<String, Object> response = this.getResponse(api);
        Map<String, Object> map =
                gson.fromJson((JsonObject) response.get("results"), this.getType(false));
        return mapper.toProvince(map);
    }

    public List<ROCity> getCity() throws IOException {
        String api = this.getCall(BASE_URL + "/city");
        Map<String, Object> response = this.getResponse(api);
        List<Map<String, Object>> results =
                gson.fromJson((JsonElement) response.get("results"), this.getType(true));
        return mapper.toCityList(results);
    }
    public ROCity getCity(String cityId, String provinceId) throws IOException {
        String isProvinceExist = StringUtils.isEmpty(provinceId) ? null : "&province=" + provinceId;
        String api = this.getCall(BASE_URL + "/city?id=" + cityId + isProvinceExist);
        Map<String, Object> response = this.getResponse(api);
        Map<String, Object> map =
                gson.fromJson((JsonObject) response.get("results"), this.getType(false));
        return mapper.toCity(map);
    }

    public List<ROCost> getCost(ROCostSave costSave) throws IOException {
        String api = this.postCall(BASE_URL + "/cost", costSave.toString());
        Map<String, Object> response = this.getResponse(api);
        return gson.fromJson((JsonElement) response.get("results"),
                new TypeToken<List<ROCost>>() {}.getType());
    }

    public String getCourierService(EDeliveryService service, EDelivery delivery) {
        if (delivery.equals(JNE)) {
            if (service.equals(REGULAR)) {
                return "CTC";
            } else {
                return "CTCYES";
            }
        } else if (delivery.equals(POS)) {
           if (service.equals(REGULAR)) {
               return "Paket Kilat Khusus";
           } else {
               return "Pos Instan Barang";
           }
        } else if (delivery.equals(TIKI)) {
            if (service.equals(REGULAR)) {
                return "REG";
            } else {
                return "ONS";
            }
        }
        return "";
    }

    public ROCostSave defaultPayloadOngkir(EDelivery delivery, Integer weight) {
        ROCostSave roCostSave = new ROCostSave();
        roCostSave.setCourier(delivery);
        roCostSave.setWeight(weight);
        roCostSave.setCityId(CITY_ID);
        roCostSave.setDestCityId(CITY_ID);
        return roCostSave;
    }

    public Double ongkirCost(EDelivery delivery, EDeliveryService deliveryService, Integer weight) throws IOException {
        // calculate ongkir
        Integer finalWeight = weight > 10_000 ? 10_000 : weight;
        ROCostSave costSave = this.defaultPayloadOngkir(delivery, finalWeight);
        List<ROCost> cost = this.getCost(costSave);
        String courierService =
                this.getCourierService(
                        deliveryService,
                        delivery
                );
        ROCostList responseRO = cost.get(0)
                .getCosts().stream()
                .filter(val -> val.getService().equals(courierService))
                .collect(Collectors.toList())
                .get(0);
        return responseRO.getCost().get(0).getValue();
    }
}
