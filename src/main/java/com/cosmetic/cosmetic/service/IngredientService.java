package com.cosmetic.cosmetic.service;

import com.cosmetic.cosmetic.dto.ingredient.IngredientMappedDto;
import com.cosmetic.cosmetic.dto.ingredient.IngredientSaveDto;
import com.cosmetic.cosmetic.entity.Ingredient;
import com.cosmetic.cosmetic.mapper.ingredient.IngredientMappedMapper;
import com.cosmetic.cosmetic.mapper.ingredient.IngredientMapper;
import com.cosmetic.cosmetic.repository.IngredientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IngredientService {

    @Autowired
    private IngredientRepository ingredientRepository;

    @Autowired
    private IngredientMappedMapper mappedMapper;

    @Autowired
    private IngredientMapper mapper;

    public IngredientMappedDto create(IngredientSaveDto ingredientDto){
        Ingredient ingredient = mapper.saveDtoToEntity(ingredientDto);
        Ingredient save = ingredientRepository.save(ingredient);
        return mappedMapper.toMappedDto(save);
    }
}