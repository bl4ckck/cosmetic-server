package com.cosmetic.cosmetic.service;

import com.cosmetic.cosmetic.entity.Address;
import com.cosmetic.cosmetic.dto.address.AddressDto;
import com.cosmetic.cosmetic.dto.address.AddressMappedDto;
import com.cosmetic.cosmetic.dto.address.AddressSaveDto;
import com.cosmetic.cosmetic.entity.User;
import com.cosmetic.cosmetic.exception.GeneralException;
import com.cosmetic.cosmetic.mapper.address.AddressMappedMapper;
import com.cosmetic.cosmetic.repository.AddressRepository;
import com.cosmetic.cosmetic.repository.CartRepository;
import com.cosmetic.cosmetic.service.auth.AuthService;
import com.cosmetic.cosmetic.service.database.DBCrudServiceImpl;
import lombok.EqualsAndHashCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Service
public class AddressService extends DBCrudServiceImpl<AddressMappedDto, Address, UUID, AddressDto,
        AddressSaveDto, AddressRepository, AddressMappedMapper> {
    @Autowired
    private AuthService authService;

    @Autowired
    private CartRepository cartRepository;

    private final String ERR_NO_DEFAULT = "User must have at least one default address";

    public AddressService() {
        super(Address.class);
    }

    private User getUserSession() {
        return this.authService.getUserSession();
    }

    public List<AddressMappedDto> findByUserId() {
        List<Address> byUserId = this.getRepository().findByUser_Id(this.getUserSession().getId());
        return this.getMapper().toList(byUserId);
    }

    @Transactional
    @Override
    public AddressMappedDto create(AddressSaveDto payload) {
        // checking address
        List<Address> addresses = this.defaultAddressChecker(payload.getIsDefault());
        // save to DB
        Address address = new Address();
        this.getMapper().create(payload, address);
        address.setUser(this.getUserSession());
        Address saveAddress = this.getRepository().saveAndFlush(address);
        // update cart address if address is null in cart
        this.updateCartAddress(saveAddress, addresses.isEmpty(), this.getUserSession());
        return this.getMapper().toDto(address);
    }

    private void updateCartAddress(Address address, boolean isAddressEmpty, User userSession) {
        if (isAddressEmpty) {
            // insert into cart
            this.cartRepository.updateAddressByUser(address, userSession);
        }
    }

    @Transactional
    @Override
    public AddressMappedDto update(AddressDto payload) {
        Optional<Address> data = this.getRepository().findById(payload.getId());
        if (data.isPresent()) {
            if (data.get().getIsDefault() == Boolean.TRUE &&
                    payload.getIsDefault() == Boolean.FALSE) {
                throw new GeneralException(ERR_NO_DEFAULT);
            }
            if (!Objects.equals(data.get().getIsDefault(), payload.getIsDefault())) {
                defaultAddressChecker(payload.getIsDefault());
            }
            this.getMapper().update(payload, data.get());
            return this.getMapper().toDto(this.getRepository().save(data.get()));
        }
        return null;
    }

    private List<Address> defaultAddressChecker(Boolean isDefault) {
        User userSession = this.getUserSession();
        List<Address> addressesByUserId = this.getRepository().findByUser_Id(userSession.getId());
        if (isDefault == Boolean.TRUE) {
            this.getRepository().updateIsDefaultByUser(false, userSession);
        } else {
            if (addressesByUserId.isEmpty()) {
                throw new GeneralException(ERR_NO_DEFAULT);
            }
        }
        return addressesByUserId;
    }

    @Override
    public Boolean delete(UUID id) {
        Optional<Address> address = this.getRepository().findById(id);
        if (address.isPresent()) {
            if (address.get().getIsDefault().equals(Boolean.TRUE)) {
                throw new GeneralException("Can't delete default address");
            }
            return super.delete(id);
        }
        return Boolean.FALSE;
    }
}
