package com.cosmetic.cosmetic.repository;

import com.cosmetic.cosmetic.entity.Cart;
import com.cosmetic.cosmetic.entity.CartVariant;
import com.cosmetic.cosmetic.entity.CartVariantKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

public interface CartVariantRepository extends JpaRepository<CartVariant, CartVariantKey> {
    long deleteById_CartIdAndId_VariantId(UUID cartId
            , UUID variantId);

    @Transactional
    @Modifying
    @Query("update CartVariant c set c.isChecked = ?1 where c.id = ?2")
    int updateIsCheckedById(Boolean isChecked, CartVariantKey id);

    Optional<CartVariant> findById_CartIdAndId_VariantId(UUID cartId, UUID variantId);

    @Transactional
    @Modifying
    @Query("delete from CartVariant c where c.cart = ?1")
    void deleteByCart(Cart cart);
}