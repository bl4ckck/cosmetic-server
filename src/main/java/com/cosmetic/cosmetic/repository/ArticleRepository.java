package com.cosmetic.cosmetic.repository;

import com.cosmetic.cosmetic.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;
import java.util.UUID;

public interface ArticleRepository extends JpaRepository<Article, UUID> {
//        List<SubChild> findAllByIdIn(List<UUID> uuids);

        @Query("SELECT a FROM Article a JOIN a.articleIngredients ai JOIN ai.products p "+
                "JOIN p.subChild sc "+
                "WHERE p.id = :productId")
        List<Article> findAllByIdProduct(UUID productId);
}