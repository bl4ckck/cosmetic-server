package com.cosmetic.cosmetic.repository;

import com.cosmetic.cosmetic.entity.Variant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface VariantRepository extends JpaRepository<Variant, UUID> {
    List<Variant> findAllByIdIn(List<UUID> uuids);
}