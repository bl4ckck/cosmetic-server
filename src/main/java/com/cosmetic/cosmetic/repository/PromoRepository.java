package com.cosmetic.cosmetic.repository;

import com.cosmetic.cosmetic.entity.Promo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface PromoRepository extends JpaRepository<Promo, UUID> {
    List<Promo> findAllByIdIn(List<UUID> uuids);
}