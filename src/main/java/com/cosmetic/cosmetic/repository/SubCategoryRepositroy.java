package com.cosmetic.cosmetic.repository;

import com.cosmetic.cosmetic.entity.SubCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface SubCategoryRepositroy extends JpaRepository<SubCategory, UUID> {
}
