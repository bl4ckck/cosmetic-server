package com.cosmetic.cosmetic.repository;

import com.cosmetic.cosmetic.entity.Address;
import com.cosmetic.cosmetic.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

public interface AddressRepository extends JpaRepository<Address, UUID> {
    List<Address> findByUser_Id(UUID id);

    @Transactional
    @Modifying
    @Query("update Address a set a.isDefault = ?1 where a.user = ?2")
    int updateIsDefaultByUser(Boolean isDefault, User user);
}
