package com.cosmetic.cosmetic.repository;

import com.cosmetic.cosmetic.entity.SubChild;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public interface SubChildRepository extends JpaRepository<SubChild, UUID> {
    Optional<Set<SubChild>> findAllByIdIn(List<UUID> uuids);
}

