package com.cosmetic.cosmetic.repository;

import com.cosmetic.cosmetic.entity.Order;
import com.cosmetic.cosmetic.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OrderRepository extends JpaRepository<Order, UUID> {
    List<Order> findByUser_Id(UUID id);

    Optional<Order> findByUser(User user);
}