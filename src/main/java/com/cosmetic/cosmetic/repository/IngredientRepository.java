package com.cosmetic.cosmetic.repository;

import com.cosmetic.cosmetic.entity.Ingredient;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public interface IngredientRepository extends JpaRepository<Ingredient, UUID> {
    Optional<Set<Ingredient>> findAllByIdIn(List<UUID> uuids);
}