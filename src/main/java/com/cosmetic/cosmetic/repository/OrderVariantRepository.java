package com.cosmetic.cosmetic.repository;

import com.cosmetic.cosmetic.entity.OrderVariant;
import com.cosmetic.cosmetic.entity.OrderVariantKey;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderVariantRepository extends JpaRepository<OrderVariant, OrderVariantKey> {
}