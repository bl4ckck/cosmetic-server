package com.cosmetic.cosmetic.repository;

import com.cosmetic.cosmetic.entity.Product;
import com.cosmetic.cosmetic.mapper.product.ProductDetailInfo;
import com.cosmetic.cosmetic.mapper.product.ProductInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProductRepository extends JpaRepository<Product, UUID> {
//    @Query(value = "SELECT com.cosmetic.cosmetic.dto.product.ProductTrendingDto(p, AVG(r.averageStar)) " +
//            "FROM Product p " +
//            "INNER JOIN p.reviews r " +
//            "ON r.product = p " +
//            "GROUP BY p")
//    List<ProductTrendingDto> findAllProductTrending();

    @Query(value = "SELECT p as product, AVG(r.averageStar) as average " +
            "FROM Product p " +
            "INNER JOIN p.reviews r " +
            "ON r.product = p " +
            "GROUP BY p " +
            "ORDER BY average DESC")
    List<ProductInfo> findAllProductTrending();


    @Query(value = "SELECT p as product, AVG(r.averageStar) as average, " +
            "AVG(r.effective) as effective, AVG(r.texture) as texture, AVG(r.price) as price," +
            "AVG(r.packaging) as packaging " +
            "FROM Product p " +
            "LEFT JOIN p.reviews r " +
            "ON r.product = p " +
            "WHERE p.id = :id " +
            "GROUP BY p, r ")
    Optional<ProductDetailInfo> findOneProductWithRating(UUID id);
//    @Query(value = "SELECT p.id as id, p.name as name, p.images as images, p.isOrganic as isOrganic," +
//            " AVG(r.averageStar) as average, b as brand, v as variants " +
//            "FROM Product p " +
//            "INNER JOIN p.reviews r " +
//            "ON r.product = p " +
//            "INNER JOIN p.brand b " +
//            "ON p.brand = b " +
//            "INNER JOIN p.variants v " +
//            "ON v.product = p " +
//            "GROUP BY p, r, b, v")
//    List<ProductInfo> findAllProductTrending();

    @Query(value = "SELECT p FROM Product p " +
            "JOIN p.subChild sc " +
            "ON p.subChild = sc.id " +
            "JOIN sc.subCategory sub " +
            "ON sc.subCategory = sub.id " +
            "JOIN sub.category c " +
            "ON sub.category = c.id " +
            "WHERE c.name IN :categories " +
            "OR sub.name IN :subCategories " +
            "OR sc.name IN :subChildCategories"
    )
    Page<Product> findAllProductByCategoriesIn(
            List<String> categories,
            List<String> subCategories,
            List<String> subChildCategories,
            Pageable pageable
    );

    @Query(value = "SELECT p " +
            "FROM Product p " +
            "INNER JOIN p.reviews r " +
            "ON r.product = p " +
            "INNER JOIN p.variants v " +
            "ON v.product = p " +
            "WHERE r.averageStar IN :reviews " +
            "OR p.isOrganic = :isOrganic " +
            "ORDER BY v.price ASC"
    )
    Page<Product> findAllProductByReviewAndPriceToHigh(
            @Param(value = "reviews") List<Double> reviews,
            @Param(value = "isOrganic") Boolean isOrganic,
            @Param(value = "pageable") Pageable pageable
    );

    @Query(value = "SELECT p " +
            "FROM Product p " +
            "INNER JOIN p.reviews r " +
            "ON r.product = p " +
            "INNER JOIN p.variants v " +
            "ON v.product = p " +
            "WHERE r.averageStar IN :reviews " +
            "OR p.isOrganic = :isOrganic " +
            "ORDER BY v.price DESC"
    )
    Page<Product> findAllProductByReviewAndPriceToLow(
            @Param(value = "reviews") List<Double> reviews,
            @Param(value = "isOrganic") Boolean isOrganic,
            @Param(value = "pageable") Pageable pageable
    );

//    select concat_ws(' ', b."name" , p."name") as test from products p
//    inner join brands b
//    on  p."brandId" = b.id
//    where LOWER(concat_ws(' ', b."name" , p."name")) LIKE LOWER('%hat%')
    @Query("SELECT p FROM Product p " +
            "JOIN p.brand b " +
            "ON p.brand = b.id " +
            "WHERE LOWER(p.name || ' ' ||b.name) LIKE LOWER(CONCAT('%',:keyword,'%')) "
           )
    Page<Product> findAllProductAndBrandBySearchKeyword(String keyword, Pageable pageable);
}
