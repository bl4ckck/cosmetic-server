package com.cosmetic.cosmetic.repository;

import com.cosmetic.cosmetic.entity.Brand;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface BrandRepository extends JpaRepository<Brand, UUID> {
}