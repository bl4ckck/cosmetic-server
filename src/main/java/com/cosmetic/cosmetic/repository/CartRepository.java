package com.cosmetic.cosmetic.repository;

import com.cosmetic.cosmetic.entity.Address;
import com.cosmetic.cosmetic.entity.Cart;
import com.cosmetic.cosmetic.entity.User;
import com.cosmetic.cosmetic.mapper.cart.CartAddressInfo;
import com.cosmetic.cosmetic.mapper.cart.CartInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

public interface CartRepository extends JpaRepository<Cart, UUID> {
    Optional<Cart> findByUser_Id(UUID id);

    @Query("SELECT c FROM Cart c WHERE c.user.id = ?1")
    Optional<CartInfo> findCartByUserId(UUID id);

    @Query("SELECT c.address as address FROM Cart c WHERE c.user.id = ?1")
    Optional<CartAddressInfo> selectAddressCartByUserId(UUID id);

    @Query("SELECT c FROM Cart c WHERE c.user.id = ?1")
    Optional<CartInfo> findCart(UUID id);

    @Transactional
    @Modifying
    @Query("update Cart c set c.address = ?1 where c.user = ?2")
    void updateAddressByUser(Address address, User user);
}