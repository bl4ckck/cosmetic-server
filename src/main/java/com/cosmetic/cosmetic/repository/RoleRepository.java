package com.cosmetic.cosmetic.repository;

import com.cosmetic.cosmetic.entity.ERole;
import com.cosmetic.cosmetic.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {
    Optional<Role> findByName(ERole name);
}