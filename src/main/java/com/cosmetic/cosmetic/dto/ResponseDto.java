package com.cosmetic.cosmetic.dto;

import com.github.javafaker.Bool;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseDto<T> {
    private Boolean success;
    private String status;
    private String message;
    private T data;
}
