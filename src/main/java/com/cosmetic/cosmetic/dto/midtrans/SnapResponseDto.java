package com.cosmetic.cosmetic.dto.midtrans;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@NoArgsConstructor
@Data
public class SnapResponseDto {
    private String token;
    private String redirect_url;
}
