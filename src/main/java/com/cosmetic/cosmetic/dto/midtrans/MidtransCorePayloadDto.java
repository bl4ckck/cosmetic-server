package com.cosmetic.cosmetic.dto.midtrans;

import com.google.gson.JsonObject;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@NoArgsConstructor
@Data
public class MidtransCorePayloadDto {
    private JsonObject token;
    private String redirect_url;
}
