package com.cosmetic.cosmetic.dto.midtrans;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@NoArgsConstructor
@Data
public class CoreResponseDto implements Serializable {
    private String orderId;
    private String bankName;
    private String transactionId;
    private String paymentType;
    private String transactionStatus;
    private String vaNumber;
    private Double grossAmount;
    private Date transactionTime;
    private Date expiryTime;
}
