package com.cosmetic.cosmetic.dto.cart;

import com.cosmetic.cosmetic.dto.DtoID;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@Data
public class CartBrandDto implements Serializable, DtoID<UUID> {
    private UUID id;
    private String brandName;
    private String banner;
    private List<CartItemDto> items;
}
