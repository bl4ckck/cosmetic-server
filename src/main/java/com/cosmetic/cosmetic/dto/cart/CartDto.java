package com.cosmetic.cosmetic.dto.cart;

import com.cosmetic.cosmetic.dto.DtoID;
import com.cosmetic.cosmetic.entity.EDelivery;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
@Data
public class CartDto implements Serializable, DtoID<UUID> {
    private UUID id;
    private UUID[] promo;
    private EDelivery delivery;
    private Double total;
    private Double weight;
}
