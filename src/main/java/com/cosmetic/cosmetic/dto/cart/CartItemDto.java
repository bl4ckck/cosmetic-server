package com.cosmetic.cosmetic.dto.cart;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
@Data
public class CartItemDto implements Serializable {
    private UUID variantId;
    private UUID productId;
    private String name;
    private String image;
    private Double price;
    private Integer quantity;
    private Double subTotal;
    private Boolean isChecked;
}
