package com.cosmetic.cosmetic.dto.cart;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
@Data
public class CartSaveDto implements Serializable {
    private UUID variantId;
    private Integer quantity;
}
