package com.cosmetic.cosmetic.dto.cart;

import com.cosmetic.cosmetic.dto.cartvariant.CartVariantDto;
import com.cosmetic.cosmetic.dto.user.UserDto;
import com.cosmetic.cosmetic.entity.EDelivery;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

@NoArgsConstructor
@Data
public class CartMappedDto implements Serializable {
    private UUID id;
    private UUID[] promo;
    private UserDto user;
    private Set<CartVariantDto> carts;
    private EDelivery delivery;
    private Double total;
    private Double weight;
}
