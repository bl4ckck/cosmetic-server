package com.cosmetic.cosmetic.dto.cart;

import com.cosmetic.cosmetic.dto.DtoID;
import com.cosmetic.cosmetic.entity.EDelivery;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

@NoArgsConstructor
@Data
public class CartListDto implements Serializable, DtoID<UUID> {
    private UUID id;
    private String name;
    private String logo;
    private Set<CartItemDto> items;
}
