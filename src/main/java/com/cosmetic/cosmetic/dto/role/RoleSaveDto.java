package com.cosmetic.cosmetic.dto.role;

import com.cosmetic.cosmetic.entity.ERole;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
@Data
public class RoleSaveDto implements Serializable {
    private ERole name;
}
