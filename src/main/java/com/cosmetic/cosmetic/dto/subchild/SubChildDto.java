package com.cosmetic.cosmetic.dto.subchild;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SubChildDto implements Serializable{
    private UUID subChildId;
    private UUID subCategoryId;
    private UUID categoryId;
    private String subChild;
    private String subCategory;
    private String category;

    public SubChildDto(UUID subChildId, UUID subCategoryId, UUID categoryId) {
        this.subChildId = subChildId;
        this.subCategoryId = subCategoryId;
        this.categoryId = categoryId;
    }
}