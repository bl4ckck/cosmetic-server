package com.cosmetic.cosmetic.dto.subchild;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SubChildSaveDto {
    private UUID subChildCategoriesId;
    private UUID subCategoriesId;
}
