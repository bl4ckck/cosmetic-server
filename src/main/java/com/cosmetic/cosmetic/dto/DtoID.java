package com.cosmetic.cosmetic.dto;

/**
 * @param <T> ID type
 */
public interface DtoID<T> {
    T getId();
}
