package com.cosmetic.cosmetic.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class DummyDto {
    private String varcharCustom1;
    private String varcharCustom2;
    private String varcharCustom3;
}
