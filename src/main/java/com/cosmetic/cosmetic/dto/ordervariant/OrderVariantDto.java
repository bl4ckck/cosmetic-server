package com.cosmetic.cosmetic.dto.ordervariant;

import com.cosmetic.cosmetic.entity.OrderVariantKey;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Data
public class OrderVariantDto implements Serializable {
    private OrderVariantKey id;
    private Double productPrice;
    private Integer quantity;
    private Double subTotal;
}
