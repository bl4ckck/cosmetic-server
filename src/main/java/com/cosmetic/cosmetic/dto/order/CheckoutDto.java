package com.cosmetic.cosmetic.dto.order;

import com.cosmetic.cosmetic.entity.EDelivery;
import com.cosmetic.cosmetic.entity.EDeliveryService;
import com.cosmetic.cosmetic.entity.midtrans.EBank;
import com.cosmetic.cosmetic.entity.midtrans.EPaymentType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@ToString
@NoArgsConstructor
@Data
public class CheckoutDto implements Serializable {
    @ApiModelProperty(required = true)
    private EDelivery delivery;

    @ApiModelProperty(required = true)
    private EDeliveryService deliveryService;

    @ApiModelProperty(required = true)
    private EPaymentType paymentType;

    @ApiModelProperty(required = true, value = "if payment method = gopay, you don't have to pass this prop")
    private EBank bank;

    @ApiModelProperty(required = false)
    private List<UUID> voucherId;
}
