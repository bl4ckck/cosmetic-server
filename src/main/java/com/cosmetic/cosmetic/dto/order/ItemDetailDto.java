package com.cosmetic.cosmetic.dto.order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.UUID;

@AllArgsConstructor
@ToString
@NoArgsConstructor
@Data
public class ItemDetailDto implements Serializable {
    private UUID id;
    private String name;
    private Double price;
    private Integer quantity;
}
