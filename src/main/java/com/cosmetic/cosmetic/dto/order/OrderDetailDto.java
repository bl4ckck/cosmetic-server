package com.cosmetic.cosmetic.dto.order;

import com.cosmetic.cosmetic.dto.DtoID;
import com.cosmetic.cosmetic.dto.address.AddressDto;
import com.cosmetic.cosmetic.dto.cart.CartBrandDto;
import com.cosmetic.cosmetic.dto.midtrans.CoreResponseDto;
import com.cosmetic.cosmetic.dto.user.UserDto;
import com.cosmetic.cosmetic.entity.EDelivery;
import com.cosmetic.cosmetic.entity.EStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@Data
public class OrderDetailDto implements Serializable, DtoID<UUID> {
    private UUID id;
    private UserDto user;
    private AddressDto address;
    private List<CartBrandDto> orderItems;
    private CoreResponseDto payment;
    private UUID[] promo;
    private EDelivery delivery;
    private EStatus status;
    private Date date;
    private Double total;
}
