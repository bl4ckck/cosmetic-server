package com.cosmetic.cosmetic.dto.order;

import com.cosmetic.cosmetic.entity.EDelivery;
import com.cosmetic.cosmetic.entity.EDeliveryService;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@ToString
@NoArgsConstructor
@Data
public class DeliveryMethodDto implements Serializable {
    private EDelivery courier;
    private List<CheckoutDeliveryServiceDto> services;
}
