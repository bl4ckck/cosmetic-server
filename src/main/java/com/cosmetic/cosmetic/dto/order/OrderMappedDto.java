package com.cosmetic.cosmetic.dto.order;

import com.cosmetic.cosmetic.dto.address.AddressDto;
import com.cosmetic.cosmetic.dto.ordervariant.OrderVariantDto;
import com.cosmetic.cosmetic.dto.user.UserDto;
import com.cosmetic.cosmetic.entity.EDelivery;
import com.cosmetic.cosmetic.entity.EStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

@NoArgsConstructor
@Data
public class OrderMappedDto implements Serializable {
    private UUID id;
    private UserDto user;
    private AddressDto address;
    private Set<OrderVariantDto> orders;
    private UUID[] promo;
    private EDelivery delivery;
    private EStatus status;
    private Date date;
    private Double total;
}
