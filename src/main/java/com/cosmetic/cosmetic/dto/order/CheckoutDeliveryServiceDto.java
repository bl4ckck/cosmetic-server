package com.cosmetic.cosmetic.dto.order;

import com.cosmetic.cosmetic.entity.EDeliveryService;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@ToString
@NoArgsConstructor
@Data
public class CheckoutDeliveryServiceDto implements Serializable {
    private EDeliveryService serviceType;
    private Double cost;
}