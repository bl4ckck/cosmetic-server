package com.cosmetic.cosmetic.dto.order;

import com.cosmetic.cosmetic.dto.cart.CartDto;
import com.cosmetic.cosmetic.mapper.cart.CartInfo;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@ToString
@NoArgsConstructor
@Data
public class CheckoutCartDto implements Serializable {
    private CartInfo.AddressInfo address;
    private List<DeliveryMethodDto> delivery;
    private CartDto overview;
}
