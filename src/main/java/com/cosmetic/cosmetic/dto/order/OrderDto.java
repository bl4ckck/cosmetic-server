package com.cosmetic.cosmetic.dto.order;

import com.cosmetic.cosmetic.dto.DtoID;
import com.cosmetic.cosmetic.dto.ordervariant.OrderVariantDto;
import com.cosmetic.cosmetic.dto.user.UserDto;
import com.cosmetic.cosmetic.entity.EDelivery;
import com.cosmetic.cosmetic.entity.EStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

@NoArgsConstructor
@Data
public class OrderDto implements Serializable, DtoID<UUID> {
    private UUID id;
    private UUID[] promo;
    private EDelivery delivery;
    private EStatus status;
    private Date date;
    private Double total;
}
