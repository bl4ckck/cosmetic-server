package com.cosmetic.cosmetic.dto.order;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.UUID;

@ToString
@NoArgsConstructor
@Data
public class ItemSaveDto implements Serializable {
    private UUID variantId;
    private Integer quantity;
}
