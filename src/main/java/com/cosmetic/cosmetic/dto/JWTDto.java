package com.cosmetic.cosmetic.dto;

import com.cosmetic.cosmetic.entity.ERole;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@ToString
@NoArgsConstructor
@Data
public class JWTDto {
    private UUID accountId;
    private UUID userId;
    private String email;
    private String avatar;
    private String name;
    private ERole role;
    private Boolean isActive;

    public Map<String, Object> getPayload() {
        HashMap<String, Object> payload = new HashMap<>();
        payload.put("accountId", this.accountId.toString());
        payload.put("userId", this.userId.toString());
        payload.put("isActive", this.isActive);
        payload.put("email", this.email);
        payload.put("avatar", this.avatar);
        payload.put("name", this.name);
        payload.put("role", this.role.name());
        return payload;
    }
}
