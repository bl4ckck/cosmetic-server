package com.cosmetic.cosmetic.dto.account.provider;

import com.cosmetic.cosmetic.dto.DtoID;
import com.cosmetic.cosmetic.entity.EProvider;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
@Data
public class AccountProviderSaveDto implements Serializable {
    private EProvider provider;
}
