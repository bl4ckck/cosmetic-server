package com.cosmetic.cosmetic.dto.account;

import com.cosmetic.cosmetic.dto.user.UserSaveDto;
import com.cosmetic.cosmetic.entity.ERole;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@ToString
@NoArgsConstructor
@Data
public class AccountUserDto implements Serializable {
    private String email;
    private ERole role;
    private UserSaveDto user;
    private String avatar;
    private Boolean isActive;
}