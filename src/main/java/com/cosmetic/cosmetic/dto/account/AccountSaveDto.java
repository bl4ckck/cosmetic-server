package com.cosmetic.cosmetic.dto.account;

import com.cosmetic.cosmetic.dto.user.UserSaveDto;
import com.cosmetic.cosmetic.entity.ERole;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Data
public class AccountSaveDto implements Serializable {
    @ApiModelProperty(required = true, value = "unique constraint")
    private String email;

    @ApiModelProperty(required = true)
    private String password;

    private UserSaveDto user;
    private String avatar;

    @ApiModelProperty(required = true)
    private ERole role;
}
