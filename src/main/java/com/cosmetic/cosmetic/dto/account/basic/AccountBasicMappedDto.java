package com.cosmetic.cosmetic.dto.account.basic;

import com.cosmetic.cosmetic.dto.account.AccountDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
@Data
public class AccountBasicMappedDto implements Serializable {
    private UUID id;
    private String password;
    private AccountDto account;
}
