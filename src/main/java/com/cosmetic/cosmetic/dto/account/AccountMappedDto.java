package com.cosmetic.cosmetic.dto.account;

import com.cosmetic.cosmetic.dto.account.basic.AccountBasicDto;
import com.cosmetic.cosmetic.dto.account.provider.AccountProviderDto;
import com.cosmetic.cosmetic.dto.user.UserDto;
import com.cosmetic.cosmetic.entity.ERole;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
@Data
public class AccountMappedDto implements Serializable {
    private UUID id;
    private String email;
    private ERole role;
    private String avatar;
    private AccountBasicDto accountBasic;
    private AccountProviderDto accountProvider;
    private UserDto user;
    private Boolean isActive;
}