package com.cosmetic.cosmetic.dto.account.basic;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
@Data
public class AccountBasicSaveDto implements Serializable {
    private String password;
}
