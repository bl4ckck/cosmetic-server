package com.cosmetic.cosmetic.dto.account;

import com.cosmetic.cosmetic.dto.DtoID;
import com.cosmetic.cosmetic.entity.ERole;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
@Data
public class AccountDto implements Serializable, DtoID<UUID> {
    private UUID id;
    private String email;
    private ERole role;
    private String avatar;
    private Boolean isActive;
}
