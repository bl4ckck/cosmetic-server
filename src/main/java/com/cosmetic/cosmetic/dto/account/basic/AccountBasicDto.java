package com.cosmetic.cosmetic.dto.account.basic;

import com.cosmetic.cosmetic.dto.DtoID;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
@Data
public class AccountBasicDto implements Serializable, DtoID<UUID> {
    private UUID id;
    private String password;
}
