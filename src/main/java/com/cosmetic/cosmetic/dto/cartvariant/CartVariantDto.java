package com.cosmetic.cosmetic.dto.cartvariant;

import com.cosmetic.cosmetic.entity.CartVariantKey;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Data
public class CartVariantDto implements Serializable {
    private CartVariantKey id;
    private Boolean isChecked;
    private Integer quantity;
    private Double subTotal;
}
