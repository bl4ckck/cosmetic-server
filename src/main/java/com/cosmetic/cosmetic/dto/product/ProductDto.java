package com.cosmetic.cosmetic.dto.product;

import com.cosmetic.cosmetic.dto.DtoID;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
@Data
public class ProductDto implements Serializable, DtoID<UUID> {
    private UUID id;
    private String name;
    private String[] images;
    private Boolean isOrganic;
}
