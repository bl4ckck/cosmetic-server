package com.cosmetic.cosmetic.dto.product;

import io.swagger.annotations.ApiParam;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@Data
public class ProductFilterByReviewAndPriceDto implements Serializable{
    @ApiParam
    private List<Double> Review;
    @ApiParam
    private Boolean HightPrice = Boolean.FALSE;
    @ApiParam
    private Boolean lowPrice = Boolean.FALSE;

    @ApiParam
    private Boolean isOrganic;
    }

