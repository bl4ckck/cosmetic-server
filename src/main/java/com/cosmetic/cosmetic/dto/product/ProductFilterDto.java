package com.cosmetic.cosmetic.dto.product;


import io.swagger.annotations.ApiParam;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@Data
public class ProductFilterDto implements Serializable {
    @ApiParam
   private List<String> category;
    @ApiParam
   private List<String> subcategory;
    @ApiParam
    private List<String> subChildCategory;
}
