package com.cosmetic.cosmetic.dto.product;

import lombok.Builder;
import lombok.Data;
import java.util.List;

@Data
@Builder
public class PaginatedProductResponse {
    private Long numberOfItems;
    private int numberOfPages;
    private List<ProductMappedDto> products;
}
