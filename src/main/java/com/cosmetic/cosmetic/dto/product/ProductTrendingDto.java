package com.cosmetic.cosmetic.dto.product;

import com.cosmetic.cosmetic.dto.brand.BrandDto;
import com.cosmetic.cosmetic.dto.variant.VariantDto;
import lombok.*;

import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

@NoArgsConstructor
@Data
public class ProductTrendingDto implements Serializable {
    private UUID id;
    private Double average;
    private BrandDto brand;
    private Set<VariantDto> variant;
    private String name;
    private String[] images;
    private Boolean isOrganic;
}
