package com.cosmetic.cosmetic.dto.product;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Data
public class ProductSaveDto implements Serializable {
    private String name;
    private String[] images;
    private Boolean isOrganic;
}
