package com.cosmetic.cosmetic.dto.product;

import com.cosmetic.cosmetic.dto.brand.BrandDto;
import com.cosmetic.cosmetic.dto.variant.VariantDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

@NoArgsConstructor
@Data
public class ProductMappedDto implements Serializable {
    private UUID id;
    private BrandDto brand;
    private Set<VariantDto> variant;
    private String name;
    private String[] images;
    private Boolean isOrganic;
}
