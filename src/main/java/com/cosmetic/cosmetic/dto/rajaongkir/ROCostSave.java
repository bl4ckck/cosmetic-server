package com.cosmetic.cosmetic.dto.rajaongkir;

import com.cosmetic.cosmetic.entity.EDelivery;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@NoArgsConstructor
@Data
public class ROCostSave implements Serializable {
    private EDelivery courier;
    private String cityId;
    private String destCityId;
    private Integer weight;

    @Override
    public String toString() {
        return "origin=" + cityId +
                "&destination=" + destCityId +
                "&weight=" + weight +
                "&courier=" + courier.name;
    }
}
