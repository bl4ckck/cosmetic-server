package com.cosmetic.cosmetic.dto.rajaongkir;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
public class ROCostListItem implements Serializable {
    private Double value;
    private String etd;
    private String note;
}
