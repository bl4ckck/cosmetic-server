package com.cosmetic.cosmetic.dto.rajaongkir;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
public class ROCost implements Serializable {
    private String code;
    private String name;
    private List<ROCostList> costs;
}
