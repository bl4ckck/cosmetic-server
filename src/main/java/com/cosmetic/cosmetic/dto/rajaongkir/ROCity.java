package com.cosmetic.cosmetic.dto.rajaongkir;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
public class ROCity implements Serializable {
    private String cityId;
    private String provinceId;
    private String province;
    private String type;
    private String cityName;
    private String postalCode;
}
