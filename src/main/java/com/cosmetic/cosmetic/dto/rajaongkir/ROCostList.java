package com.cosmetic.cosmetic.dto.rajaongkir;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
public class ROCostList implements Serializable {
    private String service;
    private String description;
    private List<ROCostListItem> cost;
}
