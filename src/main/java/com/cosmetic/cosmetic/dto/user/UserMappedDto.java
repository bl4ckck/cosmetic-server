package com.cosmetic.cosmetic.dto.user;

import com.cosmetic.cosmetic.dto.account.AccountDto;
import com.cosmetic.cosmetic.dto.address.AddressDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

@NoArgsConstructor
@Data
public class UserMappedDto implements Serializable {
    private UUID id;
    private AccountDto account;
    private String name;
    private String phone;
    private String skinType;
    private Set<AddressDto> addresses;
}
