package com.cosmetic.cosmetic.dto.user;

import com.cosmetic.cosmetic.entity.Account;
import lombok.EqualsAndHashCode;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

@EqualsAndHashCode(callSuper = true)
public class UserDetailsDto extends User {
    private final Account account;
    public UserDetailsDto(Account account, Collection<? extends GrantedAuthority> authorities) {
        super(account.getEmail(), account.getAccountBasic().getPassword(), authorities);
        this.account = account;
    }

    public Account getAccount() {
        return this.account;
    }
}
