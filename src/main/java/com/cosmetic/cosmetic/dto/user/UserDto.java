package com.cosmetic.cosmetic.dto.user;

import com.cosmetic.cosmetic.dto.DtoID;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
@Data
public class UserDto implements Serializable, DtoID<UUID> {
    private UUID id;
    private String name;
    private String phone;
    private String skinType;
}
