package com.cosmetic.cosmetic.dto.user;

import com.cosmetic.cosmetic.dto.address.AddressSaveDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Set;

@NoArgsConstructor
@Data
public class UserSaveDto implements Serializable {
    @ApiModelProperty(required = true)
    private String name;

    @ApiModelProperty(value = "unique constraint")
    private String phone;

    private String skinType;
    private AddressSaveDto address;
}
