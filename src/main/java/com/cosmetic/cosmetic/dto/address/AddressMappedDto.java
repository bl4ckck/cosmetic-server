package com.cosmetic.cosmetic.dto.address;

import com.cosmetic.cosmetic.dto.user.UserDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@Data
@NoArgsConstructor
public class AddressMappedDto implements Serializable {
    private UUID id;
    private UserDto user;
    private String phone;
    private String receiver;
    private String label;
    private Boolean isDefault;
    private String cityId;
    private String postalCode;
    private String addressDetail;
}
