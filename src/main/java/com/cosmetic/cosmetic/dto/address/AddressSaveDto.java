package com.cosmetic.cosmetic.dto.address;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@ToString
@Data
public class AddressSaveDto implements Serializable {
    @ApiModelProperty(value = "unique constraint")
    private String phone;

    private String receiver;
    private String label;

    @ApiModelProperty(value = "if new user, please set as true, if not it will show an error")
    private Boolean isDefault;

    @ApiModelProperty(value = "get this from raja ongkir endpoint")
    private String cityId;

    @ApiModelProperty(value = "you can fill any")
    private String postalCode;

    private String addressDetail;
}
