package com.cosmetic.cosmetic.dto.address;

import com.cosmetic.cosmetic.dto.DtoID;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@Data
@NoArgsConstructor
public class AddressDto implements Serializable, DtoID<UUID> {
    private UUID id;
    private String phone;
    private String receiver;
    private String label;
    private Boolean isDefault;
    private String cityId;
    private String postalCode;
    private String addressDetail;
}
