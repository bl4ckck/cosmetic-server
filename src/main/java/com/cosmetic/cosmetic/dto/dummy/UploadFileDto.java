package com.cosmetic.cosmetic.dto.dummy;

import lombok.Data;

@Data
public class UploadFileDto {
    private String excelFile;
}
