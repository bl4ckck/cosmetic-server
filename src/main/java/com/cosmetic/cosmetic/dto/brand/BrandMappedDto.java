package com.cosmetic.cosmetic.dto.brand;

import com.cosmetic.cosmetic.dto.product.ProductDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

@NoArgsConstructor
@Data
public class BrandMappedDto implements Serializable {
    private UUID id;
    private Set<ProductDto> product;
    private String name;
    private String description;
    private String logo;
    private String banner;
}
