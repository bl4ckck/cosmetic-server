package com.cosmetic.cosmetic.dto.brand;

import com.cosmetic.cosmetic.dto.DtoID;
import com.cosmetic.cosmetic.dto.product.ProductDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
@Data
public class BrandDto implements Serializable, DtoID<UUID> {
    private UUID id;
    private String name;
    private String description;
    private String logo;
    private String banner;
}
