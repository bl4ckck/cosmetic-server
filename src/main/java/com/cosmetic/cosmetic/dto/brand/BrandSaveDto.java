package com.cosmetic.cosmetic.dto.brand;

import com.cosmetic.cosmetic.dto.product.ProductSaveDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Set;

@NoArgsConstructor
@Data
public class BrandSaveDto implements Serializable {
    private String name;
    private Set<ProductSaveDto> products;
    private String description;
    private String logo;
    private String banner;
}
