package com.cosmetic.cosmetic.dto.article;

import com.cosmetic.cosmetic.dto.DtoID;
import com.cosmetic.cosmetic.dto.ingredient.IngredientDto;
import com.cosmetic.cosmetic.dto.subchild.SubChildDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
public class ArticleResponseDto extends ArticleMappedDto implements Serializable, DtoID<UUID> {
    private UUID userId;
    private String title;
    private String content;
    private String date;
    private String photo;
    private List<SubChildDto> productCategories;
    private List<IngredientDto> ingredientCategories;
}