package com.cosmetic.cosmetic.dto.article;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArticleIngredientsDto {
    private UUID ingredientId;

    public ArticleIngredientsDto(UUID articleId,UUID ingredientId) {
        this.ingredientId = ingredientId;
    }
}