package com.cosmetic.cosmetic.dto.article;

import com.cosmetic.cosmetic.dto.ingredient.IngredientDto;
import com.cosmetic.cosmetic.dto.subchild.SubChildDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
public class ArticleMappedDto implements Serializable{
    private UUID id;
    private UUID userid;
    private String title;
    private String photo;
    private String date;
    private String content;
    private List<SubChildDto> productCategories;
    private List<IngredientDto> ingredientCategories;
}