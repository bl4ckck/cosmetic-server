package com.cosmetic.cosmetic.dto.article;

import com.cosmetic.cosmetic.dto.DtoID;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
public class ArticleDto implements Serializable, DtoID<UUID> {
    private UUID id;
    private String title;
    private String content;
    private String photo;
    private Date date;
}