package com.cosmetic.cosmetic.dto.article;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArticleSaveDto implements Serializable {
    private UUID userid;
    private String title;
    private String content;
    private String photo;
    private Date date;
    private List<ArticleCategoriesDto> productCategories;
    private List<ArticleIngredientsDto> ingredientCategories;
}

