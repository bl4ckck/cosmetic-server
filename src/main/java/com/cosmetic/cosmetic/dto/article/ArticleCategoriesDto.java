package com.cosmetic.cosmetic.dto.article;

import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.UUID;

@Data
@NoArgsConstructor
public class ArticleCategoriesDto {
    private UUID subChildId;

    public ArticleCategoriesDto( UUID subChildId) {
        this.subChildId = subChildId;
    }
}