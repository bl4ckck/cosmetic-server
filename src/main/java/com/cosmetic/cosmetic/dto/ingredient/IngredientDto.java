package com.cosmetic.cosmetic.dto.ingredient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IngredientDto implements Serializable {
    private UUID ingredientId;
    private String name;
}