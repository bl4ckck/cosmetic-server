package com.cosmetic.cosmetic.dto.ingredient;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class IngredientSaveDto {
    private String name;
}