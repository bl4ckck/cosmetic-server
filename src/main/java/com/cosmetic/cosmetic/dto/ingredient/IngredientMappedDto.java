package com.cosmetic.cosmetic.dto.ingredient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IngredientMappedDto {
    private UUID id;
    private String name;
}