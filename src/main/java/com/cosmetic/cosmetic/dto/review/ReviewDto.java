package com.cosmetic.cosmetic.dto.review;

import com.cosmetic.cosmetic.dto.DtoID;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.util.UUID;

@Data
@NoArgsConstructor
public class ReviewDto implements Serializable, DtoID<UUID> {
    private UUID id;
    @JsonIgnore
    private UUID productId;
    @JsonIgnore
    private UUID userId;
    private Integer effective;
    private Integer texture;
    private Integer price;
    private Integer packaging;
    private String content;
}
