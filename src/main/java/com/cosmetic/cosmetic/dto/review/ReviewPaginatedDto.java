package com.cosmetic.cosmetic.dto.review;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ReviewPaginatedDto {
    private Long numberOfItems;
    private int numberOfPages;
    private List<ReviewMappedDto> reviews;
}