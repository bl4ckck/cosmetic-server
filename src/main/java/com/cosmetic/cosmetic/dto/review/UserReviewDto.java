package com.cosmetic.cosmetic.dto.review;

import lombok.Data;
import java.util.UUID;

@Data
public class UserReviewDto {
    private UUID id;
    private String avatar;
    private String name;
    private String phone;
    private String skinType;
    private String address;
}
