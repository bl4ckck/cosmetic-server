package com.cosmetic.cosmetic.dto.review;

import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.util.UUID;

@Data
@NoArgsConstructor
public class ReviewMappedDto implements Serializable {
    private UUID id;
    private UUID productId;
    private UserReviewDto user;
    private Integer effective;
    private Integer texture;
    private Integer price;
    private Integer packaging;
    private Double averageStar;
    private String date;
    private String content;
    private String[] images;
    private Integer imagesCount;
}
