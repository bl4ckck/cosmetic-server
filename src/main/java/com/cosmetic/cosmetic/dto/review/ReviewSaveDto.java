package com.cosmetic.cosmetic.dto.review;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Data
public class ReviewSaveDto implements Serializable {
    private UUID userId;
    private UUID productId;
    private Integer effective;
    private Integer texture;
    private Integer price;
    private Integer packaging;
    private String content;
    @JsonIgnore
    private List<MultipartFile> files;
}
