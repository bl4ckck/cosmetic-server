package com.cosmetic.cosmetic.dto.promo;

import com.cosmetic.cosmetic.dto.DtoID;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@NoArgsConstructor
@Data
public class PromoMappedDto implements Serializable, DtoID<UUID> {
    private UUID id;
    private String name;
    private String voucher;
    private Integer discount;
    private Double minimumPrice;
    private Date startDate;
    private Date endDate;
}
