package com.cosmetic.cosmetic.dto.promo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@NoArgsConstructor
@Data
public class PromoSaveDto implements Serializable {
    private String name;
    private String voucher;
    private Integer discount;
    private Double minimumPrice;
    private Date startDate;
    private Date endDate;
}
