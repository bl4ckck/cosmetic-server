package com.cosmetic.cosmetic.dto.variant;

import com.cosmetic.cosmetic.dto.DtoID;
import com.cosmetic.cosmetic.dto.product.ProductDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
@Data
public class VariantMappedDto implements Serializable, DtoID<UUID> {
    private UUID id;
    private ProductDto product;
    private String name;
    private Double price;
    private Integer quantity;
    private Double weight;
    private Integer imageIndex;
}
