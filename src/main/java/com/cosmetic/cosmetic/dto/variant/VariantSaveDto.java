package com.cosmetic.cosmetic.dto.variant;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
@Data
public class VariantSaveDto implements Serializable {
    private UUID productId;
    private String name;
    private Double price;
    private Integer quantity;
    private Double weight;
    private Integer imageIndex;
}
