package com.cosmetic.cosmetic.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@EnableSwagger2
@EnableWebMvc
@Configuration
public class SpringFoxConfig {
    @Bean
    public Docket swaggerApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("All")
                .select()
                .build()
                .securitySchemes(this.securitySchemeArray())
                .apiInfo(this.apiInfo());
    }

    @Bean
    public Docket authApi() {
        return this.generateGroup("Auth", "/api/v1/auth/**", false);
    }

    @Bean
    public Docket productApi() {
        return this.generateGroup("Product", "/api/v1/product/**", true);
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Cosmetic API")
                .version("V1.0.0")
                .build();
    }

    private Docket generateGroup(String groupName, String path, boolean security) {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(groupName)
                .apiInfo(this.apiInfo())
                .select()
                .paths(PathSelectors.ant(path))
                .build()
                .securitySchemes(security ? this.securitySchemeArray() : null);
//                .securityContexts(Arrays.asList(securityContext()));
//                .securityContexts(Arrays.asList(securityContext()));
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
//                .forPaths(PathSelectors.regex("/anyPath.*"))
                .build();
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Arrays.asList(new SecurityReference("Authorization", authorizationScopes));
    }

    private List<springfox.documentation.service.SecurityScheme> securitySchemeArray() {
//        HttpAuthenticationScheme authenticationScheme = HttpAuthenticationScheme.JWT_BEARER_BUILDER
//                .name("Authorization")
//                .build();
//        return Collections.singletonList(authenticationScheme);
        return Collections.singletonList(new ApiKey("Bearer", "Authorization", "header"));
//        return List.of(
//                HttpAuthenticationScheme.JWT_BEARER_BUILDER
//                        .bearerFormat("Bearer")
//                        .name("Bearer")
//                        .build()
//        );
    }
}