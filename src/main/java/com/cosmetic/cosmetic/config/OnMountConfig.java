package com.cosmetic.cosmetic.config;

import com.cosmetic.cosmetic.service.database.SeederService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import java.util.Arrays;

@Log4j2
@Configuration
public class OnMountConfig {
    @Autowired
    private SeederService seederService;

    @Value( "${spring.jpa.hibernate.ddl-auto}" )
    private String DDL_TYPE;

    @Bean
    ApplicationRunner applicationRunner(Environment environment) {
        return args -> {
            this.seed();
            log.info("Current profile " +
                    Arrays.toString(environment.getActiveProfiles()));
        };
    }

    /**
     * Running seeder when ddl-auto = create
     */
    private void seed() {
        Boolean status = seederService.execute(DDL_TYPE);
        if (status == Boolean.TRUE) {
            log.info("Seeders completed");
        } else {
            log.info("No running seeders");
        }
    }
}
