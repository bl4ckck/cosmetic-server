package com.cosmetic.cosmetic.config;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import io.github.cdimascio.dotenv.Dotenv;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CloudinaryConfig {
    @Value("${cloudinary.cloud.name:null}")
    private String CLOUD_NAME;

    @Value("${cloudinary.api.key:null}")
    private String API_KEY;

    @Value("${cloudinary.api.secret:null}")
    private String API_SECRET;

    private final Dotenv dotenv = Dotenv.configure().ignoreIfMissing().load();

    @Bean
    public Cloudinary cloudinary() {
        String cloudName;
        String apiKey;
        String apiSecret;

        if (dotenv.get("CLOUD_NAME").isEmpty()
                && dotenv.get("CLOUDINARY_API_KEY").isEmpty()
                && dotenv.get("CLOUDINARY_API_SECRET").isEmpty()
        ) {
            cloudName = this.CLOUD_NAME;
            apiKey = this.API_KEY;
            apiSecret = this.API_SECRET;
        } else {
            cloudName = dotenv.get("CLOUD_NAME");
            apiKey = dotenv.get("CLOUDINARY_API_KEY");
            apiSecret = dotenv.get("CLOUDINARY_API_SECRET");
        }

        return new Cloudinary(ObjectUtils.asMap(
                "cloud_name", cloudName,
                "api_key", apiKey,
                "api_secret", apiSecret,
                "secure", true));
    }
}
