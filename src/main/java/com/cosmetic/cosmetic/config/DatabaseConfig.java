package com.cosmetic.cosmetic.config;

import io.github.cdimascio.dotenv.Dotenv;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.sql.DataSource;

@Configuration
@Profile("dev")
public class DatabaseConfig {
    private final Dotenv dotenv = Dotenv.configure().load();

    @Bean
    public DataSource getDataSource() {
        return DataSourceBuilder.create()
                .driverClassName("org.postgresql.Driver")
                .url(getDataSourceUrl())
                .username(dotenv.get("DB_USERNAME"))
                .password(dotenv.get("DB_PASSWORD"))
                .build();
    }

    private String getDataSourceUrl() {
        return "jdbc:postgresql://" + dotenv.get("DB_HOST")
                + ":" + dotenv.get("DB_PORT")
                + "/" + dotenv.get("DB_NAME");
    }
}