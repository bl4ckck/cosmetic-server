package com.cosmetic.cosmetic.config;

import com.midtrans.Config;
import io.github.cdimascio.dotenv.Dotenv;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MidtransServiceConfig {

    @Value( "${midtrans.server.key:null}" )
    private String SERVER_KEY;

    @Value( "${midtrans.client.key:null}" )
    private String CLIENT_KEY;

    private final Dotenv dotenv = Dotenv.configure().ignoreIfMissing().load();

    @Bean
    public Config config() {
        String serverKey;
        String clientKey;

        if (dotenv.get("MIDTRANS_SERVER_KEY").isEmpty()
                && dotenv.get("MIDTRANS_CLIENT_KEY").isEmpty()) {
            serverKey = this.SERVER_KEY;
            clientKey = this.CLIENT_KEY;
        } else {
            serverKey = dotenv.get("MIDTRANS_SERVER_KEY");
            clientKey = dotenv.get("MIDTRANS_CLIENT_KEY");
        }

        return Config.builder()
                .setServerKey(serverKey)
                .setClientKey(clientKey)
                .setIsProduction(false)
                .build();
    }
}
